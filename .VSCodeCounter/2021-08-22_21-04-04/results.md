# Summary

Date : 2021-08-22 21:04:04

Directory c:\Users\LENOVO\Desktop\git-Projects\dose_api

Total : 118 files,  7611 codes, 3900 comments, 306 blanks, all 11817 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JavaScript | 115 | 4,335 | 3,900 | 302 | 8,537 |
| JSON | 2 | 3,273 | 0 | 2 | 3,275 |
| Markdown | 1 | 3 | 0 | 2 | 5 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 118 | 7,611 | 3,900 | 306 | 11,817 |
| libs | 4 | 60 | 15 | 10 | 85 |
| migrations | 14 | 528 | 0 | 0 | 528 |
| models | 30 | 1,136 | 63 | 70 | 1,269 |
| routes | 45 | 1,725 | 3,791 | 118 | 5,634 |
| seeders | 9 | 357 | 2 | 42 | 401 |
| test | 7 | 341 | 24 | 37 | 402 |
| test\routes | 6 | 334 | 23 | 34 | 391 |

[details](details.md)