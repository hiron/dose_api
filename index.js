const express = require("express");
const consign = require("consign");
const http = require("http");
require("dotenv").config();
const app = express();
const PORT = process.env.PORT || 3000;

var server = http.createServer(app);
var io = require("socket.io")(server);
//console.log(app);

consign({ verbose: false })
  .include("libs/config.js")
  .then("db.js")
  .then("auth.js")
  .then("adminAccess.js")
  .then("libs/middleware.js")
  .then("libs/util.js")
  .then("routes")
  .then("libs/boot.js")
  .into(app, io);

server.listen(PORT, () => console.log(`DOSE API @port-${PORT}`));

module.exports = app;
