const passport = require("passport");
const { Strategy, ExtractJwt } = require("passport-jwt");
const colors = require("colors");

module.exports = (app) => {
  //const Login = app.db.Login;
  const cfg = app.libs.config;
  const params = {
    secretOrKey: cfg.jwtSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("jwt"),
  };

  const strategy = new Strategy(params, (payload, done) => {
    app.db.Login.findOne({ where: { uId: payload.uId, state: true } })
      .then((login) => {
        var { uId, id, type } = login;
        if (login) {
          return done(null, {
            id,
            uId,
            type,
          });
        }
        return done(null, false, { msg: "Incorrect user." });
      })
      .catch((error) => {
        console.log("i am not general".red);
        done(error, false, "UnAuthorised");
      });
  });

  const adminStrategy = new Strategy(params, (payload, done) => {
    app.db.AdminLogin.findOne({ where: { uId: payload.uId, state: true } })
      .then((login) => {
        var { uId, id, type } = login;
        if (login) {
          return done(null, {
            id,
            uId,
            type,
          });
        }
        return done(null, false);
      })
      .catch((error) => {
        console.log("i am not admin".red);
        done(error, false, "unAuthorised");
      });
  });

  const allStrategy = new Strategy(params, (payload, done) => {
    app.db.Login.findOne({ where: { uId: payload.uId, state: true } })
      .then((login) => {
        var { uId, id, type } = login;
        if (login) {
          return done(null, {
            id,
            uId,
            type,
          });
        }
        return done(null, false, { msg: "Incorrect user." });
      })
      .catch((error) => {
        //console.log("i am not general".red);
        //done(error, false, "UnAuthorised");
        app.db.AdminLogin.findOne({ where: { uId: payload.uId, state: true } })
          .then((login) => {
            var { uId, id, type } = login;
            if (login) {
              return done(null, {
                id,
                uId,
                type,
              });
            }
            return done(null, false, { msg: "Incorrect user." });
          })
          .catch((error) => {
            //console.log("i am not general".red);
            done(error, false, "UnAuthorised");
          });
      });
  });

  passport.use("user-role", strategy);
  passport.use("admin-role", adminStrategy);
  passport.use("all", allStrategy);

  return {
    initialize: () => {
      return passport.initialize();
    },
    authenticate: (role = "user-role") => {
      return passport.authenticate(role, cfg.jwtSession);
    },
  };
};
