'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Invoices', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      payment_method: {
        type: Sequelize.STRING
      },
      price: {
        type: Sequelize.FLOAT
      },
      deliveryCharge: {
        type: Sequelize.FLOAT
      },
      prescription: {
        type: Sequelize.BOOLEAN
      },
      orderTime: {
        type: Sequelize.DATE
      },
      confirmationTime: {
        type: Sequelize.DATE
      },
      addressId: {
        type: Sequelize.INTEGER
      },
      done: {
        type: Sequelize.BOOLEAN
      },
      cancel: {
        type: Sequelize.BOOLEAN
      },
      cancelBy: {
        type: Sequelize.STRING
      },
      invoiceId: {
        type: Sequelize.STRING
      },
      clientId: {
        type: Sequelize.INTEGER
      },
      agentId: {
        type: Sequelize.INTEGER
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Invoices');
  }
};