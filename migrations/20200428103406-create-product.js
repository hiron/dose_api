'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Products', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      manufacturer: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      group: {
        type: Sequelize.STRING
      },
      genericName: {
        type: Sequelize.STRING
      },
      strength: {
        type: Sequelize.STRING
      },
      price: {
        type: Sequelize.INTEGER
      },
      prescription: {
        type: Sequelize.BOOLEAN
      },
      keyword: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Products');
  }
};