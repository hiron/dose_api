"use strict";

const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const sequelizeStream = require("node-sequelize-stream");
const basename = path.basename(__filename);
const db = {};

module.exports = (app) => {
  let sequelize;
  const config = app.libs.config;

  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  );

  sequelizeStream(sequelize, 100);

  fs.readdirSync(path.join(__dirname, "models"))
    .filter((file) => {
      return (
        file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
      );
    })
    .forEach((file) => {
      const model = sequelize["import"](path.join(__dirname, "models", file));
      db[model.name] = model;
    });

  Object.keys(db).forEach((modelName) => {
    if (db[modelName].associate) {
      db[modelName].associate(db);
    }
  });

  db.sequelize = sequelize;
  db.Sequelize = Sequelize;
  db.sequelizeStream = sequelizeStream;
  return db;
};
