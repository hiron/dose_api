const jwt = require("jwt-simple");
const uniqid = require("uniqid");
const speakeasy = require("speakeasy");

describe("Route: Email", () => {
  const Login = app.db.Login;
  const cfg = app.libs.config;
  var token;
  var otp;

  before((done) => {
    Login.destroy({
      where: {},
    }).then((_) =>
      Login.create({
        phone: "01521257488",
        type: "client",
        uId: uniqid.process(),
        secret: speakeasy.generateSecret().ascii,
      })
        .then((user) => {
          token =
            "JWT " +
            jwt.encode(
              {
                uId: user.uId,
              },
              cfg.jwtSecret
            );

          otp = speakeasy.totp({
            secret: user.secret,
          });

          return app.db.UserClient.create({
            birthDate: "1989-03-17",
            firstName: "Hiron",
            lastName: "Das",
            email: "hcdas.09@gmail.com",
            sex: "male",
            nid: "2693717",
            clientId: user.uId,
          });
        })
        .then((data) => {
          console.log(JSON.stringify(data).blue);
          done();
        })
    );
  });

  after((done) => {
    app.db.UserClient.destroy({
      where: {},
    }).then((_) => done());
  });

  describe("POST 'email'", () => {
    describe("Staus: 202", () => {
      it("Email varified", (done) => {
        request
          .post("/email")
          .set("Content-Type", "application/json")
          .set("Authorization", token)
          .send({
            otp: otp,
          })
          .expect(202)
          .end((err) => done(err));
      });
    });

    describe("Staus: 403", () => {
      it("varify without 'otp'", (done) => {
        console.log(token.red);
        request
          .post("/email")
          .set("Content-Type", "application/json")
          .set("Authorization", token)
          .send({})
          .expect(403)
          .end((err) => done(err));
      });
    });
  });

  describe("GET 'email'", (_) => {
    describe("Status: 200", (_) => {
      it("Email send to the user", (done) => {
        request
          .get("/email")
          // .set("Content-Type", "application/json")
          .set("Authorization", token)
          .expect(200)
          .end((err, res) => {
            console.log(res.data);
            done(err);
          });
      });
    });

    // describe("Status: 401", (_) => {
    //   it("Unauthorized token", (done) => {
    //     request
    //       .get("/client")
    //       .expect(401)
    //       .end((err, res) => {
    //         console.log(res.data);
    //         done(err);
    //       });
    //   });
    // });
  });
});
