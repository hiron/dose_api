const jwt = require("jwt-simple");
const uniqid = require("uniqid");
const speakeasy = require("speakeasy");

describe("Route: User", () => {
    const Login = app.db.Login;
    const cfg = app.libs.config;
    var token;
    before((done) => {
        Login.destroy({
            where: {}
        }).then(_ =>
            Login.create({
                phone: "01521257488",
                type: "client",
                uId: uniqid.process(),
                secret: speakeasy.generateSecret().ascii,
            }).then(user => {
                token = "JWT " + jwt.encode({
                    uId: user.uId
                }, cfg.jwtSecret);
                done();
            })
        )
    });

    after((done) => {
        app.db.UserClient.destroy({
            where: {}
        }).then(_ => done());
    });

    describe("POST '\client'", () => {
        describe("Staus: 204", () => {
            it("Client profile created", done => {
                console.log(token.red);
                request.post("/client")
                .set("Content-Type", "application/json")
                .set("Authorization", token).send({
                    // "birthDate": "1989-03-17",
                    "firstName": "Hiron",
                    "lastName": "Das",
                    "email": "hcdas.09@gmail.com",
                    // "sex": "male",
                    // "nid": "2693717",
                }).expect(204).end((err) => done(err));
            })
        });

        describe("Staus: 403", () => {
            it("Client profile without 'firstName'", done => {
                console.log(token.red);
                request.post("/client")
                .set("Content-Type", "application/json")
                .set("Authorization", token).send({
                    //firstName is a mandatory field
                    "lastName": "Das",
                    "email": "hcdas.09@gmail.com",
                    "sex": "male",
                    "nid": "2693717",
                }).expect(403).end((err) => done(err));
            })
        });
    });

    

    describe("GET '\client'", _ => {
        describe('Status: 200', _ => {
            it("Get Client Details", done => {
                request.get('/client')
                    // .set("Content-Type", "application/json")
                    .set("Authorization", token)
                    .expect(200)
                    .end((err, res) => {
                        console.log(res.data);
                        done(err);
                    })
            })
        });

        describe('Status: 401', _ => {
            it("Unauthorized token", done => {
                request.get('/client')
                    .expect(401)
                    .end((err, res) => {
                        console.log(res.data);
                        done(err);
                    })
            })
        });
    });

    describe("PUT '\client'", _ => {
        describe('Status: 204', _ => {
            it("Update Client Details", done => {
                request.put('/client')
                    .set("Content-Type", "application/json")
                    .set("Authorization", token)
                    .send({
                        //firstName is a mandatory field
                        "firstName": "Suvash",
                        "email": "hcdas@outlook.com"
                    })
                    .expect(204)
                    .end((err, res) => {
                        console.log(res.data);
                        done(err);
                    })
            })
        });

    });

});