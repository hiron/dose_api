describe("Routes: Login & registration", () => {
  const Login = app.db.Login;
  beforeEach((done) => {
    Login.destroy({ where: {} }).then((user) => done());
  });

  describe("POST: '/login'", () => {
    describe("status 200", () => {
      it("send a otp to the user phone", (done) => {
        request
          .post("/login")
          .send({ phone: "59739573213", type: "client" })
          .expect(200)
          .end((err, res) => {
            // expect(res.body).to.eql("ok");
            done(err);
          });
      });
    });

    describe("status 401", () => {
        it("wrong phone or type formate", (done) => {
          request
            .post("/login")
            .send({ phone: "0170298405", type: "sdffe" })
            .expect(403)
            .end((err, res) => {
              // expect(res.body).to.eql("ok");
              done(err);
            });
        });
      });

      describe("status 412", () => {
        it("blank phone or type", (done) => {
          request
            .post("/login")
            .send({  })
            .expect(412)
            .end((err, res) => {
              // expect(res.body).to.eql("ok");
              done(err);
            });
        });
      });
  });
});
