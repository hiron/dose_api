const uniqid = require("uniqid");
const speakeasy = require("speakeasy");
const atob = require("atob");

describe("Routes: Token", () => {
  const Login = app.db.Login;
  let otp;
  beforeEach((done) => {
    Login.destroy({
      where: {}
    }).then(
      Login.create({
        phone: "01739573213",
        type: "agent",
        uId: uniqid.process(),
        secret: speakeasy.generateSecret().ascii,
      }).then((user) => {
        otp = speakeasy.totp({
          secret: user.secret
        });
        console.log(otp.yellow);

        done();
      })
    );
  });

  describe("POST '\token'", () => {
    describe("status 200", () => {
      it("otp is matched!!", (done) => {
        request
          .post("/token")
          .send({
            phone: "01739573213",
            otp: otp,
            type: "agent"
          })
          .expect(200)
          .end((err, res) => {
            console.log(res.body.token.green);
            console.log(atob(res.body.token.split(".")[1]).yellow);

            done(err);
          });
      });
    });

    describe("status 401", () => {
      it("otp is wrong!!", (done) => {
        request
          .post("/token")
          .send({
            phone: "01739573213",
            otp: "123546",
            type: "agent"
          })
          .expect(401)
          .end((err, res) => {
            done(err);
          });
      });
    });

    describe("status 403", () => {
      it("phone is invalid or wrong", (done) => {
        request
          .post("/token")
          .send({
            phone: "012364758",
            otp: "123546",
            type: "agent"
          })
          .expect(403)
          .end((err, res) => {
            done(err);
          });
      });
    });
  });
});