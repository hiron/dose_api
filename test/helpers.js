const supetest = require('supertest');
const chai = require('chai');
const app = require('../index.js');
const colors = require('colors');

// const atob = require('atob');

global.app = app;
global.request = supetest(app);
global.expect = chai.expect;
