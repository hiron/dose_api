"use strict";
module.exports = (sequelize, DataTypes) => {
  const Commission = sequelize.define(
    "Commission",
    {
      payment: {
        type: DataTypes.FLOAT,
        allowNull: false,
        validate: {
          max: {
            args: 100,
            msg: "value should not be greater then 100",
          },
          min: {
            args: 0,
            msg: "value should not be lesser then 0",
          },
        },
      },
      delivery: {
        type: DataTypes.FLOAT,
        allowNull: false,
        validate: {
          max: {
            args: 100,
            msg: "value should not be greater then 100",
          },
          min: {
            args: 0,
            msg: "value should not be lesser then 0",
          },
        },
      },
    },
    {
      underscored: true,
      timestamps: false,
    }
  );
  Commission.associate = function (models) {
    // associations can be defined here
  };
  return Commission;
};
