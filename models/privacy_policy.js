"use strict";
module.exports = (sequelize, DataTypes) => {
  const privacyPolicy= sequelize.define(
    "PrivacyPolicy",
    {
      title: {
        type: DataTypes.TEXT("tiny"), //DataTypes.TEXT,
        validate: {
          len: { args: [0, 255], msg: "Review is exited the max limit" },
        },
      },
      detail:{
          type: DataTypes.STRING(1234),
      }
    },
    {
      underscored: true,
    }
  );
  return privacyPolicy;
};
