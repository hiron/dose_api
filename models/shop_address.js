'use strict';
module.exports = (sequelize, DataTypes) => {
  const ShopAddress = sequelize.define('ShopAddress', {
    lat: {
      type: DataTypes.FLOAT(4,8),
      allowNull: false
    },
    long: {
      type: DataTypes.FLOAT(4,8),
      allowNull: false
    },
    name: DataTypes.STRING(60),
  }, {
    underscored: true
  });

  ShopAddress.associate = function (models) {
    ShopAddress.belongsTo(models.Login, {
      foreignKey: "agentId",
      targetKey: "uId"
    });
    ShopAddress.belongsTo(models.Area, {
      foreignKey: "area",
      targetKey: "id"
    });
  };
  return ShopAddress;
};

