"use strict";
module.exports = (sequelize, DataTypes) => {
  const FAQ = sequelize.define(
    "faq",
    {
      question: {
        type: DataTypes.TEXT("tiny"), //DataTypes.TEXT,
        validate: {
          len: { args: [0, 255], msg: "Review is exited the max limit" },
        },
      },
      answer: {
        type: DataTypes.STRING(1234),
      },
      order: { type: DataTypes.INTEGER(5), allowNull: false },
    },
    {
      underscored: true,
    }
  );
  return FAQ;
};
