"use strict";
module.exports = (sequelize, DataTypes) => {
  const Area = sequelize.define("Area", {
    district: {
      type: DataTypes.STRING(20),
      allowNull: false,
    },
    area: {
      type: DataTypes.STRING(40),
      allowNull: false,
    },
  },{
    underscored: true,
    timestamps: false
  });

  return Area;
};
