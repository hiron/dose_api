"use strict";

module.exports = (sequelize, DataTypes) => {
  const AdminLogin = sequelize.define(
    "AdminLogin",
    {
      username: {
        type: DataTypes.STRING(15),
        allowNull: false,
        unique: true,
      },
      // password: {
      //   type: DataTypes.STRING(15),
      //   allowNull: false,
      //   validate: {
      //     len: [8, 13],
      //     is: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,13}$/i
      //   },
      // },
      phone: {
        type: DataTypes.STRING(15),
        allowNull: false,
        // unique: true,
        validate: {
          len: [11, 13],
          is: /^\d+$/i,
        },
      },
      secret: {
        type: DataTypes.STRING(60),
        allowNull: false,
      },
      uId: {
        type: DataTypes.STRING(12),
        allowNull: false,
        unique: true,
      },
      state: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
      type: {
        type: DataTypes.STRING(15),
        allowNull: false,
      },
    },
    {
      underscored: true,
    }
  );

  AdminLogin.associate = function (models) {
    AdminLogin.belongsTo(models.AdminType, {
      foreignKey: "type",
      targetKey: "type",
    });

    AdminLogin.hasOne(models.AdminProfile);
  };

  return AdminLogin;
};
