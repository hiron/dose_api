"use strict";
module.exports = (sequelize, DataTypes) => {
  const FeedBack = sequelize.define(
    "ClientFeedback",
    {
      clientId: DataTypes.INTEGER,
      subject: {
        type: DataTypes.TEXT("tiny"), //DataTypes.TEXT,
        validate: {
          len: { args: [0, 255], msg: "Review is exited the max limit" },
        },
      },
      body:{
          type: DataTypes.STRING(1234),
      }
    },
    {
      underscored: true,
    }
  );
  FeedBack.associate = function (models) {
    FeedBack.belongsTo(models.UserClient, {
      foreignKey: "clientId",
    });
  };
  return FeedBack;
};
