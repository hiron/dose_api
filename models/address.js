'use strict';
module.exports = (sequelize, DataTypes) => {
  const Address = sequelize.define('Address', {
    lat: {
      type: DataTypes.FLOAT(4,8),
      allowNull: false
    },
    long: {
      type: DataTypes.FLOAT(4,8),
      allowNull: false
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phone: {
      type: DataTypes.STRING(15),
      allowNull: false
    },
    name: DataTypes.STRING(60),
    label: DataTypes.STRING(60)
  }, {
    underscored: true
  });

  Address.associate = function (models) {
    Address.belongsTo(models.Login, {
      foreignKey: "userId",
      targetKey: "uId"
    });
  };
  return Address;
};

