module.exports = (sequelize, DataTypes) => {
  const Prescription = sequelize.define(
    "Prescription",
    {
      image: DataTypes.STRING,
      clientId: DataTypes.STRING(12),
      invoiceId: {
        type: DataTypes.STRING(8),
        allowNull: true,
        defaultValue: null,
      },
    },
    {
      underscored: true,
    }
  );
  Prescription.associate = function (models) {
    Prescription.belongsTo(models.Login, {
      foreignKey: "clientId",
      targetKey: "uId",
    });

    Prescription.belongsTo(models.Invoice, {
      foreignKey: "invoiceId",
      targetKey: "invoiceId",
    });
  };

  return Prescription;
};
