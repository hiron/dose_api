"use strict";
module.exports = (sequelize, DataTypes) => {
  const UserType = sequelize.define(
    "UserType",
    {
      type: { type: DataTypes.STRING(8), allowNull: false, unique: true },
    },
    { underscored: true, timestamps: false }
  );
  UserType.associate = function (models) {
    UserType.hasMany(models.Login, {
      foreignKey: "type",
      targetKey: "type",
    });
  };
  return UserType;
};
