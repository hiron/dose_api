"use strict";
const moment = require('moment');
module.exports = (sequelize, DataTypes) => {
  const UserClient = sequelize.define(
    "UserClient", {
      firstName: {
        type: DataTypes.STRING(20),
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      lastName: {
        type: DataTypes.STRING(20),
      },
      // phone: {
      //   type: DataTypes.STRING(15),
      //   allowNull: false,
      //   unique: true,
      //   validate: {
      //     notEmpty: true,
      //   },
      // },
      email: {
        type: DataTypes.STRING(60),
        allowNull: true,
        unique: true,
        validate: {
          notEmpty: true,
          isEmail: {
            msg: "Please insert a valid email",
          },
        },
      },
      emailStatus: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      birthDate: {
        type: DataTypes.DATEONLY,
        get() {
          return this.getDataValue('birthDate') == null ? null : moment.utc(this.getDataValue('birthDate')).format('YYYY-MM-DD');
        }
      },
      sex: {
        type: DataTypes.STRING(12),
      },
      nid: {
        type: DataTypes.BIGINT,
      },
      level: {
        type: DataTypes.INTEGER,
        defaultValue: 1,
        validate:{
          isIn: [[1,2,3,4]],
        }
      },
      clientId: {
        type: DataTypes.STRING(12),
        unique: true,
        allowNull: true,
      },
      age: {
        type: DataTypes.VIRTUAL,
        get() {
          return this.getDataValue('birthDate') == null ? null : Math.abs(
            new Date(
              Date.now() - new Date(this.getDataValue("birthDate")).getTime()
            ).getUTCFullYear() - 1970
          );
        },
      },
    }, {
      underscored: true,
    }
  );
  UserClient.associate = function (models) {
    UserClient.belongsTo(models.Login, {
      foreignKey: "clientId",
      targetKey: "uId",
    });

    UserClient.hasMany(models.Invoice, {
      foreignKey: "clientId",
      targetKey: "clientId",
    });

    UserClient.belongsTo(models.DiscountRule, {
      foreignKey: "level",
    });
  };
  return UserClient;
};