"use strict";
module.exports = (sequelize, DataTypes) => {
  const DiscountRule = sequelize.define(
    "DiscountRule",
    {
      type: {
        type: DataTypes.STRING(8),
        allowNull: false,
      },
      level: {
        type: DataTypes.STRING(10),
        allowNull: false,
        defaultValue: "regular",
      },
      delivery:{
        type: DataTypes.FLOAT,
        defaultValue: 30
      },
      // limit: {
      //   type: DataTypes.FLOAT,
      //   allowNull: false,
      //   defaultValue: 0
      // },
      discount: {
        type: DataTypes.FLOAT,
        validate: {
          max: {
            args: 100,
            msg: "value should not be greater then 100",
          },
          min: {
            args: 0,
            msg: "value should not be lesser then 0",
          },
        },
      },
    },
    {
      underscored: true,
      timestamps: false,
    }
  );

  DiscountRule.associate = function (models) {
    DiscountRule.belongsTo(models.UserType, {
      foreignKey: "type",
      targetKey: "type",
    });
  };

  return DiscountRule;
};
