'use strict';
module.exports = (sequelize, DataTypes) => {
  const Address = sequelize.define('OfficeAddress', {
    lat: {
      type: DataTypes.FLOAT(4,8),
      allowNull: false
    },
    long: {
      type: DataTypes.FLOAT(4,8),
      allowNull: false
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phone: {
      type: DataTypes.STRING,
    },
    email: DataTypes.STRING(60)
  }, {
    underscored: true
  });
  return Address;
};

