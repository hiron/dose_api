"use strict";
var customId = require("custom-id");

module.exports = (sequelize, DataTypes) => {
  const Statement = sequelize.define(
    "Statement",
    {
      doseAmount: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      agentAmount: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      codAmmount: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      bankAmmount: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      discount: DataTypes.INTEGER,
      agentDiscountAmount: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      doseDiscountAmount: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      openDate: DataTypes.DATE,
      closeDate: DataTypes.DATE,
      statementId: {
        type: DataTypes.STRING(8),
        unique: true,
        allowNull: false,
      },
    },
    {
      underscored: true,
    }
  );
  Statement.associate = function (models) {
    Statement.belongsTo(models.UserAgent, {
      foreignKey: "agentId",
    });
    Statement.hasMany(models.Account, {
      foreignKey: "statementId",
      targetKey: "statementId",
    });
  };

  Statement.beforeCreate((statement, options) => {
    statement.statementId = customId({
      name: "statement",
    });
  });

  return Statement;
};
