'use strict';
module.exports = (sequelize, DataTypes) => {
  const SmartCart = sequelize.define('SmartCart', {
    productId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    quantity: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    label: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    clientId: {
      type: DataTypes.STRING(12),
      allowNull: false
    }
  }, {
    underscored: true,
  });
  SmartCart.associate = function (models) {

    SmartCart.belongsTo(models.Product, {
      foreignKey: 'productId',
    });

    SmartCart.belongsTo(models.Login, {
      foreignKey: 'clientId',
      targetKey: 'uId'
    })
  };
  return SmartCart;
};