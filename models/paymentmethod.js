"use strict";
module.exports = (sequelize, DataTypes) => {
  const PaymentMethod = sequelize.define(
    "PaymentMethod",
    {
      method: { type: DataTypes.STRING(20), unique: true, allowNull: false },
      img: { type: DataTypes.STRING(100) },
    },
    {
      underscored: true,
      timestamps: false,
    }
  );
  PaymentMethod.associate = function (models) {
    PaymentMethod.hasMany(models.Invoice, {
      foreignKey: "paymentMethod",
      targetKey: "method",
    });
  };
  return PaymentMethod;
};
