"use strict";
module.exports = (sequelize, DataTypes) => {
  const AdminType = sequelize.define(
    "AdminType",
    {
      type: { type: DataTypes.STRING(15), allowNull: false, unique: true },
    },
    { underscored: true, timestamps: false }
  );

  AdminType.associate = function (models) {
    AdminType.belongsToMany(models.SubMenu, {
      through: "C",
      as: "subMenu",
    });
  };

  return AdminType;
};
