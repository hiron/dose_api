"use strict";
module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define(
    "Product",
    {
      manufacturer: { type: DataTypes.STRING(60) },
      name: { type: DataTypes.STRING(60), allowNull: false },
      // group: { type: DataTypes.STRING(100), allowNull: false },
      group: { type: DataTypes.STRING, allowNull: false },
      strength: { type: DataTypes.STRING(80) },
      price: { type: DataTypes.FLOAT },
      type: { type: DataTypes.STRING(30) },
      prescription: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      keyword: { type: DataTypes.STRING },
      saleableUnit: {
        type: DataTypes.INTEGER(3).UNSIGNED,
        allowNull: false,
        defaultValue: 1,
      },
      maxLimit: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        //allowNull: false,
        defaultValue: null,
      },
    },
    {
      underscored: true,
      indexes: [
        {
          //unique: true,
          fields: ["name"],
        },
        {
          //unique: true,
          fields: ["group"],
        },
      ],
    }
  );
  Product.associate = function (models) {
    // associations can be defined here
  };
  return Product;
};
