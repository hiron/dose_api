"use strict";
module.exports = (sequelize, DataTypes) => {
  const ShippingAddress = sequelize.define(
    "ShippingAddress",
    {
      lat: {
        type: DataTypes.FLOAT(4, 8),
        allowNull: false,
      },
      long: {
        type: DataTypes.FLOAT(4, 8),
        allowNull: false,
      },
      address: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      phone: {
        type: DataTypes.STRING(15),
        allowNull: false,
      },
      name: DataTypes.STRING(60),
    },
    {
      underscored: true,
    }
  );

  ShippingAddress.associate = function (models) {
    ShippingAddress.belongsTo(models.Login, {
      foreignKey: "userId",
      targetKey: "uId",
    });
  };
  return ShippingAddress;
};
