'use strict';
module.exports = (sequelize, DataTypes) => {
  const Menu = sequelize.define('Menu', {
    icon: {
      type: DataTypes.STRING(6),
    },
    muiIcon: {
      type: DataTypes.STRING(6),
    },
    menuName: {
      type: DataTypes.STRING(20),
      allowNull: false,
      unique: true,
    },
    
  }, {
    underscored: true,timestamps: false
  });
  Menu.associate = function (models) {

    Menu.hasMany(models.SubMenu);

    // Cart.belongsTo(models.Login, {
    //   foreignKey: 'clientId',
    //   targetKey: 'uId'
    // })
  };
  return Menu;
};