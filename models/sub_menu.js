'use strict';
module.exports = (sequelize, DataTypes) => {
  const SubMenu = sequelize.define('SubMenu', {
    path: {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: true,
    },
    subMenuName: {
      type: DataTypes.STRING(30),
      allowNull: false,
    },
  }, {
    underscored: true,timestamps: false
  });
  SubMenu.associate = function (models) {

    SubMenu.belongsTo(models.Menu);

    SubMenu.belongsToMany(models.AdminType, {
        through: 'C', as:'belongsTo'
    });
  };
  return SubMenu;
};