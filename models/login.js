"use strict";

module.exports = (sequelize, DataTypes) => {
  const Login = sequelize.define(
    "Login",
    {
      phone: {
        type: DataTypes.STRING(15),
        allowNull: false,
        unique: true,
        validate: {
          len: [11, 13],
          is: /^\d+$/i
        },
      },
      secret: {
        type: DataTypes.STRING(32),
        allowNull: false
      },
      uId: {
        type: DataTypes.STRING(12),
        allowNull: false,
        unique: true,
      },
      state: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
      type:{
        type: DataTypes.STRING(8),
        allowNull: false
      }
    },
    {
      underscored: true,
    }
  );
  Login.associate = function (models) {
    Login.belongsTo(models.UserType, {
      foreignKey: "type",
      targetKey: "type",
    });
    
    // Login.hasMany(models.Invoice, {
    //   foreignKey: "agentId",
    //   targetKey: "uId",
    // });
  };

  // Login.beforeCreate((user, options) => {
  //   user.uId = uniqid.process();
  //   user.secret = speakeasy.generateSecret().ascii;
  // });
  return Login;
};
