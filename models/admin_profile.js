"use strict";

module.exports = (sequelize, DataTypes) => {
  const AdminProfile = sequelize.define(
    "AdminProfile",
    {
      firstName: {
        type: DataTypes.STRING(15),
      },
      lastName: {
        type: DataTypes.STRING(15),
      },
      nid: {
        type: DataTypes.STRING(15),
      },
      address: {
        type: DataTypes.STRING,
      },
      email: {
        type: DataTypes.STRING(60),
        unique: true,
        validate: {
          notEmpty: true,
          isEmail: {
            msg: "Please insert a valid email",
          },
        },
      },
      photo:{
        type: DataTypes.BLOB
      }
    },
    {
      underscored: true,
    }
  );

  AdminProfile.associate = function (models) {
    AdminProfile.belongsTo(models.AdminLogin);
  };

  return AdminProfile;
};
