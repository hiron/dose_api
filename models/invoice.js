"use strict";

var customId = require("custom-id");

module.exports = (sequelize, DataTypes) => {
  const Invoice = sequelize.define(
    "Invoice",
    {
      paymentMethod: {
        type: DataTypes.STRING(20),
        allowNull: true,
      },
      paymentType: {
        type: DataTypes.STRING(20),
        allowNull: false,
        validate: {
          isIn: [["prepay", "postpay"]],
        },
      },
      txnId: {
        type: DataTypes.STRING(20),
      },
      price: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      discountPrice: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      deliveryCharge: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      // prescription: {
      //   type: DataTypes.STRING,
      // },
      discount: {
        type: DataTypes.FLOAT,
        validate: {
          max: 100,
          min: 0,
        },
      },
      doseCharge: {
        type: DataTypes.FLOAT,
      },
      agentCharge: {
        type: DataTypes.FLOAT,
      },
      agentCommission: {
        type: DataTypes.FLOAT,
      },
      doseCommission: {
        type: DataTypes.FLOAT,
      },
      deliveredAt: DataTypes.DATE,
      confirmedAt: DataTypes.DATE,
      addressId: DataTypes.INTEGER,
      done: { type: DataTypes.BOOLEAN, defaultValue: false },
      cancel: { type: DataTypes.BOOLEAN, defaultValue: false },
      cancelBy: DataTypes.STRING(12),
      invoiceId: {
        type: DataTypes.STRING(8),
        unique: true,
        allowNull: false,
      },
      clientId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      agentId: DataTypes.INTEGER,
    },
    {
      underscored: true,
    }
  );
  Invoice.associate = function (models) {
    Invoice.belongsTo(models.PaymentMethod, {
      foreignKey: "paymentMethod",
      targetKey: "method",
    });
    Invoice.belongsTo(models.ShippingAddress, {
      foreignKey: "addressId",
      as: "Address",
    });
    Invoice.belongsTo(models.CancelCause);
    Invoice.belongsTo(models.Login, {
      foreignKey: "cancelBy",
      targetKey: "uId",
    });
    Invoice.belongsTo(models.UserClient, {
      foreignKey: "clientId",
    });
    Invoice.belongsTo(models.UserAgent, {
      foreignKey: "agentId",
    });
    Invoice.hasMany(models.Order);

    Invoice.hasMany(models.Rating);
    Invoice.hasMany(models.Prescription, {
      foreignKey: "invoiceId",
      sourceKey: "invoiceId",
    });
  };

  Invoice.afterCreate(async (invoice) => {
    // console.log(invoice);
    // invoice.doseCharge = await invoice.deliveryCharge - invoice.agentCharge;
    // invoice.doseCommission = await invoice.discountPrice - invoice.agentCommission;
    await Invoice.update(
      {
        doseCharge: invoice.deliveryCharge - invoice.agentCharge,
        doseCommission: invoice.discountPrice - invoice.agentCommission,
      },
      { where: { id: invoice.id } }
    );
  });

  // Invoice.beforeCreate(async (invoice, options) => {
  //   invoice.invoiceId = await customId({
  //     name: "invoice"
  //   });
  // });

  return Invoice;
};
