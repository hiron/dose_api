"use strict";
module.exports = (sequelize, DataTypes) => {
  const Order = sequelize.define(
    "Order",
    {
      quantity: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      price: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      clientId: {
        type: DataTypes.STRING(12),
        allowNull: false,
      },
      productId: { type: DataTypes.INTEGER },
      invoiceId: { type: DataTypes.INTEGER },
    },
    {
      underscored: true,
      // defaultScope: {
      //   attributes: { exclude: ["id"] },
      // },
    }
  );
  Order.associate = function (models) {
    Order.belongsTo(models.Product, {
      targetKey: "id",
      foreignKey: "productId",
    });

    Order.belongsTo(models.Login, {
      foreignKey: "clientId",
      targetKey: "uId",
    });

    Order.belongsTo(models.Invoice, {
      targetKey: "id",
      foreignKey: "invoiceId",
    });
  };
  return Order;
};
