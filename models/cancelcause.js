'use strict';
module.exports = (sequelize, DataTypes) => {
  const CancelCause = sequelize.define('CancelCause', {
    cause: DataTypes.STRING(50),
    type: DataTypes.STRING(8)
  }, {
    underscored: true,
    timestamps: false,
  });
  CancelCause.associate = function(models) {
    CancelCause.belongsTo(models.UserType, {
      foreignKey: "type",
      targetKey: "type",
    });
  };
  return CancelCause;
};