"use strict";
const moment = require('moment');
module.exports = (sequelize, DataTypes) => {
  const UserAgent = sequelize.define(
    "UserAgent",
    {
      pharmacy: { type: DataTypes.STRING(60), allowNull: true },
      openingTime: { type: DataTypes.TIME, allowNull: true },
      closingTime: { type: DataTypes.TIME, allowNull: true },
      state: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
      firstName: {
        type: DataTypes.STRING(20),
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      lastName: {
        type: DataTypes.STRING(20),
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      // phone: {
      //   type: DataTypes.STRING(15),
      //   allowNull: false,
      //   unique: true,
      //   validate: {
      //     notEmpty: true,
      //   },
      // },
      email: {
        type: DataTypes.STRING(60),
        allowNull: false,
        unique: true,
        validate: {
          notEmpty: true,
          isEmail: {
            msg: "Please insert a valid email",
          },
        },
      },
      emailStatus: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      level: {
        type: DataTypes.INTEGER,
        defaultValue: 5,
        validate:{
          isIn: [[5,6,7,8]],
        }
      },
      addressId: DataTypes.INTEGER,
      nid: { type: DataTypes.INTEGER, allowNull: true },
      tradeLicense: { type: DataTypes.STRING(60), allowNull: true },
      drugLicense: { type: DataTypes.STRING(60), allowNull: true },
      //shopRegistration: { type: DataTypes.STRING(60), allowNull: false },
      birthDate: {
        type: DataTypes.DATEONLY,
        get() {
          return this.getDataValue('birthDate') == null ? null : moment.utc(this.getDataValue('birthDate')).format('YYYY-MM-DD');
        }
      },
      sex: { type: DataTypes.STRING(12) },
      limit: { type: DataTypes.INTEGER, defaultValue: 5 },
    },
    { underscored: true }
  );
  UserAgent.associate = function (models) {
    UserAgent.belongsTo(models.Login, {
      foreignKey: "agentId",
      targetKey: "uId",
    });
    UserAgent.hasMany(models.Invoice, {
      foreignKey: "agentId",
      targetKey: "agentId",
    });

    UserAgent.belongsTo(models.ShopAddress, {
      foreignKey: "addressId",
    });
    UserAgent.belongsTo(models.DiscountRule, {
      foreignKey: "level",
    });
  };
  return UserAgent;
};
