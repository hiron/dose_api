'use strict';
module.exports = (sequelize, DataTypes) => {
  const Cart = sequelize.define('Cart', {
    productId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    quantity: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    clientId: {
      type: DataTypes.STRING(12),
      allowNull: false
    }
  }, {
    underscored: true,
  });
  Cart.associate = function (models) {

    Cart.belongsTo(models.Product, {
      foreignKey: 'productId',
    });

    Cart.belongsTo(models.Login, {
      foreignKey: 'clientId',
      targetKey: 'uId'
    })
  };
  return Cart;
};