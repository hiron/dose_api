"use strict";
module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define(
    "Account", {
      invoiceId: {
        type: DataTypes.STRING(8),
        unique: true,
        allowNull: false
      },
      statementId: {
        type: DataTypes.STRING(8)
      },
      paymentMethod: {
        type: DataTypes.STRING(20),
        allowNull: false
      },
      doseAmount: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      agentAmount: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
    }, {
      underscored: true,
    }
  );
  Account.associate = function (models) {
    Account.belongsTo(models.UserAgent, {
      foreignKey: "agentId",
    });
    Account.belongsTo(models.UserClient, {
      foreignKey: "clientId",
    });
    Account.belongsTo(models.Invoice, {
      foreignKey: 'invoiceId',
      targetKey: 'invoiceId'
    });

    Account.belongsTo(models.Statement, {
      foreignKey: 'statementId',
      targetKey: 'statementId'
    });
    Account.belongsTo(models.PaymentMethod, {
      foreignKey: "paymentMethod",
      targetKey: "method",
    });
  };
  return Account;
};