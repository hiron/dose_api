"use strict";
module.exports = (sequelize, DataTypes) => {
  const Rating = sequelize.define(
    "Rating",
    {
      clientId: DataTypes.INTEGER,
      agentId: DataTypes.INTEGER,
      rate: {
        type: DataTypes.INTEGER,
        validate: {
          max: 5,
          min: 1,
        },
      },
      detail: {
        type: DataTypes.TEXT("tiny"), //DataTypes.TEXT,
        validate: {
          len: { args: [0, 255], msg: "Review is exited the max limit" },
        },
      },
    },
    {
      underscored: true,
    }
  );
  Rating.associate = function (models) {
    Rating.belongsTo(models.UserAgent, {
      foreignKey: "agentId",
    });
    Rating.belongsTo(models.UserClient, {
      foreignKey: "clientId",
    });
    Rating.belongsTo(models.Invoice,{
      foreignKey: "invoiceId"
    });
  };
  return Rating;
};
