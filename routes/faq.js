module.exports = (app) => {
  app
    .route("/faq")
    .all(app.auth.authenticate("all"))
    /**
     * 
     * @api {get} /faq FAQ of the Dose
     * @apiName faq
     * @apiGroup FAQ
     * @apiVersion  1.0.0
     * 
     * @apiSuccess (200) {number} id id of the faq
     * @apiSuccess (200) {String} question Questions of the
     * @apiSuccess (200) {String} answer Answer of the questions
     * 
     * 
     * 
     * @apiSuccessExample {json} Success-Response:
     *[
    {
        "id": 1,
         "order": 1,
        "question": "What is Dose?",
        "answer": "Ut irure dolore est dolor ex non culpa magna nulla ut minim commodo ullamco qui. Dolore ut est et velit exercitation ad culpa sit est deserunt occaecat ipsum. Amet id nulla aliquip et exercitation Lorem ex elit elit occaecat eu. Et consequat labore sunt duis sit tempor. Labore cupidatat adipisicing reprehenderit anim dolore id eu veniam. Laboris pariatur sint veniam sint eiusmod ea dolor mollit aliqua eiusmod occaecat.",
        "createdAt": "2020-08-28T18:00:00.000Z",
        "updatedAt": "2020-08-28T18:00:00.000Z"
    },
    {
        "id": 2,
         "order": 2,
        "question": "How can I get Offer from Dose",
        "answer": "Ut irure dolore est dolor ex non culpa magna nulla ut minim commodo ullamco qui. Dolore ut est et velit exercitation ad culpa sit est deserunt occaecat ipsum. Amet id nulla aliquip et exercitation Lorem ex elit elit occaecat eu. Et consequat labore sunt duis sit tempor. Labore cupidatat adipisicing reprehenderit anim dolore id eu veniam. Laboris pariatur sint veniam sint eiusmod ea dolor mollit aliqua eiusmod occaecat.",
        "createdAt": "2020-08-28T18:00:00.000Z",
        "updatedAt": "2020-08-28T18:00:00.000Z"
    },
    {
        "id": 3,
         "order": 3,
        "question": "What can it provide ua pandemic?",
        "answer": "Ut irure dolore est dolor ex non culpa magna nulla ut minim commodo ullamco qui. Dolore ut est et velit exercitation ad culpa sit est deserunt occaecat ipsum. Amet id nulla aliquip et exercitation Lorem ex elit elit occaecat eu. Et consequat labore sunt duis sit tempor. Labore cupidatat adipisicing reprehenderit anim dolore id eu veniam. Laboris pariatur sint veniam sint eiusmod ea dolor mollit aliqua eiusmod occaecat.",
        "createdAt": "2020-08-28T18:00:00.000Z",
        "updatedAt": "2020-08-28T18:00:00.000Z"
    }]
     * 
     * 
     */
    .get((req, res) => {
      app.db.faq
        .findAll({ order: ["order"] })
        .then((d) => res.json(d))
        .catch((e) => res.status(401).json({ msg: e.message }));
    })
    /**
     * 
     * @api {post} /faq Add new FAQ 
     * @apiName insert faq
     * @apiGroup FAQ
     * @apiVersion  1.0.0
     * 
     * @apiPrivate
     * @apiPermission admin
     *   
     * @apiParam  {String} question Questions of the
     * @apiParam  {String} answer Answer of the questions
     * 
     * @apiSuccess (200) {number} id id of the faq
     * @apiSuccess {number} order Order of the faq
     * @apiSuccess (200) {String} question Questions of the
     * @apiSuccess (200) {String} answer Answer of the questions
     * 
     * 
     * 
     * @apiSuccessExample {json} Success-Response:
    {
        "id": 1,
         "order": 1,
        "question": "What is Dose?",
        "answer": "Ut irure dolore est dolor ex non culpa magna nulla ut minim commodo ullamco qui. Dolore ut est et velit exercitation ad culpa sit est deserunt occaecat ipsum. Amet id nulla aliquip et exercitation Lorem ex elit elit occaecat eu. Et consequat labore sunt duis sit tempor. Labore cupidatat adipisicing reprehenderit anim dolore id eu veniam. Laboris pariatur sint veniam sint eiusmod ea dolor mollit aliqua eiusmod occaecat.",
        "createdAt": "2020-08-28T18:00:00.000Z",
        "updatedAt": "2020-08-28T18:00:00.000Z"
    }

    * @apiParamExample {json} Param-Response:
    {
        "question": "What is Dose?",
        "answer": "Ut irure dolore est dolor ex non culpa magna nulla ut minim commodo ullamco qui. Dolore ut est et velit exercitation ad culpa sit est deserunt occaecat ipsum. Amet id nulla aliquip et exercitation Lorem ex elit elit occaecat eu. Et consequat labore sunt duis sit tempor. Labore cupidatat adipisicing reprehenderit anim dolore id eu veniam. Laboris pariatur sint veniam sint eiusmod ea dolor mollit aliqua eiusmod occaecat.",
    },
 
 
     * 
     * 
     */
    .all(app.adminAccess.of("FAQ"))
    .post(async (req, res) => {
      let lastRecord = await app.db.faq.findOne({
        order: [["order", "DESC"]],
        limit: 1,
      });

      app.db.faq
        .create({
          ...req.body,
          order: !!lastRecord ? lastRecord.order + 1 : 1,
        })
        .then((faq) => res.json(faq))
        .catch((e) => res.status(401).json({ msg: e.message }));
    })
    /**
     * 
     * @api {put} /faq Update FAQ 
     * @apiName update faq
     * @apiGroup FAQ
     * @apiVersion  1.0.0
     * 
     * @apiPrivate
     * @apiPermission admin
     * 
     * @apiParam  {number} faqId id of the faq
     * @apiParam  {String} question Questions of the
     * @apiParam  {String} answer Answer of the questions
     * 
     * @apiSuccess (200) {number} id id of the faq
     * @apiSuccess {number} order Order of the faq
     * @apiSuccess (200) {String} question Questions of the
     * @apiSuccess (200) {String} answer Answer of the questions
     * 
     * 
     * 
     * @apiSuccessExample {json} Success-Response:
    {
        "id": 1,
        "order": 1,
        "question": "What is Dose?",
        "answer": "Ut irure dolore est dolor ex non culpa magna nulla ut minim commodo ullamco qui. Dolore ut est et velit exercitation ad culpa sit est deserunt occaecat ipsum. Amet id nulla aliquip et exercitation Lorem ex elit elit occaecat eu. Et consequat labore sunt duis sit tempor. Labore cupidatat adipisicing reprehenderit anim dolore id eu veniam. Laboris pariatur sint veniam sint eiusmod ea dolor mollit aliqua eiusmod occaecat.",
        "createdAt": "2020-08-28T18:00:00.000Z",
        "updatedAt": "2020-08-28T18:10:00.000Z"
    }

    * @apiParamExample {json} Param-Response:
    {
        "faqId":1,
        "question": "What is Dose?",
        "answer": "Ut irure dolore est dolor ex non culpa magna nulla ut minim commodo ullamco qui. Dolore ut est et velit exercitation ad culpa sit est deserunt occaecat ipsum. Amet id nulla aliquip et exercitation Lorem ex elit elit occaecat eu. Et consequat labore sunt duis sit tempor. Labore cupidatat adipisicing reprehenderit anim dolore id eu veniam. Laboris pariatur sint veniam sint eiusmod ea dolor mollit aliqua eiusmod occaecat.",
    },
 
     * 
     * 
     */
    .put((req, res) => {
      app.db.faq
        .findOne({
          where: { id: req.body.faqId },
        })
        .then((record) => {
          Object.keys(req.body).forEach((d) => (record[d] = req.body[d]));
          return record.save();
        })
        .then((data) => res.json(data))
        .catch((e) => res.status(401).json({ msg: e.message }));
    });
};
