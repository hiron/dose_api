module.exports = (app) => {
  app
    .route("/method")
    .all(app.auth.authenticate())
    /**
     * 
     * @api {Get} /method Get all available payment Methods
     * @apiName payment methods
     * @apiGroup Payment
     * @apiVersion  1.0.0
     * 
     * 
     * @apiHeader  {String} Authorization User Authorization Token
     * @apiHeader  {String} Content-Type Content type of the post document
     * 
     * @apiSuccess (200) {Number} id Id of the method
     * @apiSuccess (200) {String} method Availabel payment method
     * @apiSuccess (200) {String} img Image icon of the payment method
     * 
     *  @apiHeaderExample  {json} Header-Example:
     * {
     *     "Authorization" : "1234.vchdtr.xyer",
     *     "Content-Type": "application/json"
     * }
     * 
     * 
     * @apiSuccessExample {JSON} Success-Response:
     *[
    {
        "id": 1,
        "method": "bkash",
        "img": "images/d_06.png"
    },
    {
        "id": 2,
        "method": "rocket",
        "img": "images/d_09.png"
    }
  ]
     * 
     * 
     */
    .get((req, res) => {
      app.db.PaymentMethod.findAll({}).then((d) => res.json(d));
    });
};
