const { Op } = require("sequelize");

module.exports = (app) => {
  const Address = app.db.Address;
  app
    .route("/address")
    .all(app.auth.authenticate())
    /**
         * 
         * @api {get} /address Get all available addresses of user
         * @apiName Get Addresses
         * @apiGroup Address
         * @apiVersion  1.0.0
         * 
         * 
         * @apiHeader  {String} Content-type content type of the request
         * @apiHeader  {String} Authorization User's authentication token
         * 
         * @apiSuccess {Number} lat Latitude of the address
         * @apiSuccess {Number} long Longitude of the address
         * @apiSuccess {String} address Address detail of the home address
         * @apiSuccess {String} name Name of the recipient
         * @apiSuccess {String} phone Phone address of the recipient
         * @apiSuccess {String} label Label of the address
         * @apiSuccess {String} userId user's Id
         * @apiSuccess {Number} id Address id
         * 
         * @apiHeaderExample  {json} Header-Example:
         * {
         *     Content-Type : "aplication/json"
         *     Authorization : "JWT xyz.1234.wxyz"
         * }
         * 
         * 
         * @apiSuccessExample {json} Success-Response:
         * HTTP/1.1 200 Ok
         * [
             {
               "id": 1,
               "lat": 67.3467464,
               "long": 34.24437,
               "address": "Home flat info is included here",
               "phone": "01723948523",
               "name": "Hiron Das",
               "label": "Home",
              },
              {
                "id": 2,
                "lat": -67.3467464,
                "long": 134.24437,
                "address": "Home flat info is included here",
                "phone": "01723948523",
                "name": "Hiron Das",
                "label": "Office"
              }
         *  ]
         * 
         * @apiErrorExample {json} Authentication error:
         *  HTTP/1.1 401 Unauthorized
         * 
         */
    .get((req, res) => {
      Address.findAll({
        where: {
          userId: req.user.uId,
        },
        attributes:{exclude:['userId', "createdAt", 'updatedAt']}
      })
        .then((address) => res.json(address))
        .catch((err) =>
          res.status(401).json({
            msg: err.message,
          })
        );
    })
    /**
         * 
         * @api {post} /address Set address of the user
         * @apiName Set Address
         * @apiGroup Address
         * @apiVersion  1.0.0
         * 
         * 
         * @apiHeader  {String} Content-type content type of the request
         * @apiHeader  {String} Authorization User's authentication token
         * 
         * @apiParam {Number} lat Latitude of the address
         * @apiParam {Number} long Longitude of the address
         * @apiParam {String} address Address detail of the home address
         * @apiParam {String} name Name of the recipient
         * 
         * @apiParam {String} phone Phone address of the recipient
         * @apiParam {String} label Label of the address
         * 
         * @apiSuccess {Number} lat Latitude of the address
         * @apiSuccess {Number} long Longitude of the address
         * @apiSuccess {String} address Address detail of the home address
         * @apiSuccess {String} name Name of the recipient
         * @apiSuccess {String} phone Phone number of the recipient
         * @apiSuccess {String} label Label of the address
         * @apiSuccess {String} userId user's Id
         * @apiSuccess {Number} id Address id
         * 
         * @apiHeaderExample  {json} Header-Example:
         * {
         *     Content-Type : "aplication/json"
         *     Authorization : "JWT xyz.1234.wxyz"
         * }
         
         * @apiParamExample {json} Param-Request:
             {
               "lat": 67.3467464,
               "long": 34.24437,
               "address": "Home flat info is included here",
               "phone": "01723948523",
               "name": "Hiron Das",
               "label": "Home",
              }
         * 
         * @apiSuccessExample {json} Success-Response:
         * HTTP/1.1 200 Ok
            {
            "id": 1,
            "lat": 67.3467464,
            "long": 34.24437,
            "address": "Home flat info is included here",
            "phone": "01723948523",
            "name": "Hiron Das",
            "label": "Home",
            "createdAt": "2020-06-14T21:03:19.411Z",
            "updatedAt": "2020-06-14T21:03:19.411Z",
            "userId": "8dkkbfdydjp"
            }
         * 
         * @apiErrorExample {json} Not Acceptable error:
         *  HTTP/1.1 406 Not Acceptable
         * 
         */
    .post((req, res) => {
      Address.destroy({
        where: {
          [Op.and]: {
            userId: req.user.uId,
            label: req.body.label,
          },
        },
      })
        .then((d) => {
          console.log(d);
          return Address.create({
            userId: req.user.uId,
            lat: req.body.lat,
            long: req.body.long,
            address: req.body.address,
            phone: req.body.phone,
            name: req.body.name,
            label: req.body.label,
          });
        })
        .then((cart) => res.send(cart))
        .catch((err) =>
          res.json({
            msg: err.message,
          })
        );
    })
};
