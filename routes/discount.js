module.exports = (app) => {
  app
    .route("/discount")
    .all(app.auth.authenticate())
    /**
     * 
     * @api {get} /discount Discount of clients
     * @apiName discount for users
     * @apiGroup Discount
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {String} Authorization Authorization token of the user
     * 
     * @apiSuccess (200) {Number} id Id of the Discount
     * @apiSuccess (200) {String} type Client Type
     * @apiSuccess (200) {String} level Level of the discount
     * @apiSuccess (200) {String} delivery Delivery charge for this client
     * @apiSuccess (200) {String} discount Discount charge for this client
     * 
     * @apiParamExample  {JSON} Header-Example:
     * {
     *     Authorization : "xsdf.1234.serts"
     * }
     * 
     * 
     * @apiSuccessExample {JSON} Success-Response:
     * {
     * "id":1,
     * "type":"client",
     * "level":"regular",
     * "delivery":30,
     * "discount":0
     * }
     * 
     */
    .get((req, res) => {
      app.db.UserClient.findOne({
        where: { clientId: req.user.uId },
        include: [{ model: app.db.DiscountRule }],
        attributes: [],
      }).then((d) => res.json(d.DiscountRule));
    });
};
