module.exports = (app) => {
  app
    .route("/allorder")
    //.all(app.auth.authenticate())
    /**
     * 
     * @api {get} /order/:orderId Get current Order items
     * @apiName get detail Order
     * @apiGroup Order
     * @apiVersion  1.0.0
     * 
     * 
     * @apiHeader  {String} Authorization User's authentication token
     * 
     * @apiSuccess {Number} id order item id
     * @apiSuccess {Number} productId Medicine id
     * @apiSuccess {Number} quantity Medicine quantity
     * @apiSuccess {String} clientId client's uid 
     * @apiSuccess {String} invoiceId id of the invoice
     * @apiSuccess {Number} price Total Price of the product
     * 
     * @apiSuccess {String} manufacturer manufacturer of the medicine
     * @apiSuccess {String} name Brand name of the medicine
     * @apiSuccess {String} group Group name of the medicine
     * @apiSuccess {String} strength quantity pre unit of the medicine
     * @apiSuccess {Number} price Price per unit
     * @apiSuccess {String} type Medicine Type
     * @apiSuccess {Boolean} prescription=false Presciption is mandatory or not
     * @apiSuccess {String} keyword Keyword of the medicine
     * 
     * @apiHeaderExample  {JSON} Request-Header:
     * {
     *     "Authorization" : "JWT 1234.xyde.4563"
     * }
     * 
     * 
     * 
     * @apiSuccessExample {JSON} Success-Response:
     *  HTTP/1.1 200 Ok
     [
   {
      "id":1,
      "productId":300,
      "quantity":20,
      "clientId":"9wokbf7v0ex",
      "createdAt":"2020-07-04T09:39:13.366Z",
      "updatedAt":"2020-07-04T09:39:13.366Z",
      "price": 10.56,
      "invoiceId": 1,
      "Product":{
         "id":300,
         "manufacturer":"Benham Pharmaceuticals Ltd.",
         "name":"Beonac 50",
         "group":"Diclofenac Sodium",
         "strength":"50 mg",
         "price":0.6,
         "type":"Tablet",
         "prescription":false,
         "keyword":null,
         "createdAt":"2020-06-14T15:21:21.085Z",
         "updatedAt":"2020-06-14T15:21:21.085Z"
      }
   },
   {
      "id":2,
      "productId":500,
      "quantity":10,
      "clientId":"9wokbf7v0ex",
        "price": 34.4,
         "invoiceId": 1,
      "createdAt":"2020-07-04T09:39:26.301Z",
      "updatedAt":"2020-07-04T09:39:26.301Z",
      "Product":{
         "id":500,
         "manufacturer":"Novartis (Bangladesh) Ltd.",
         "name":"Servigesic",
         "group":"Paracetamol",
         "strength":"500 mg",
         "price":0.8,
         "type":"Tablet",
         "prescription":false,
         "keyword":null,
         "createdAt":"2020-06-14T15:21:21.086Z",
         "updatedAt":"2020-06-14T15:21:21.086Z"
      }
   }
]
     * 
     * 
     */
    .get((req, res) => {
      const stream = app.db.Invoice.findAllWithStream({
        batchSize: 10,
        isObjectMode: true,
        include: [
          { model: app.db.Order, include: [{ model: app.db.Product }] },
        ],
      });
      stream.pipe(res);

      // app.db.Invoice.findAll({
      //   include: [
      //     { model: app.db.Order, include: [{ model: app.db.Product }] },
      //   ],
      // })
      //   .then((d) => res.json(d))
      //   .catch((e) => res.status(401).json({ msg: e.message }));
    });
};
