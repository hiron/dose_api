module.exports = (app) => {
  app
    .route("/state")
    .all(app.auth.authenticate("admin-role"))
    .all(app.adminAccess.of("Add User"))
    /**
     * 
     * @api {post} /state Change the sate of Admin Profile
     * @apiName lock or unlocd account
     * @apiGroup Registration
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {String} adminId Id of the admin account
     * @apiParam  {Boolean} state State of the Account
     * 
     * @apiSuccess (200) {String} msg Admin account state message
     * 
     * @apiParamExample  {JSON} Request-Example:
     * {
          "adminId": 1,
          "state": true
      }
     * 
     * 
     * @apiSuccessExample {type} Success-Response:
     * {
          "msg": "The user is active"
      }
     * 
     * 
     */
    .post((req, res) => {
      app.db.AdminLogin.findOne({
        include: {
          model: app.db.AdminProfile,
          where: { id: req.body.adminId },
        },
      })
        .then((admin) => {
          if (!admin) throw Error("The adminId is wrong!!");
          admin.state = req.body.state;
          admin
            .save()
            .then((d) =>
              d.state
                ? res.json({ msg: "The user is active" })
                : res.json({ msg: "The user is locked" })
            );
        })
        .catch((err) => res.status(401).json({ msg: err.message }));
    });
};
