var speakeasy = require("speakeasy");
module.exports = (app) => {
  const Client = app.db.UserClient;
  const Login = app.db.Login;

  app
    .route("/email")
    .all(app.auth.authenticate())
    /**
     *
     * @api {get} /email send mail to Verify user's email
     * @apiName Verify email
     * @apiGroup User
     * @apiVersion  1.0.0
     *
     *
     * @apiHeader  {String} Authorization User's authentication token
     *
     * @apiSuccess {STRING} msg email send msg
     *
     * @apiHeaderExample  {json} Header:
     * {
     *     "Authorization" : "JWT 123.xutefe.abd.ettye"
     * }
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * {
     *     "msg" : "token send to client's email"
     * }
     *
     *
     */
    .get((req, res) => {
      Client.findOne({
        where: {
          clientId: req.user.uId,
        },
      })
        .then(async (user) => {
          // res.json(user.email);
          let login = await Login.findOne({
            where: {
              uId: user.clientId,
            },
          });
          return app.libs.util
            .sendMail({
              to: user.email,
              subject: "Email OTP",
              html: `<div>this is a test mail ${speakeasy.totp({
                secret: login.secret,
              })}</div>`,
            })
            .then((info) =>
              res.json({
                msg: "token send to client's email",
              })
            )
            .catch((err) =>
              res.status(403).json({
                msg: err.message,
              })
            );
        })
        .catch((err) => {
          res.status(401).json({
            msg: err.message,
          });
        });
    })
    /**
     *
     * @api {post} /email Validate user's email
     * @apiName confirm valification
     * @apiGroup User
     * @apiVersion  1.0.0
     *
     *
     * @apiHeader  {String} Authorization User's authentication token
     *
     * @apiHeader {STRING} Content-Type content type of the request
     *
     * @apiHeaderExample  {json} Header:
     * {
     *     "Authorization" : "JWT 123.xutefe.abd.ettye",
     *     "Content-Type": "application/json"
     * }
     *
     * @apiParam  {String} otp one time password get from email
     *
     * @apiSuccess {String} msg confirm msg
     *
     * @apiParamExample  {json} Request-Example:
     * {
     *     "otp" : "236423"
     * }
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 202 Accepted
     * {
     *     "msg" : "done"
     * }
     *
     *
     */
    .post((req, res) => {
      Login.findOne({
        where: {
          uId: req.user.uId,
        },
      })
        .then(async (login) => {
          const user = await Client.findOne({
            where: {
              clientId: login.uId,
            },
          });
          let verified = speakeasy.totp.verify({
            secret: login.secret,
            token: req.body.otp,
            window: 10,
          });

          if (verified) {
            user.emailStatus = true;
            await user.save();
            res.status(200).json({ msg: "OTP is matched!!" });
          } else {
            res.status(403).json({
              msg: "Otp is not matched!!!",
            });
          }
        })
        .catch((err) =>
          res.status(403).json({
            msg: err.message,
          })
        );
    });
};
