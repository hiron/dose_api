var QueryTypes = require("sequelize");

module.exports = (app) => {
  app
    .route("/suggestion")
    .all(app.auth.authenticate())
    /**
     * 
     * @api {get} /suggestion Get products/medicine detail suggestion
     * @apiName suggested product details 
     * @apiGroup Suggestion
     * @apiVersion  1.0.0
     * 
     * 
     * @apiHeader  {String} Authorization User's authorization token
     * 
     * @apiSuccess {Number} id Medicine id
     * @apiSuccess {String} manufacturer manufacturer of the medicine
     * @apiSuccess {String} name Brand name of the medicine
     * @apiSuccess {String} group Group name of the medicine
     * @apiSuccess {String} strength quantity pre unit of the medicine
     * @apiSuccess {Number} price Price per unit
     * @apiSuccess {String} type Medicine Type
     * @apiSuccess {Boolean} prescription=false Presciption is mandatory or not
     * @apiSuccess {String} keyword Keyword of the medicine
     * 
     * 
     * @apiHeaderExample  {JSON} Request-Example:
     * {
     *     "Authorization" : "JWT 1234.xyde.4563"
     * }
     * 
    * @apiSuccessExample {JSON} Success-Response:
     *  HTTP/1.1 200 Ok
      [{
         "id":300,
         "manufacturer":"Benham Pharmaceuticals Ltd.",
         "name":"Beonac 50",
         "group":"Diclofenac Sodium",
         "strength":"50 mg",
         "price":0.6,
         "type":"Tablet",
         "prescription":false,
         "keyword":null,
         "createdAt":"2020-06-14T15:21:21.085Z",
         "updatedAt":"2020-06-14T15:21:21.085Z"
      },
      {
         "id":30,
         "manufacturer":"Benham Pharmaceuticals Ltd.",
         "name":"Beonac 50",
         "group":"Diclofenac Sodium",
         "strength":"50 mg",
         "price":0.6,
         "type":"Tablet",
         "prescription":false,
         "keyword":null,
         "createdAt":"2020-06-14T15:21:21.085Z",
         "updatedAt":"2020-06-14T15:21:21.085Z"
      }]
   
     * 
     */
    .get((req, res) => {
      // app.db.Order.findAll({
      //   attributes: [
      //     [
      //       app.db.sequelize.fn("DISTINCT", app.db.sequelize.col("product_id")),
      //       "productId",
      //     ],
      //   ],
      //   limit: 10,
      //   include: [{ model: app.db.Product }],
      //   // group: ["productId"],
      //   order: [["updated_at", "DESC"]],
      //   where: { clientId: req.user.uId },
      // })
      //.aggregate("productId", "DISTINCT", { plain: false })
      app.db.sequelize
        .query(
          `SELECT DISTINCT ON ("product_id") "product_id", "Order"."updated_at", "Product"."id" AS "Product.id", "Product"."manufacturer" AS "Product.manufacturer", 
      "Product"."name" AS "Product.name", "Product"."group" AS "Product.group", 
      "Product"."strength" AS "Product.strength", "Product"."price" AS "Product.price", 
      "Product"."type" AS "Product.type", "Product"."prescription" AS "Product.prescription", 
      "Product"."keyword" AS "Product.keyword", "Product"."created_at" AS "Product.createdAt", 
      "Product"."updated_at" AS "Product.updatedAt" FROM "orders" AS "Order" 
      LEFT OUTER JOIN "products" 
     AS "Product" ON "Order"."product_id" = "Product"."id" 
     WHERE "Order"."client_id" = '${req.user.uId.toString()}' ORDER BY "product_id","Order"."updated_at" DESC LIMIT 10;
     `,
          {
            nest: true,
            type: QueryTypes.SELECT,
          }
        )
        .then((d) =>
          res.json(
            d.filter((d) => d.Product != undefined).map((d) => d.Product)
          )
        )
        .catch((error) => res.status(403).json(error));
    });
};
