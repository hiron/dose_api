const { Op } = require("sequelize");

module.exports = (app) => {
  app
    .route("/reorder")
    .all(app.auth.authenticate("admin-role"))
    /**
     * 
     * @api {post} /reorder Reorder FAQ Question
     * @apiName reorder faq
     * @apiGroup FAQ
     * @apiVersion  1.0.0
     * 
     * @apiPrivate
     * @apiPermission admin
     *   
     * @apiParam  {Number} faqId id of faq
     * @apiParam  {Number} oldOrder old order of faq
     * @apiParam  {Number} newOrder new Order of faq
     * 
     * 
     * 

    * @apiParamExample {json} Param-Response:
    {
    "faqId": 4,
    "oldOrder":8,
    "newOrder":5
}
     * 
     * 
     */
    .all(app.adminAccess.of("FAQ"))
    .post((req, res) => {
      app.db.faq
        .update(
          { order: 0 },
          {
            where: { id: req.body.faqId },
          }
        )
        .then((record) => {
          if (req.body.newOrder < req.body.oldOrder) {
            return app.db.faq
              .findAll({
                where: {
                  order: {
                    [Op.between]: [req.body.newOrder, req.body.oldOrder - 1],
                  },
                },
              })
              .then((data) => {
                data.forEach((d) => (d.order += 1));
                return Promise.all(data.map((d) => d.save()));
              });
          } else {
            return app.db.faq
              .findAll({
                where: {
                  order: {
                    [Op.between]: [req.body.oldOrder + 1, req.body.newOrder],
                  },
                },
              })
              .then((data) => {
                //console.log(data);
                data.forEach((d) => (d.order -= 1));
                return Promise.all(data.map((d) => d.save()));
              });
          }
        })
        .then((data) => {
          //console.log(data);
          return app.db.faq.update(
            { order: req.body.newOrder },
            {
              where: { id: req.body.faqId },
            }
          );
        })
        .then((faq) => res.json(faq))
        .catch((e) => res.status(403).json({ msg: e.message }));
    });
};
