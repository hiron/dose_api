const uniqid = require("uniqid");
const axios = require("axios");
const bcrypt = require("bcrypt");

module.exports = (app) => {
  const Login = app.db.AdminLogin;

  app
    .route("/password")
    .all(app.auth.authenticate("admin-role"))
    /**
     *
     * @api {post} /password Change password of admin
     * @apiName change password
     * @apiGroup Password
     * @apiVersion  1.0.0
     * @apiDescription Change user password
     * @apiPrivate
     * @apiPermission admin
     * @apiParam  {String} currentPassword Current password of Admin
     * @apiParam  {String} newPassword Admin's new Password
     *
     *
     *
     * @apiParamExample {JSON} Param-Response:
    {
       "currentPassword": 'Hiron@12345',
       "newPassword": 'test@13245'
    }

     @apiSuccessExample {Json} Success-resposnse:
     *HTTP/1.1 200 OK
     { "msg": "Password is successfully updated!!" }

     *
     *
     *  @apiErrorExample {json} Error-wrong phone or type:
     *  HTTP/1.1 403 Forbidden Error
     * { msg: "new password is not saved!!" }
     *
     * @apiErrorExample {json} Error-account lock:
     *  HTTP/1.1 401 Unauthorized Error
     * { msg: "Old password is not matched!!" }
     *
     * @apiErrorExample {json} Error-email send fail:
     *  HTTP/1.1 412 Precondition Failed
     *
     */

    .post((req, res) => {
      app.db.AdminLogin.findOne({
        where: { uId: req.user.uId, state: true },
      })
        .then((user) => {
          if (!user) throw new Error("The account is Locked!!");
          bcrypt.compareSync(req.body.currentPassword, user.secret)
            ? !!req.body.newPassword
              ? ((user.secret = bcrypt.hashSync(req.body.newPassword, 10)),
                user
                  .save()
                  .then((_) =>
                    res.json({ msg: "Password is successfully updated!!" })
                  ))
              : res.status(403).json({ msg: "new password is not saved!!" })
            : res.status(403).json({ msg: "Old password is not matched!!" });
        })
        .catch((err) => {
          res.status(401).json({ msg: err.message });
        });
    });
};
