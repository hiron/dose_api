module.exports = (app) => {
  app
    .route("/type")
    .all(app.auth.authenticate("admin-role"))
    .all(app.adminAccess.of("User Type"))
    /**
     * 
     * @api {get} /type Type of the admins
     * @apiName Get Admins Type
     * @apiGroup User Management
     * @apiVersion  1.0.0
     *  @apiPrivate
     * 
       * @apiHeader  {String} Authorization Admin's unique token
     * 
     * @apiSuccess (200) {String} type Name of the admin type
     * @apiSuccess (200) {Number} id id of the type 
     * @apiSuccess (200) {Number} subMenu count of the selected submenu
     * 
    * @apiHeaderExample  {json} Header:
        * {
        *     "Authorization" : "JWT 1234.xyde.4563"
        * }
     * 
     * 
     * @apiSuccessExample {json} Success-Response:
     *[
    {
        "id": 1,
        "type": "admin",
        "subMenu": 16
    },
    {
        "id": 2,
        "type": "account-admin",
        "subMenu": 0
    },
    {
        "id": 5,
        "type": "feedback-admin",
        "subMenu": 0
    },
    {
        "id": 4,
        "type": "customer-admin",
        "subMenu": 0
    },
    {
        "id": 3,
        "type": "agent-admin",
        "subMenu": 0
    }
]
     * 
     * 
     */
    .get((req, res) => {
      app.db.AdminType.findAll({
        include: { model: app.db.SubMenu, as: "subMenu" },
      })
        .then((data) => {
          //data[0].subMenu = data[0].subMenu.length;
          return JSON.parse(JSON.stringify(data)).map((d) => ({
            ...d,
            subMenu: d.subMenu.length,
          }));
        })
        .then((data) => res.json(data))
        .catch((err) => {
          res.status(401).json({ msg: err.message });
        });
    })
    /**
     *
     * @api {post} /type Add new type in admin type
     * @apiName Add Type
     * @apiGroup User Management
     * @apiVersion  1.0.0
     * @apiprivate
     *
     * @apiParam  {String} type type of the Admin
     *
     * @apiSuccess (200) {String} type type of the Admin
     *
     * @apiParamExample  {json} Request-Example:
     * {
     *     type : "admin-type"
     * }
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * {
     *     "type" : "admin-type",
     *      "id": 5
     * }
     *
     *
     */
    .post((req, res) => {
      console.log(req);
      app.db.AdminType.findOne({
        where: { type: req.body.type },
      })
        .then((value) =>
          value == null
            ? app.db.AdminType.create({ type: req.body.type })
                .then((data) => res.json(data))
                .catch((err) => {
                  res.status(401).json({ msg: err.message });
                })
            : res.json(value)
        )
        .catch((err) => res.status(403).json({ msg: err.message }));
    });
};
