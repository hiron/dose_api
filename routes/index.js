module.exports = (app) => {
  /**
   *
   * @api {GET} / status
   * @apiName Home
   * @apiGroup Home
   * @apiVersion  1.0.0
   *
   *
   * @apiSuccess  {String} dose Welcome message
   *
   * @apiSuccessExample  {json} Request-Example:
   * {
   *     "dose" : "Welcome to dose!!!"
   * }
   *
   *
   *
   */
  app.get("/", (req, res) => res.send({ dose: "Welcome to dose!!!" }));
};
