const { Op } = require("sequelize");
module.exports = (app) => {
  app
    .route("/faq/:id")
    .all(app.auth.authenticate("admin-role"))
    .all(app.adminAccess.of("FAQ"))
    /**
     * 
     * @api {delete} /faq/:id Delete FAQ 
     * @apiName Delete faq
     * @apiGroup FAQ
     * @apiVersion  1.0.0
     * 
     * @apiPrivate
     * @apiPermission admin
     * 
     * @apiParam  {number} id id of the faq

     * 
     * 
     */
    .delete(async (req, res) => {
      let deleteEntity = await app.db.faq.findOne({
        where: { id: req.params.id },
      });
      app.db.faq
        .destroy({ where: { id: req.params.id } })
        .then((d) => {
          console.log(d);
          console.log(deleteEntity.order);
          return app.db.faq.findAll({
            where: {
              order: {
                [Op.gt]: deleteEntity.order,
              },
            },
          });
        })
        .then((data) => {
          console.log(data);
          data.forEach((d) => (d.order -= 1));
          return Promise.all(data.map((d) => d.save()));
        })
        .then((d) => res.json(d))
        .catch((e) => res.status(402).json({ msg: e.message }));
    });
};
