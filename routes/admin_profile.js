const uniqid = require("uniqid");
const axios = require("axios");
const bcrypt = require("bcrypt");

module.exports = (app) => {
  const Login = app.db.AdminLogin;

  app
    .route("/profile")
    .all(app.auth.authenticate("admin-role"))
    //.all(app.adminAccess.of("Add User"))
    /**
     *
     * @api {get} /profile get a registared Admin profile
     * @apiName get admins profile
     * @apiGroup Profile
     * @apiVersion  1.0.0
     * @apiDescription get agmin's profile
     * @apiPrivate
     * @apiPermission admin
     * @apiSuccess  {String{..15}} username Admin's unique username
     * @apiSuccess  {String{..15}} firstname Admin's first name
     * @apiSuccess  {String{..15}} lastname Admin's last name
     * @apiSuccess  {String} email Admin's email
     * @apiSuccess  {String{8..15}} nid Admin's Password
     * @apiSuccess  {String} phone Admin's phone
     * @apiSuccess  {String=admin, account-admin, agent-admin, customer-admin} type Admin's type
     *
     * @apiSuccess {Boolean{true, false}} state the type of the user profile locked or open
     *
     *
     * @apiSuccessExample {JSON} Success-Response:
     * HTTP/1.1 200 OK
    {
        "id": 2,
        "firstName": "SuvasH",
        "lastName": "Das",
        "nid": null,
        "address": null,
        "email": "das.suvash09@gmail.com",
        "photo": null,
        "createdAt": "2021-07-08T11:02:57.354Z",
        "updatedAt": "2021-07-08T17:27:25.059Z",
        "AdminLoginId": 9,
        "AdminLogin": {
            "username": "suvash",
            "phone": "01739573213",
            "type": "admin"
        }
    }

     *
     *
     *  @apiErrorExample {json} Error-wrong phone or type:
     *  HTTP/1.1 403 Forbidden Error
     *
     * @apiErrorExample {json} Error-account lock:
     *  HTTP/1.1 401 Unauthorized Error
     *
     * @apiErrorExample {json} Error-email send fail:
     *  HTTP/1.1 412 Precondition Failed
     *
     */

    .get((req, res) => {
      console.log(req.user);
      Login.findOne({ where: { uId: req.user.uId } })
        .then((user) =>
          app.db.AdminProfile.findOne({
            where: { AdminLoginId: user.id },
            include: {
              model: Login,
              attributes: ["username", "phone", "type"],
              // include: {
              //   model: app.db.AdminType,
              // },
            },
          })
        )
        .then((user) => res.json(user))
        .catch((err) => res.status(401).json({ msg: err.message }));
    })
    /**
     *
     * @api {PUT} /profile Admin Profile update
     * @apiName update admin profile
     * @apiGroup Profile
     * @apiVersion  1.0.0
     * @apiDescription Update admin profile
     * @apiPrivate
     * @apiPermission admin
     * @apiParam  {Number} adminId Admin's id
     * @apiParam  {String} phone Admin's phone
     * @apiParam  {String} email Admin's email
     * @apiParam  {String} firstName Admin's first name
     * @apiParam  {String} lastName Admin's last name
     * @apiParam  {String} photo Admin's base64 photo
     * @apiParam  {String} nid Admin's nid
     * @apiParam  {String} address Admin's address
    
     *
     * @apiSuccess {String} msg phone acknowledge msg
     *
     * @apiParamExample  {JSON} Request-Example:
     * {
     *     "adminId":7,
     *     "phone" : "01874595676",
            "firstName":"Hiorn",
            "lastName":"Das",
            "email":"testmail@gmail.com",
            "nid":"12453556",
            "address":"Your Address Here",
            "photo": "base64 photo data"
        }
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     *  {
            "msg": "username & password is send to the phone"
        }
     *
     *
     *  @apiErrorExample {json} Error-wrong phone or type:
     *  HTTP/1.1 403 Forbidden Error
     *
     * @apiErrorExample {json} Error-account lock:
     *  HTTP/1.1 401 Unauthorized Error
     *
     * @apiErrorExample {json} Error-email send fail:
     *  HTTP/1.1 412 Precondition Failed
     *
     */
    .put(async (req, res) => {
      app.db.AdminLogin.findOne(
        //{ password: req.body.password, phone: req.body.phone },
        {
          include: {
            model: app.db.AdminProfile,
            where: { id: req.body.adminId },
          },
        }
      )
        .then(async (data) => {
          if (!data) throw new Error("No Record Found!!");

          Object.keys(req.body)
            .filter((d) => d != "username" && d != "password" && d != "type")
            .forEach((d) => {
              data[d] = req.body[d];
              data.AdminProfile[d] = req.body[d];
            });

          if (!!req.body.password)
            data.secret = bcrypt.hashSync(req.body.password, 10);
          //AdminProfile: req.body,
          await data.AdminProfile.save();
          data
            .save()
            // .then((user) =>
            //   !!req.body.password
            //     ? axios.get(
            //         `${process.env.otpHost}&receiver=${user.phone}&message=Your username is ${user.username} and the password is ${req.body.password} . Don't share it to others.`
            //       )
            //     : res.json({
            //         msg: "Profile is updated!!",
            //       })
            // )
            .then((_) =>
              res.json({
                msg: "Profile is updated!!",
              })
            )
            .catch((err) => res.status(401).json({ msg: err.message }));
        })
        .catch((err) => res.status(403).json({ msg: err.message }));
    });
};
