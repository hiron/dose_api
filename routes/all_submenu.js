module.exports = (app) => {
  app
    .route("/all-submenu")
    .all(app.auth.authenticate("admin-role"))
    .all(app.adminAccess.of("User Type"))
    /**
       * 
       * @api {get} /all-submenu get all submenu 
       * @apiDescription user get submenue for this Adminuser
       * @apiName allsubmenus
       * @apiGroup User Management
       * @apiPrivate
       * @apiVersion  1.0.0
       * @apiHeader  {String} Authorization Admin's unique token
       * 
       * @apiSuccess {Number} id id of the admin user
       * @apiSuccess {String} path route path of the menu
       * @apiSuccess {String} subMenuName sumMenu of the menu
       * 
       * @apiHeaderExample  {json} Header:
        * {
        *     "Authorization" : "JWT 1234.xyde.4563"
        * }
       *
       *
       * 
       * 
       * @apiSuccessExample {JSON} Success-Response:
       * HTTP/1.1 200 OK
       *
 [
    {
        "id": 1,
        "path": "/main/dashboard",
        "subMenuName": "Dashboard",
        "MenuId": 1
    },
    {
        "id": 2,
        "path": "/main/agent",
        "subMenuName": "Agent List",
        "MenuId": 2
    },
    {
        "id": 3,
        "path": "/main/request",
        "subMenuName": "Agent Pending Request",
        "MenuId": 2
    },
    {
        "id": 4,
        "path": "/main/clear",
        "subMenuName": "Clear Payment",
        "MenuId": 3
    },
    {
        "id": 5,
        "path": "/main/due",
        "subMenuName": "Due Payment",
        "MenuId": 3
    },
    {
        "id": 6,
        "path": "/main/expired",
        "subMenuName": "Expired Due",
        "MenuId": 3
    },
    {
        "id": 7,
        "path": "/main/order",
        "subMenuName": "Order List",
        "MenuId": 4
    },
    {
        "id": 8,
        "path": "/main/runing-order",
        "subMenuName": "On-going Order",
        "MenuId": 4
    },
    {
        "id": 9,
        "path": "/main/history",
        "subMenuName": "Order History",
        "MenuId": 4
    },
    {
        "id": 10,
        "path": "/main/client",
        "subMenuName": "Client List",
        "MenuId": 5
    },
    {
        "id": 11,
        "path": "/main/medicine",
        "subMenuName": "Medicine List",
        "MenuId": 6
    },
    {
        "id": 12,
        "path": "/main/feedback",
        "subMenuName": "Client Feedback",
        "MenuId": 7
    },
    {
        "id": 13,
        "path": "/main/faq",
        "subMenuName": "FAQ",
        "MenuId": 7
    },
    {
        "id": 14,
        "path": "/main/discount",
        "subMenuName": "Discount List",
        "MenuId": 8
    },
    {
        "id": 15,
        "path": "/main/type",
        "subMenuName": "User Type",
        "MenuId": 9
    },
    {
        "id": 16,
        "path": "/main/user",
        "subMenuName": "Add User",
        "MenuId": 9
    }
]
       * 
       */
    .get((req, res) => {
      //console.log(req);
      app.db.SubMenu.findAll()
        .then((menu) => res.json(menu))
        .catch((err) => {
          res.status(401).json({ msg: err.message });
        });
    });
};
