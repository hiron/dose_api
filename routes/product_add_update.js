module.exports = (app) => {
  const Product = app.db.Product;
  app
    .route("/product")
    .all(app.auth.authenticate("admin-role"))
    //.all(app.auth.authenticate("all"))
    .all(app.adminAccess.of("Medicin Management"))

    /**
     * 
     * @api {post} /product add new products/medicine
     * @apiName new product Add
     * @apiGroup Products
     * @apiVersion  1.0.0
     * 
     * 
     * @apiHeader  {String} Authorization User's authorization token
     * 
     * @apiParam {String} manufacturer manufacturer of the medicine
     * @apiParam {String} name Brand name of the medicine
     * @apiParam {String} group Group name of the medicine
     * @apiParam {String} strength quantity pre unit of the medicine
     * @apiParam {Number} price Price per unit
     * @apiParam {String} type Medicine Type
     * @apiParam {Boolean} prescription=false Presciption is mandatory or not
     * @apiParam {String} keyword Keyword of the medicine
     * 
     * 
     * @apiHeaderExample  {JSON} Request-Example:
     * {
     *     "Authorization" : "JWT 1234.xyde.4563"
     * }
     * @apiParamExample  {JSON} Request-Example:
    * {
        "manufacturer": "Beximco Pharmaceuticals Ltd.",
        "name": "Napa",
        "group": "Paracetamol",
        "strength": "500 mg",
        "price": 0.8,
        "type": "Tablet",
        "prescription": false,
        "keyword": null,
        "saleableUnit": 1,
        "maxLimit": null
      }
     * 
    * @apiSuccessExample {JSON} Success-Response:
     *  HTTP/1.1 200 Ok
      {
        "id": 470,
        "manufacturer": "Beximco Pharmaceuticals Ltd.",
        "name": "Napa",
        "group": "Paracetamol",
        "strength": "500 mg",
        "price": 0.8,
        "type": "Tablet",
        "prescription": false,
        "keyword": null,
        "saleableUnit": 1,
        "maxLimit": null,
        "createdAt": "2021-08-11T15:22:16.443Z",
        "updatedAt": "2021-08-11T15:22:16.443Z"
    }
   
     * 
     */
    .post((req, res) => {
      Product.create({
        ...req.body,
      })
        .then((products) => res.send(products))
        .catch((err) =>
          res.status(403).json({
            msg: err.message,
          })
        );
    })

    /**
     * 
     * @api {put} /product update new products/medicine
     * @apiName  product Update
     * @apiGroup Products
     * @apiVersion  1.0.0
     * 
     * 
     * @apiHeader  {String} Authorization User's authorization token
     * 
     * @apiParam {String} manufacturer manufacturer of the medicine
     * @apiParam {String} name Brand name of the medicine
     * @apiParam {String} group Group name of the medicine
     * @apiParam {String} strength quantity pre unit of the medicine
     * @apiParam {Number} price Price per unit
     * @apiParam {String} type Medicine Type
     * @apiParam {Boolean} prescription=false Presciption is mandatory or not
     * @apiParam {String} keyword Keyword of the medicine
     * 
     * 
     * @apiHeaderExample  {JSON} Request-Example:
     * {
     *     "Authorization" : "JWT 1234.xyde.4563"
     * }
     * @apiParamExample  {JSON} Request-Example:
    * {
    *  "productId": 470,
        "manufacturer": "Beximco Pharmaceuticals Ltd.",
        "name": "Napa",
        "group": "Paracetamol",
        "strength": "500 mg",
        "price": 0.8,
        "type": "Tablet",
        "prescription": false,
        "keyword": null,
        "saleableUnit": 1,
        "maxLimit": null
      }
     * 
    * @apiSuccessExample {JSON} Success-Response:
     *  HTTP/1.1 200 Ok
      {
        "id": 470,
        "manufacturer": "Beximco Pharmaceuticals Ltd.",
        "name": "Napa",
        "group": "Paracetamol",
        "strength": "500 mg",
        "price": 0.8,
        "type": "Tablet",
        "prescription": false,
        "keyword": null,
        "saleableUnit": 1,
        "maxLimit": null,
        "createdAt": "2021-08-11T15:22:16.443Z",
        "updatedAt": "2021-08-11T15:22:16.443Z"
    }
   
     * 
     */
    .put((req, res) => {
      Product.findOne({ where: { id: req.body.productId } })
        .then((product) => {
          if (!product) throw new Error("Medicin not found!!");
          Object.keys(req.body).forEach((d) => (product[d] = req.body[d]));
          return product.save();
        })
        .then((products) => res.send(products))
        .catch((err) =>
          res.status(403).json({
            msg: err.message,
          })
        );
    });
};
