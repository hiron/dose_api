const e = require("express");

module.exports = (app) => {
  app
    .route("/cause")
    .all(app.auth.authenticate())
    /**
     * 
     * @api {get} /cause Causes of Cancel
     * @apiName Cause of Cancel
     * @apiGroup Product Cancel
     * @apiVersion  1.0.0
     * 
     * 
     * @apiHeader  {String} authorization User Authorization token
     * 
     * @apiSuccess (200) {String} cause Causes of order Cancellation
     * @apiSuccess (200) {String} type User Type
     * @apiSuccess (200) {Number} id Causes id
     * 
     * @apiHeaderExample  {JSON} Header-Example:
     * {
     *     "Authorization" : "JWT 46539.gfyrhg.erey"
     * }
     * 
     * 
     * @apiSuccessExample {json} Success-Response:
     * [
    {
        "id": 1,
        "cause": "Change/combine order",
        "type": "client"
    },
    {
        "id": 2,
        "cause": "Duplicate order",
        "type": "client"
    },
  ]
     * 
     * 
     */
    .get(async (req, res) => {
      let type = await app.db.Login.findOne({
        where: { uId: req.user.uId },
      }).then((d) => d.type);
      app.db.CancelCause.findAll({ where: { type: type } })
        .then((d) => res.json(d))
        .catch((e) => d.status(401).json({ msg: e.message }));
    })

    /**
     * 
     * @api {put} /cause Order Cancel
     * @apiName order Cancel
     * @apiGroup Product Cancel
     * @apiVersion  1.0.0
     * 
     * 
     * @apiHeader  {String} authorization User Authorization token
     * 
     * @apiParam {String} orderId the invoice code of order
     * @apiParam {Number} causeId the selected cause id
     * 
     * @apiHeaderExample  {JSON} Header-Example:
     * {
     *     "Authorization" : "JWT 46539.gfyrhg.erey"
     * }
     * 
     * 
     * @apiParamExample {json} Request-body:
     * {
"orderId": "32FB83IH",
"causeId": 2
}
     * 
     * 
     */
    .put((req, res) => {
      app.db.Invoice.update(
        { cancel: true, cancelBy: req.user.uId, CancelCauseId: req.body.causeId },
        {
          where: { invoiceId: req.body.orderId },
        }
      )
        .then((d) => res.json(d))
        .catch((err) => res.status(401).json({ msg: err.message }));
    });
};
