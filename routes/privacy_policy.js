module.exports = (app) => {
  app.route("/policy")
  /**
   * 
   * @api {get} /policy Privacy policy of Dose
   * @apiName privacy policy
   * @apiGroup Policy
   * @apiVersion  1.0.0
   * 
   * 
   * @apiSuccess (200) {String} title Title of the privacy policy
   * @apiSuccess (200) {String} detail Detail info of the privacy policy
   * 
   * 
   * 
   * @apiSuccessExample {json} Success-Response:
   * [
    {
        "id": 3,
        "title": "Privacy Policy",
        "detail": "Exercitation aliquip irure anim velit anim sint fugiat dolor duis consequat occaecat. Dolore culpa dolore enim ipsum consequat veniam amet. Elit incididunt culpa reprehenderit reprehenderit elit anim tempor labore dolor deserunt enim sit. Ullamco commodo eiusmod et magna cupidatat voluptate tempor aute et nisi. Voluptate laboris sit minim proident in qui labore mollit.\n\nSunt velit cillum ullamco laborum deserunt nostrud nulla reprehenderit labore veniam non ea ut magna. Dolor labore exercitation commodo adipisicing commodo do. Ullamco sunt occaecat dolore sit.\n\nEu aute culpa sunt incididunt veniam id enim ad dolor pariatur. Qui excepteur ea est occaecat pariatur qui officia pariatur enim ipsum. Veniam nulla elit aute elit.\n\nConsectetur ad sint ad do laborum occaecat eu. Deserunt deserunt amet quis labore exercitation nisi labore ea officia incididunt. Voluptate consectetur cillum pariatur ut eiusmod. Dolor cupidatat exercitation deserunt aliquip non anim proident proident nulla.",
        "createdAt": "2020-08-27T18:00:00.000Z",
        "updatedAt": "2020-08-27T18:00:00.000Z"
    }
]
   * 
   * 
   */
  .get((req, res) => {
    app.db.PrivacyPolicy.findAll({})
      .then((d) => res.json(d))
      .catch((e) => res.status(401).json({ msg: e.message }));
  });
};
