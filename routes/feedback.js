module.exports = (app) => {
  app
    .route("/feedback")
    .all(app.auth.authenticate())
    /**
     * 
     * @api {post} /feedback Clients feedback
     * @apiName contact us
     * @apiGroup Feedback
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {String} subject Subject of the email
     * @apiParam  {String} body Body of the email
     * 
     * @apiSuccess (200) {number} id id of the feedback
     * @apiSuccess (200) {number} clientId id of the client
     * @apiSuccess (200) {String} subject sub of the feedback
     * @apiSuccess (200) {String} body Body of the feedback
     * 
     * @apiParamExample  {json} Request-Example:
     * {
     *     "subject" : "this is the subject",
     *     "body" : "Body of the mail"
     * }
     * 
     * 
     * @apiSuccessExample {JSON} Success-Response:
     * {
    "id": 1,
    "subject": "this is the subject",
    "body": "Body of the mail",
    "clientId": 3,
    "updatedAt": "2020-08-28T19:36:03.729Z",
    "createdAt": "2020-08-28T19:36:03.729Z"
}
     * 
     * 
     */
    .post(async (req, res) => {
      var clientId = await app.db.UserClient.findOne({
        where: { clientId: req.user.uId },
      }).then((d) => d.id);
      app.db.ClientFeedback.create({
        subject: req.body.subject,
        body: req.body.body,
        clientId: clientId,
      })
        .then((d) => res.json(d))
        .catch((e) => res.status(401).json({ msg: e.message }));
    });
};
