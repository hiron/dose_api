var customId = require("custom-id");

module.exports = (app, io) => {
  console.log(app);
  app
    .route("/agent-order")
    .all(app.auth.authenticate())
    /**
     * 
     * @api {get} /agent-order Get all received order List
     * @apiName All available undelivered Products List
     * @apiGroup Order
     * @apiVersion  1.0.0
     * 
     * 
     * @apiHeader  {String} Authorization User Authorization Token
     * 
     * @apiSuccess (200) {String} paymentMethod Payment Method of this order
     * @apiSuccess (200) {String} paymentType Payment type of this order
     * @apiSuccess (200) {String} txnId Transection Id of the payment
     * @apiSuccess (200) {Number} price Total cost of the Order
     * @apiSuccess (200) {Number} discountPrice Price after discount
     * @apiSuccess (200) {Number} discount Persentage of the discount
     * @apiSuccess (200) {Number} agentCharge Agent amount of the delivery charge
     * @apiSuccess (200) {Number} doseCharge Dose amount of the delivery charge
     * @apiSuccess (200) {Number} agentCommission Persentage of the discount
     * @apiSuccess (200) {Date} deliveredAt Date of product delivery
     * @apiSuccess (200) {Date} confirmedAt Date of confirm delivery
     * @apiSuccess (200) {id} addressId Id of the delivery address
     * @apiSuccess (200) {Boolean} done=false Delivery Confirmation Flag
     * @apiSuccess (200) {Boolean} cancel=false Delivery Cancelation Flag
     * @apiSuccess (200) {String} cancelBy id of the user(agent/client) who cancel the order
     * @apiSuccess (200) {String} invoiceId Id of the order
     * @apiSuccess (200) {Number} clientId Id of the Client
     * @apiSuccess (200) {Number} agentId Id of the agent who receive the order
     * @apiSuccess (200) {Number} cancelCauseId Id of the cancel cause
     * 
     * @apiHeaderExample  {json} Header-Example:
     * {
     *     "Authorization" : "1234.vchdtr.xyer"
     * }
     * 
     * 
     * @apiSuccessExample {JSON} Success-Response:
     *[
    {
        "id": 1,
        "paymentMethod": null,
        "paymentType": "postpay",
        "txnId": null,
        "price": 225.5,
        "discountPrice": 225.5,
        "deliveryCharge": 30,
        "prescription": null,
        "discount": 0,
        "doseCharge": 10,
        "agentCharge": 20,
        "agentCommission": 214.225,
        "doseCommission": 11.275,
        "deliveredAt": null,
        "confirmedAt": "2020-10-15T13:50:17.227Z",
        "addressId": 1,
        "done": false,
        "cancel": false,
        "cancelBy": null,
        "invoiceId": "09XR59QP",
        "clientId": 1,
        "agentId": 1,
        "createdAt": "2020-10-08T08:12:42.100Z",
        "updatedAt": "2020-10-15T13:50:17.228Z",
        "CancelCauseId": null,
        "Address": {
            "id": 1,
            "lat": 20.245584,
            "long": 40.05546846,
            "address": "Rajjaque Place, 6/1/a Mohonpur Adabor Dhaka-1207",
            "phone": "01521257488",
            "name": "Hiron Das",
            "label": "Home",
            "createdAt": "2020-10-08T08:11:11.113Z",
            "updatedAt": "2020-10-08T08:11:11.113Z",
            "userId": "c7wkg0jhk93"
        }
    }
]
     * 
     * 
     */
    .get((req, res) =>
      app.db.UserAgent.findOne({
        where: { agentId: req.user.uId },
        include: [
          {
            model: app.db.Invoice,
            where: { cancel: false, done: false },
            include: [
              { model: app.db.Address },
            //   {
            //     model: app.db.Order,
            //   },
            ],
          },
        ],
      })
        .then((d) => res.json(d.Invoices))
        .catch((e) => res.status(404).json({ msg: e.message }))
    )
};
