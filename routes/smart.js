const { Op } = require("sequelize");

module.exports = (app) => {
  const Cart = app.db.Cart;
  const SmartCart = app.db.SmartCart;
  // const Product = app.db.Product;
  app
    .route("/smart")
    .all(app.auth.authenticate())
    /**
     * 
     * @api {get} /smart get all availabel smart cart info
     * @apiName get smartcart
     * @apiGroup Smart Cart
     * @apiVersion  1.0.0
     * 
     * @apiHeader  {String} Authorization Token of authenticated user
     * 
     * 
     * @apiSuccess {Number} noOfMedicine No of total medicine type included in smartcart   
     * @apiSuccess {String} label Name of the smart-cart      
     * 
     * @apiSuccessExample  {JSON} Request-Example:
     *  HTTP/1.1 200 Ok
     * [
          {
        "label": "parent",
        "noOfMedicine": "1"
        }, {
        "label": "mother",
        "noOfMedicine": "5"
        }
     *]
     * 
     * @apiHeaderExample {json} Request-Header:
     *    {
     *        "Authorization": "JWT 1234.xysd.34hiy"
     *    }
     * 
     * 
     */
    .get((req, res) => {
      SmartCart.findAll({
        group: ["label"],
        attributes: [
          "label",
          [
            app.db.Sequelize.fn("COUNT", app.db.Sequelize.col("label")),
            "noOfMedicine",
          ],
        ],
        where: {
          clientId: req.user.uId,
        },
      })
        .then((cart) => res.json(cart))
        .catch((err) =>
          res.status(403).json({
            msg: err.message,
          })
        );
    })
    /**
     * 
     * @api {put} /smart update/replace smart cart
     * @apiName update smartcart
     * @apiGroup Smart Cart
     * @apiVersion  1.0.0
     * 
     * @apiHeader  {String} Authorization Token of authenticated user
     * @apiHeader  {String} Content-type content type of response
     * 
     * @apiParam  {String} label Name of the new Smart-cart
     * 
     * @apiSuccess {Number} id Smart Cart id
     * @apiSuccess {Number} productId Medicine id
     * @apiSuccess {Number} quantity Medicine quantity
     * @apiSuccess {String} clientId id of the client      
     * @apiSuccess {String} label Name of the smart-cart      
     * 
     * @apiSuccessExample  {JSON} Request-Example:
     *  HTTP/1.1 200 Ok
     * [
          {
          "id": 1,
          "productId": 300,
          "quantity": 20,
          "clientId": "9wokbf7v0ex",
          "createdAt": "2020-07-04T09:39:13.366Z",
          "updatedAt": "2020-07-04T09:39:13.366Z",
          "label": "parent"
          },{
          "id": 2,
          "productId": 200,
          "quantity": 10,
          "clientId": "9wokbf7v0ex",
          "createdAt": "2020-07-04T09:39:13.366Z",
          "updatedAt": "2020-07-04T09:39:13.366Z",
          "label": "parent"
          }
        ]
     * 
   * @apiHeaderExample {json} Request-Header:
     *    {
     *        "Content-Type":"application/json",
     *        "Authorization": "JWT 1234.xysd.34hiy"
     *    }
     * 
     * @apiParamExample {JSON} Success-Response:
     * {
     *     "label" : "parent"
     * }
     * 
     * 
     */
    .put(async (req, res) => {
      const cart = await Cart.findAll({
        where: {
          clientId: req.user.uId,
        },
      });

      console.log(
        JSON.parse(JSON.stringify(cart)).map(({ id, ...d }) => ({
          ...d,
          label: req.body.label,
        }))
      );

      SmartCart.findAll({
        where: {
          [Op.and]: [
            {
              label: req.body.label,
            },
            {
              clientId: req.user.uId,
            },
            {
              productId: {
                [Op.in]: JSON.parse(JSON.stringify(cart)).map(
                  (d) => d.productId
                ),
              },
            },
          ],
        },
        attributes: ["id"],
      })
        .then((d) =>
          SmartCart.destroy({
            where: {
              id: {
                [Op.in]: d.map((d) => d.id),
              },
            },
          })
        )
        .then((_) =>
          SmartCart.bulkCreate(
            JSON.parse(JSON.stringify(cart)).map(({ id, ...d }) => ({
              ...d,
              label: req.body.label,
            }))
          )
        )
        .then((cart) => res.json(cart))
        .catch((err) =>
          res.status(406).json({
            msg: err.message,
          })
        );
    })
    /**
     * 
     * @api {post} /smart Create/replace new smart cart
     * @apiName add smartcart
     * @apiGroup Smart Cart
     * @apiVersion  1.0.0
     * 
     * @apiHeader  {String} Authorization Token of authenticated user
     * @apiHeader  {String} Content-type content type of response
     * 
     * @apiParam  {String} label Name of the new Smart-cart
     * 
     * @apiSuccess {Number} id Smart Cart id
     * @apiSuccess {Number} productId Medicine id
     * @apiSuccess {Number} quantity Medicine quantity
     * @apiSuccess {String} clientId id of the client      
     * @apiSuccess {String} label Name of the smart-cart      
     * 
     * @apiSuccessExample  {JSON} Request-Example:
     *  HTTP/1.1 200 Ok
     * [
          {
          "id": 1,
          "productId": 300,
          "quantity": 20,
          "clientId": "9wokbf7v0ex",
          "createdAt": "2020-07-04T09:39:13.366Z",
          "updatedAt": "2020-07-04T09:39:13.366Z",
          "label": "parent"
          },{
          "id": 2,
          "productId": 200,
          "quantity": 10,
          "clientId": "9wokbf7v0ex",
          "createdAt": "2020-07-04T09:39:13.366Z",
          "updatedAt": "2020-07-04T09:39:13.366Z",
          "label": "parent"
          }
        ]
     * 
   * @apiHeaderExample {json} Request-Header:
     *    {
     *        "Content-Type":"application/json",
     *        "Authorization": "JWT 1234.xysd.34hiy"
     *    }
     * 
     * @apiParamExample {JSON} Success-Response:
     * {
     *     "label" : "parent"
     * }
     * 
     * 
     */
    .post(async (req, res) => {
      const cart = await Cart.findAll({
        where: {
          clientId: req.user.uId,
        },
      });

      SmartCart.findAll({
        where: {
          [Op.and]: [
            {
              label: req.body.label,
            },
            {
              clientId: req.user.uId,
            },
          ],
        },
        attributes: ["id"],
      })
        .then((d) =>
          SmartCart.destroy({
            where: {
              id: {
                [Op.in]: d.map((d) => d.id),
              },
            },
          })
        )
        .then((_) =>
          SmartCart.bulkCreate(
            JSON.parse(JSON.stringify(cart)).map(({ id, ...d }) => ({
              ...d,
              label: req.body.label,
            }))
          )
        )
        .then((cart) => res.json(cart))
        .catch((err) =>
          res.status(401).json({
            msg: err.message,
          })
        );
    })
    /**
     *
     * @api {delete} /smart/?labels='parent'&labels='home' Remove a smart cart
     * @apiName Delete smartcart
     * @apiGroup Smart Cart
     * @apiVersion  1.0.0
     *
     * @apiHeader  {String} Authorization Token of authenticated user
     * @apiHeader  {String} Content-type content type of response
     *
     * @apiParam  {String} labels Names of the Smart-cart
     *
     *
     * @apiSuccessExample  {JSON} Request-Example:
     *  HTTP/1.1 200 Ok
     * 1
     *
     * @apiHeaderExample {json} Request-Header:
     *    {
     *        "Content-Type":"application/json",
     *        "Authorization": "JWT 1234.xysd.34hiy"
     *    }
     *
     * @apiParamExample {JSON} Success-Response:
     * {
     *     "labels" : ["parent","Mother"]
     * }
     *
     *
     */
    .delete((req, res) =>
      SmartCart.destroy({
        where: {
          [Op.and]: [
            {
              label: {
                [Op.in]: req.query.labels,
              },
            },
            {
              clientId: req.user.uId,
            },
          ],
        },
      })
        .then((d) => res.json(d))
        .catch((err) =>
          res.status(406).json({
            msg: err.message,
          })
        )
    );
};
