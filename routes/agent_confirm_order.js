module.exports = (app, io) => {
  app
    .route("/accept")
    .all(app.auth.authenticate())
    /**
     * 
     * @api {post} /accept Accept an Order
     * @apiName accept order
     * @apiGroup Order
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {String} orderId Id of the Order
     * 
     * 
     * @apiParamExample  {JSON} Request-Example:
     * {
     *     "orderId":"09XR59QP"
     * }
     * 
     * 
     * @apiSuccessExample {JSON} Success-Response:
     * [1]
     * 
     * 
     */
    .post(async (req, res) => {
      console.log(req);
      let agentId = await app.db.UserAgent.findOne({
        where: { agentId: req.user.uId },
      }).then((d) => d.id);
      app.db.Invoice.update(
        { confirmedAt: new Date(), agentId: agentId },
        {
          where: { invoiceId: req.body.orderId, agentId: null },
        }
      )
        .then(async (d) => {
          var orders = await app.db.Invoice.findAll({
            where: { cancel: false, done: false, agentId: null },
            include: [{ model: app.db.Address }],
          }).then((d) => d);
          // console.log(orders);
          io.emit("alertMsg", {
            order: orders.length,
            title: "New Order Request",
            body: "New Available Orders " + orders.length,
          });

          io.emit("orderInfo", {
            order: orders,
            orderCount: orders.length,
          });
          res.json(d);
        })
        .catch((e) => res.status(401).json({ msg: e.message }));
    });
};
