var speakeasy = require("speakeasy");
const jwt = require("jwt-simple");

module.exports = (app) => {
  const Login = app.db.Login;
  const cfg = app.libs.config;

  app
    .route("/token")
    /**
     *
     * @api {post} /token Authenticaiton Token
     * @apiName Authorization
     * @apiGroup Credentials
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {String} phone User's phone
     * @apiParam  {String} type User's type
     * @apiParam  {String} otp User's otp (previously sent to usre's email)
     *
     * @apiSuccess {String} token Token of Authenticated user
     *
     * @apiParamExample  {json} Request-Example:
     * {
     *     "phone" : "01274860876",
     *     "type" : "client",
     *     "otp" : "122376"
     * }
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 Ok
     * {
     *     "token" : "bjfyei.345jde.478.xyz"
     * }
     * @apiErrorExample {json} Authentication error:
     *  HTTP/1.1 401 Unauthorized
     *  @apiErrorExample {json} ivalid email error:
     *  HTTP/1.1 403 Forbidden
     *
     */
    .post((req, res) => {
      Login.findOne({ where: { phone: req.body.phone.substr(-11) } })
        .then((user) => {
          if (user.type == req.body.type) {
            let varified = speakeasy.totp.verify({
              secret: user.secret,
              token: req.body.otp,
              window: 6,
            });

            if (varified) {
              res.json({ token: jwt.encode({ uId: user.uId }, cfg.jwtSecret) });
            } else {
              res
                .status(401)
                .json({
                  msg: "OTP is wrong or expaired plz send otp request agani.",
                });
            }
          }else{
            res.status(403).json({ msg: "This user is blocked form this App. Please use another number!" })
          }
        })
        .catch((err) => res.status(403).json({ msg: err.message }));
    });
};

//eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1SWQiOiI1MTRrOXA5eDk3aCJ9.aLbnR-SUDCy_2eMPpLxb3FY3PV4Eu4p9uNr4wTHnnBo
//eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1SWQiOiIyZGtrOW8ycGE0bCJ9.NcuiyDC-m41IYXwHQCocBcuD1w6YUT79hkDou_0sKmc <hcdas.09@gmail.com>
