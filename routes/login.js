var speakeasy = require("speakeasy");
const uniqid = require("uniqid");
const axios = require("axios");

module.exports = (app) => {
  const Login = app.db.Login;

  app
    .route("/login")
    /**
     *
     * @api {POST} /login user login
     * @apiDescription send OTP to the respective phone and include the user if not included before
     * @apiName SendOtp
     * @apiGroup Login
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {String} phone User's phone
     * @apiParam  {String} type Users's type
     *
     * @apiSuccess {String} msg phone acknowledge msg
     *
     * @apiParamExample  {json} Request-Example:
     * {
     *     "phone" : "01874595676",
     *     "type" : "client"
     * }
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *     "msg" : "otp send to phone"
     * }
     *
     *
     *  @apiErrorExample {json} Error-wrong phone or type:
     *  HTTP/1.1 403 Forbidden Error
     *
     * @apiErrorExample {json} Error-account lock:
     *  HTTP/1.1 401 Unauthorized Error
     *
     * @apiErrorExample {json} Error-email send fail:
     *  HTTP/1.1 412 Precondition Failed
     *
     */

    .post((req, res) => {
      if (req.body.phone == undefined || req.body.type == undefined) {
        res.status(412).json({
          msg: "Precondition Failed!! phone or type wrong",
        });
      } else {
        Login.findOne({
          where: {
            phone: req.body.phone.substr(-11),
          },
        })
          .then((user) => {
            if (user == null) {
              Login.create({
                ...req.body,
                phone:
                  req.body.phone.length < 14
                    ? req.body.phone.substr(-11)
                    : req.body.phone,
                secret: speakeasy.generateSecret().ascii,
                uId: uniqid.process(),
              })
                .then((user) => {
                  axios
                    .get(
                      `${process.env.otpHost}&receiver=${
                        user.phone
                      }&message=Your otp is ${speakeasy.totp({
                        secret: user.secret,
                      })}. Don't share it to others.`
                    )
                    .then((info) =>
                      res.json({
                        reg: false,
                        msg: "otp send to phone",
                      })
                    )
                    .catch((err) => res.status(412).json(err.message));
                })
                .catch((err) =>
                  res.status(403).json({
                    msg: err.message,
                  })
                );
            } else {
              // res.json(speakeasy.totp({ secret: user.secret }));
              user.state && user.type == req.body.type
                ? axios
                    .get(
                      `${process.env.otpHost}&receiver=${
                        user.phone
                      }&message=Your otp is ${speakeasy.totp({
                        secret: user.secret,
                      })}. Don't share it to others.`
                    )
                    .then(async (info) => {
                      var userFormated = JSON.parse(JSON.stringify(user));
                      console.log(userFormated);
                      var validUser =
                        userFormated.type == "client"
                          ? await app.db.UserClient.findOne({
                              where: { clientId: userFormated.uId },
                            }).then((d) => d)
                          : await app.db.UserAgent.findOne({
                              where: { clientId: userFormated.uId },
                            }).then((d) => d);

                      res.json({
                        reg: validUser != null ? true : false,
                        msg: "otp send to phone",
                      });
                    })
                    .catch((err) => res.status(412).json({ msg: err.message }))
                : res
                    .status(401)
                    .json({
                      msg: "the account is locked!! please contact with vendor.",
                    });
            }
          })
          .catch((err) =>
            res.status(412).json({
              msg: err.message,
            })
          );
      }
    });
};
