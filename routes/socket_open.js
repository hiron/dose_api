module.exports = (app, io) => {
  /**
   * 
   * @api {get} /alertMsg get Data from socket
   * @apiName Socket
   * @apiGroup Socket
   * @apiVersion  1.0.0
   * 
   * 
   * 
   * @apiSuccessExample {JSON} Success-Response:
      {"order":2,"title":"New Order Request","body":"New Available Orders 2"}
   * 
   * 
   */

   /**
   * 
   * @api {get} /orderInfo get Order Detail Data from socket
   * @apiName Socket detail
   * @apiGroup Socket
   * @apiVersion  1.0.0
   * 
   * 
   * 
   * @apiSuccessExample {JSON} Success-Response:
  {
   "orderCount":2,
   "order":[
      {
         "id":1,
         "paymentMethod":null,
         "paymentType":"postpay",
         "txnId":null,
         "price":225.5,
         "discountPrice":225.5,
         "deliveryCharge":30,
         "prescription":null,
         "discount":0,
         "doseCharge":10,
         "agentCharge":20,
         "agentCommission":214.225,
         "doseCommission":11.275,
         "deliveredAt":null,
         "confirmedAt":null,
         "addressId":1,
         "done":false,
         "cancel":false,
         "cancelBy":null,
         "invoiceId":"09XR59QP",
         "clientId":1,
         "agentId":null,
         "createdAt":"2020-10-08T08:12:42.100Z",
         "updatedAt":"2020-10-08T08:12:42.119Z",
         "CancelCauseId":null,
         "Address":{
            "id":1,
            "lat":20.245584,
            "long":40.05546846,
            "address":"Rajjaque Place, 6/1/a Mohonpur Adabor Dhaka-1207",
            "phone":"01521257488",
            "name":"Hiron Das",
            "label":"Home",
            "createdAt":"2020-10-08T08:11:11.113Z",
            "updatedAt":"2020-10-08T08:11:11.113Z",
            "userId":"c7wkg0jhk93"
         },
      },
      {
         "id":2,
         "paymentMethod":null,
         "paymentType":"postpay",
         "txnId":null,
         "price":232.9,
         "discountPrice":232.9,
         "deliveryCharge":30,
         "prescription":null,
         "discount":0,
         "doseCharge":10,
         "agentCharge":20,
         "agentCommission":221.255,
         "doseCommission":11.645,
         "deliveredAt":null,
         "confirmedAt":null,
         "addressId":1,
         "done":false,
         "cancel":false,
         "cancelBy":null,
         "invoiceId":"16FQ37VT",
         "clientId":1,
         "agentId":null,
         "createdAt":"2020-10-08T08:16:34.953Z",
         "updatedAt":"2020-10-08T08:16:34.962Z",
         "CancelCauseId":null,
         "Address":{
            "id":1,
            "lat":20.245584,
            "long":40.05546846,
            "address":"Rajjaque Place, 6/1/a Mohonpur Adabor Dhaka-1207",
            "phone":"01521257488",
            "name":"Hiron Das",
            "label":"Home",
            "createdAt":"2020-10-08T08:11:11.113Z",
            "updatedAt":"2020-10-08T08:11:11.113Z",
            "userId":"c7wkg0jhk93"
         },
      }
   ]
}
   * 
   * 
   */
  io.on("connection", async (socket) => {
    var orders = await app.db.Invoice.findAll({
      where: { cancel: false, done: false, agentId: null },
      include: [{ model: app.db.Address }],
    }).then((d) => d);
    // console.log(orders);
    socket.emit("alertMsg", {
      order: orders.length,
      title: "New Order Request",
      body: "New Available Orders " + orders.length,
    });

    socket.emit("orderInfo", {
      order: orders,
      orderCount: orders.length,
    });

  });
};
