const multer = require("multer");
const fs = require("fs");

const upload = multer({ dest: "/tmp/" });

module.exports = (app) => {
  app
    .route("/upload")
    .all(app.auth.authenticate())
      /**
     *
     * @api {get} /upload Get upload image file
     * @apiName get upload prescription image
     * @apiGroup Prescription
     * @apiVersion  1.0.0
     *
     *
     *
     * @apiSuccess (200) {string} image link of the image
     * @apiSuccess (200) {String} clientId Client id
     * @apiSuccess (200) {number} id ID of the image
     *
     * @apiSuccessExample {json} Success-Response:
     * [{
     *     "clientId" : "",
     *      "invoiceId": null,
     *      "image":"abc.png",
     *      "id": 1
     * },{
     *     "clientId" : "",
     *      "invoiceId": null,
     *      "image":"xyz.jpg",
     *      "id": 2
     * }]
     *
     *
     */
    .get((req, res) => {
      app.db.Prescription.findAll({
        where: { clientId: req.user.uId, invoiceId: null },
      })
        .then((d) => res.json(d))
        .catch((e) => res.status(401).json({ msg: e.message }));
    })
    /**
     *
     * @api {post} /upload upload image file
     * @apiName upload image
     * @apiGroup Prescription
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {file} file upload prescription image
     *
    * @apiSuccess (200) {string} image link of the image
     * @apiSuccess (200) {String} clientId Client id
     * @apiSuccess (200) {number} id ID of the image
     *
     * @apiParamExample  {json} Request-Example:
     * {
     *     "file" : imagefile
     * }
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * {
     *     "clientId" : "",
     *      "image":"",
     *      "id": 1
     * }
     *
     *
     */
    .post(upload.single("file"), (req, res) => {
      console.log(req.file);
      let extArray = req.file.originalname.split(".");
      let extension = extArray[extArray.length - 1];
      let fileNewName = req.file.filename +"_"+Date.now()+ "." + extension;
      fs.rename(
        req.file.path,
        "public/prescription/" + fileNewName,
        (err) => {
          if (err) {
            console.log(err);
            res.sendStatus(500);
          } else {
            app.db.Prescription.create({
              clientId: req.user.uId,
              image: fileNewName,
            })
              .then((d) => res.json(d))
              .catch((e) => res.status(401).json({ msg: e.message }));
          }
        }
      );
    });
};
