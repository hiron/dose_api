module.exports = (app) => {
  const Product = app.db.Product;
  app
    .route("/product/:id")
    //.all(app.auth.authenticate())
    .all(app.auth.authenticate("all"))
    //.all(app.adminAccess.of("User Type"))

    /**
     * 
     * @api {get} /product/:id Get products/medicine detail from id 
     * @apiName product details
     * @apiGroup Products
     * @apiVersion  1.0.0
     * 
     * 
     * @apiHeader  {String} Authorization User's authorization token
     * 
     * @apiSuccess {Number} id Medicine id
     * @apiSuccess {String} manufacturer manufacturer of the medicine
     * @apiSuccess {String} name Brand name of the medicine
     * @apiSuccess {String} group Group name of the medicine
     * @apiSuccess {String} strength quantity pre unit of the medicine
     * @apiSuccess {Number} price Price per unit
     * @apiSuccess {String} type Medicine Type
     * @apiSuccess {Boolean} prescription=false Presciption is mandatory or not
     * @apiSuccess {String} keyword Keyword of the medicine
     * 
     * 
     * @apiHeaderExample  {JSON} Request-Example:
     * {
     *     "Authorization" : "JWT 1234.xyde.4563"
     * }
     * 
    * @apiSuccessExample {JSON} Success-Response:
     *  HTTP/1.1 200 Ok
      {
         "id":300,
         "manufacturer":"Benham Pharmaceuticals Ltd.",
         "name":"Beonac 50",
         "group":"Diclofenac Sodium",
         "strength":"50 mg",
         "price":0.6,
         "type":"Tablet",
         "prescription":false,
         "keyword":null,
         "createdAt":"2020-06-14T15:21:21.085Z",
         "updatedAt":"2020-06-14T15:21:21.085Z"
      }
   
     * 
     */
    .get((req, res) => {
      console.log(req.query);
      Product.findOne({
        where: {
          id: req.params.id,
        },
      })
        .then((products) => res.send(products))
        .catch((err) =>
          res.status(403).json({
            msg: err.message,
          })
        );
    });
};
