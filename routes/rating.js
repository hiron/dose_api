module.exports = (app) => {
  app
    .route("/rating")
    .all(app.auth.authenticate())
    /**
     *
     * @api {post} /rating Rate the delivery
     * @apiName rating service
     * @apiGroup Rating
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {Number} rate Rating of the service out of 5
     * @apiParam  {Number} invoiceId id of the invoice
     * @apiParam  {Number} clientId id of the client
     * @apiParam  {Number} agentId id of the agent
     * @apiParam  {String} detail Detail text message given by user
     *
     * @apiSuccess (200) {type} name description
     *
     * @apiParamExample  {json} Request-Example:
     * {
     *     "rate" : 3,
     *     "invoiceId" : 13,
     *     "clientId" : 3,
     *     "agentId" : 30,
     *     "detail": "Occaecat aute sunt in ex occaecat consequat."
     * }
     *
     *
     * @apiSuccessExample {JSON} Success-Response:
     * {
    "id": 7,
    "rate": 5,
    "invoiceId": 8,
    "clientId": 1,
    "agentId": null,
    "detail": "Occaecat aute sunt in ex occaecat consequat.",
    "updatedAt": "2020-08-27T10:45:51.709Z",
    "createdAt": "2020-08-27T10:45:51.709Z"
} 
     *
     *
     */
    .post((req, res) => {
      app.db.Rating.create({
        ...req.body,
      })
        .then((d) => res.json(d))
        .catch((e) => res.status(402).json({ msg: e.message }));
    });
};
