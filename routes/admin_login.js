const bcrypt = require("bcrypt");
const jwt = require("jwt-simple");

module.exports = (app) => {
  const cfg = app.libs.config;
  app
    .route("/adminlogin")
    /**
     * 
     * @api {post} /adminlogin varify Admin-login 
     * @apiDescription Admin get token after login
     * @apiName postAdminLogin
     * @apiGroup Registration
     * @apiPrivate
     * @apiVersion  1.0.0
     * @apiParam  {String} username Admin's unique username
     * @apiParam  {String} password Admin's Password
     *
     * @apiSuccess {String} token user token
     *
     * @apiParamExample  {json} Request-Example:
     * {
            "password": "Hiron@12345",
            "username": "Hiron"
        }
     *
     * 
     * 
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
          "token": "1234.xydse.abcd"
        }
     * @apiErrorExample {json} Error-redponcd:
    *  HTTP/1.1 403 Forbidden Error
        {
          "msg": "username or password is not matched!!"
        }
     * 
     */
    .post((req, res) => {
      app.db.AdminLogin.findOne({
        where: { username: req.body.username, state: true },
      })
        .then((user) => {
          if (!user) throw new Error("The account is Locked!!");
          bcrypt.compareSync(req.body.password, user.secret)
            ? res.json({ token: jwt.encode({ uId: user.uId }, cfg.jwtSecret) })
            : res
                .status(403)
                .json({ msg: "username or password is not matched!!" });
        })
        .catch((err) => {
          res.status(401).json({ msg: err.message });
        });
    });
};
