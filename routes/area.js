module.exports = (app) => {
  app
    .route("/area")
    .all(app.auth.authenticate())
    /**
     * 
     * @api {get} /area get all area
     * @apiName get area info
     * @apiGroup Area
     * @apiVersion  1.0.0
     * 
     * 
     * @apiHeader  {String} Authorization Authentication token of the user
     * 
     * @apiSuccess (200) {String} district Name of the district
     * @apiSuccess (200) {String} area Name of the Area
     * 
     * @apiHeaderExample  {JSON} Request-Example:
     * {
     *     "Authorization" : "JWT 123.xdr.234",
     *     "content-type" : "application/json"
     * }
     * 
     * 
     * @apiSuccessExample {JSON} Success-Response:
     *[
        {
            "district": "Dhaka",
            "area": [
                "Adabar",
                "Azampur",
                "Badda",
                "Bangsal",
                "Bimanbandar",
                "Cantonment",
                "Chowkbazar",
                "Darus Salam",
                "Demra",
                "Dhanmondi",
                "Gendaria",
                "Gulshan",
                "Hazaribagh",
                "Kadamtali",
                "Kafrul",
                "Kalabagan",
                "Kamrangirchar",
                "Khilgaon",
                "Khilkhet",
                "Kotwali",
                "Lalbagh",
                "Mirpur Model",
                "Mohammadpur",
                "Motijheel",
                "New Market",
                "Pallabi",
                "Paltan",
                "Panthapath",
                "Ramna",
                "Rampura",
                "Sabujbagh",
                "Shah Ali",
                "Shahbag",
                "Sher-e-Bangla Nagar",
                "Shyampur",
                "Sutrapur",
                "Tejgaon Industrial Area",
                "Tejgaon",
                "Turag",
                "Uttar Khan",
                "Uttara",
                "Vatara",
                "Wari"
            ]
        }
    ]
     * 
     * 
     */
    .get((req, res) => {
      app.db.Area.findAll()
        .then((d) => {
          var obj = d.reduce(
            (result, item) => ({
              ...result,
              [item.district]: [...(result[item.district] || []), item.area],
            }),
            {}
          );

          res.json(
            Object.keys(obj).map((d) => ({ district: d, area: obj[d] }))
          );
        })
        .catch((e) => res.status(401).json({ msg: e.message }));
    });
};
