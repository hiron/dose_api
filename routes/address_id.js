const { Op } = require("sequelize");

module.exports = (app) => {
  const Address = app.db.Address;
  app
    .route("/address/:id")
    .all(app.auth.authenticate())
    /**
         * 
         * @api {get} /address/:id Get a addresses of user
         * @apiName Get Address
         * @apiGroup Address
         * @apiVersion  1.0.0
         * 
         * 
         * @apiHeader  {String} Content-type content type of the request
         * @apiHeader  {String} Authorization User's authentication token
         * 
         * @apiSuccess {Number} lat Latitude of the address
         * @apiSuccess {Number} long Longitude of the address
         * @apiSuccess {String} address Address detail of the home address
         * @apiParam {String} name Name of the recipient
         * @apiSuccess {String} phone Phone address of the recipient
         * @apiSuccess {String} label Label of the address
         * @apiSuccess {String} userId user's Id
         * @apiSuccess {Number} id Address id
         * 
         * @apiHeaderExample  {json} Header-Example:
         * {
         *     Content-Type : "aplication/json"
         *     Authorization : "JWT xyz.1234.wxyz"
         * }
         * 
         * 
         * @apiSuccessExample {json} Success-Response:
         * HTTP/1.1 200 Ok
             {
               "id": 1,
               "lat": 67.3467464,
               "long": 34.24437,
               "address": "Home flat info is included here",
               "phone": "01723948523",
               "name": "Hiron Das",
               "label": "Home",
              }
         * 
         * @apiErrorExample {json} Authentication error:
         *  HTTP/1.1 401 Unauthorized
         * 
         */
    .get((req, res) => {
      Address.findOne({
        where: {
          userId: req.user.uId,
          id: req.params.id,
        },
        // attributes: { exclude: ["userId", "createdAt", "updatedAt"] },
      })
        .then((address) => res.json(address))
        .catch((err) =>
          res.status(401).json({
            msg: err.message,
          })
        );
    }) /**
    * 
    * @api {delete} /address/:id Delete address of the given ID
    * @apiName Delete Address
    * @apiGroup Address
    * @apiVersion  1.0.0
    * 
    * 
    * @apiHeader  {String} Content-type content type of the request
    * @apiHeader  {String} Authorization User's authentication token
    * 
    * 
    * 
    * @apiHeaderExample  {json} Header-Example:
    * {
    *     Content-Type : "aplication/json"
    *     Authorization : "JWT xyz.1234.wxyz"
    * }
    * 
    * @apiSuccessExample {json} Success-Response:
    * HTTP/1.1 200 Ok
       1
    * 
    * @apiErrorExample {json} Not Acceptable error:
    *  HTTP/1.1 406 Not Acceptable
    * 
    */
    .delete((req, res) => {
      console.log(req.body);
      Address.destroy({
        where: {
          [Op.and]: [
            {
              userId: req.user.uId,
            },
            {
              id: req.params.id,
            },
          ],
        },
      })
        .then((d) => res.json(d))
        .catch((err) =>
          res.status("406").json({
            msg: err.message,
          })
        );
    })
    .post((req, res) => {
      Address.update(
        {
          lat: req.body.lat,
          long: req.body.long,
          address: req.body.address,
          phone: req.body.phone,
          name: req.body.name,
        },
        {
          where: {
            [Op.and]: {
              userId: req.user.uId,
              id: req.params.id,
            },
          },
        }
      )
        .then((d) => {
          res.json(d);
        })
        .catch((e) => res.status(402).json(e));
    });
};
