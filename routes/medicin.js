const { Op } = require("sequelize");
module.exports = (app) => {
  const Product = app.db.Product;
  app
    .route("/medicine/:search")
    //.all(app.auth.authenticate())
    .all(app.auth.authenticate("all"))
    /**
     * 
     * @api {get} /medicine/:search search products/medicines detail info 
     * @apiName product search
     * @apiGroup Products
     * @apiVersion  1.0.0
     * 
     * 
     * @apiHeader  {String} Authorization User's authorization token
     * 
     * @apiSuccess {Number} id Medicine id
     * @apiSuccess {String} manufacturer manufacturer of the medicine
     * @apiSuccess {String} name Brand name of the medicine
     * @apiSuccess {String} group Group name of the medicine
     * @apiSuccess {String} strength quantity pre unit of the medicine
     * @apiSuccess {Number} price Price per unit
     * @apiSuccess {String} type Medicine Type
     * @apiSuccess {Boolean} prescription=false Presciption is mandatory or not
     * @apiSuccess {String} keyword Keyword of the medicine
     * 
     * 
     * 
     * @apiHeaderExample  {json} Header:
     * {
     *     "Authorization" : "JWT 1234.xyde.4563"
     * }
     * 
    * @apiSuccessExample {JSON} Success-Response:
     *  HTTP/1.1 200 Ok
     [{
    "id": 470,
    "manufacturer": "Beximco Pharmaceuticals Ltd.",
    "name": "Napa",
    "group": "Paracetamol",
    "strength": "500 mg",
    "price": 0.8,
    "type": "Tablet",
    "prescription": false,
    "keyword": null,
    "createdAt": "2020-06-14T15:21:21.086Z",
    "updatedAt": "2020-06-14T15:21:21.086Z"
    },
      {
    "id": 471,
    "manufacturer": "Beximco Pharmaceuticals Ltd.",
    "name": "Napa Rapid",
    "group": "Paracetamol",
    "strength": "500 mg",
    "price": 0.8,
    "type": "Rapid Tablet",
    "prescription": false,
    "keyword": null,
    "createdAt": "2020-06-14T15:21:21.086Z",
    "updatedAt": "2020-06-14T15:21:21.086Z"
    },
      {
    "id": 836,
    "manufacturer": "Beximco Pharmaceuticals Ltd.",
    "name": "Napa DT",
    "group": "Paracetamol",
    "strength": "500 mg",
    "price": 1.28,
    "type": "Tablet",
    "prescription": false,
    "keyword": null,
    "createdAt": "2020-06-14T15:21:21.088Z",
    "updatedAt": "2020-06-14T15:21:21.088Z"
}]
     * 
     */
    .get((req, res) => {
      Product.findAll({
        where: {
          [Op.or]: [
            { name: { [Op.iLike]: `${req.params.search}%` } },
            { group: { [Op.iLike]: `${req.params.search}%` } },
            { keyword: { [Op.iLike]: `%${req.params.search}%` } },
          ],
        },
      })
        .then((products) => res.json(products))
        .catch((err) => res.status(401).json({ msg: err.message }));
    });
};
