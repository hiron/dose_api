const {
  Op
} = require("sequelize");

module.exports = (app) => {
  const Cart = app.db.Cart;
  const Product = app.db.Product;
  const SmartCart = app.db.SmartCart;

  app
    .route("/cart")
    .all(app.auth.authenticate())
    /**
     * 
     * @api {get} /cart Get current cart items
     * @apiName getCart
     * @apiGroup Cart
     * @apiVersion  1.0.0
     * 
     * @apiHeader  {String} Authorization User's authentication token
     * 
     * @apiSuccess {Number} id cart item id
     * @apiSuccess {Number} productId Medicine id
     * @apiSuccess {Number} quantity Medicine quantity
     * @apiSuccess {String} clientId client's uid 
     * @apiSuccess {String} manufacturer manufacturer of the medicine
     * @apiSuccess {String} name Brand name of the medicine
     * @apiSuccess {String} group Group name of the medicine
     * @apiSuccess {String} strength quantity pre unit of the medicine
     * @apiSuccess {Number} price Price per unit
     * @apiSuccess {String} type Medicine Type
     * @apiSuccess {Boolean} prescription=false Presciption is mandatory or not
     * @apiSuccess {String} keyword Keyword of the medicine
     * 
     * @apiHeaderExample  {JSON} Request-Header:
     * {
     *     "Authorization" : "JWT 1234.xyde.4563"
     * }
     * 
     * 
     * 
     * @apiSuccessExample {JSON} Success-Response:
     *  HTTP/1.1 200 Ok
     [
   {
      "id":1,
      "productId":300,
      "quantity":20,
      "clientId":"9wokbf7v0ex",
      "createdAt":"2020-07-04T09:39:13.366Z",
      "updatedAt":"2020-07-04T09:39:13.366Z",
      "Product":{
         "id":300,
         "manufacturer":"Benham Pharmaceuticals Ltd.",
         "name":"Beonac 50",
         "group":"Diclofenac Sodium",
         "strength":"50 mg",
         "price":0.6,
         "type":"Tablet",
         "prescription":false,
         "keyword":null,
         "createdAt":"2020-06-14T15:21:21.085Z",
         "updatedAt":"2020-06-14T15:21:21.085Z"
      }
   },
   {
      "id":2,
      "productId":500,
      "quantity":10,
      "clientId":"9wokbf7v0ex",
      "createdAt":"2020-07-04T09:39:26.301Z",
      "updatedAt":"2020-07-04T09:39:26.301Z",
      "Product":{
         "id":500,
         "manufacturer":"Novartis (Bangladesh) Ltd.",
         "name":"Servigesic",
         "group":"Paracetamol",
         "strength":"500 mg",
         "price":0.8,
         "type":"Tablet",
         "prescription":false,
         "keyword":null,
         "createdAt":"2020-06-14T15:21:21.086Z",
         "updatedAt":"2020-06-14T15:21:21.086Z"
      }
   }
]
     * 
     * 
     */
    .get((req, res) => {
      Cart.findAll({
          include: [{
            model: Product,
          }, ],
          where: {
            clientId: req.user.uId,
          },
        })
        .then((cart) => res.json(cart))
        .catch((err) =>
          res.status(403).json({
            msg: err.message,
          })
        );
    })
    /**
     * 
     * @api {post} /post Add items on cart
     * @apiName add item
     * @apiGroup Cart
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam {Number} productId Id of the medicine
     * @apiParam {Number} Quantity Quantity of the Products
     * 
     * 
     * @apiParamExample  {JSON} Request-Example:
     * {
     *     productId : 200,
     *     quantity : 20
     * }
     * 
     * @apiHeader  {String} Authorization User's authentication token
     * @apiHeader  {String} Content-type content type header
     * 
     * @apiSuccess {Number} id cart item id
     * @apiSuccess {Number} productId Medicine id
     * @apiSuccess {Number} quantity Medicine quantity
     * @apiSuccess {String} clientId client's uid 
     * @apiSuccess {String} manufacturer manufacturer of the medicine
     * @apiSuccess {String} name Brand name of the medicine
     * @apiSuccess {String} group Group name of the medicine
     * @apiSuccess {String} strength quantity pre unit of the medicine
     * @apiSuccess {Number} price Price per unit
     * @apiSuccess {String} type Medicine Type
     * @apiSuccess {Boolean} prescription=false Presciption is mandatory or not
     * @apiSuccess {String} keyword Keyword of the medicine
     * 
     * @apiHeaderExample  {JSON} Request-Header:
     * {
     *     "Authorization" : "JWT 1234.xyde.4563"
     *     "Content-Type" : "application/json"
     * }
     * 
     * 
     * @apiSuccessExample {JSON} Success-Response:
     *  HTTP/1.1 200 Ok
   {
      "id":1,
      "productId":300,
      "quantity":20,
      "clientId":"9wokbf7v0ex",
      "createdAt":"2020-07-04T09:39:13.366Z",
      "updatedAt":"2020-07-04T09:39:13.366Z",
      "Product":{
         "id":300,
         "manufacturer":"Benham Pharmaceuticals Ltd.",
         "name":"Beonac 50",
         "group":"Diclofenac Sodium",
         "strength":"50 mg",
         "price":0.6,
         "type":"Tablet",
         "prescription":false,
         "keyword":null,
         "createdAt":"2020-06-14T15:21:21.085Z",
         "updatedAt":"2020-06-14T15:21:21.085Z"
      }
   }
     * 
     * 
     */
    .post((req, res) => {
      Cart.destroy({
          where: {
            [Op.and]: [{
                clientId: req.user.uId,
              },
              {
                productId: req.body.productId,
              },
            ],
          },
        })
        .then((d) => {
          console.log(d);
          return Cart.create({
            clientId: req.user.uId,
            productId: req.body.productId,
            quantity: req.body.quantity,
          });
        })
        .then((cart) => res.send(cart))
        .catch((err) =>
          res.status(401).json({
            msg: err.message,
          })
        );
    })
  
    /**
     * 
     * @api {put} /cart add item from smart cart to cart
     * @apiName add from smartcart
     * @apiGroup Cart
     * @apiVersion  1.0.0
     * 
     * @apiHeader  {String} Authorization Token of authenticated user
     * @apiHeader  {String} Content-type content type of response
     * 
     * @apiParam  {String} labels Names of the Smart-cart
     * 
     * 
     * @apiSuccessExample  {JSON} Success-Response:
     *  HTTP/1.1 200 Ok
     * [
          {
        "id": 10,
        "productId": 200,
        "quantity": 74,
        "clientId": "9wokbf7v0ex",
        "createdAt": "2020-07-07T11:53:22.346Z",
        "updatedAt": "2020-07-07T11:53:22.346Z"
        },
          {
        "id": 11,
        "productId": 300,
        "quantity": 98,
        "clientId": "9wokbf7v0ex",
        "createdAt": "2020-07-07T11:53:22.346Z",
        "updatedAt": "2020-07-07T11:53:22.346Z"
        },
          {
        "id": 12,
        "productId": 400,
        "quantity": 154,
        "clientId": "9wokbf7v0ex",
        "createdAt": "2020-07-07T11:53:22.346Z",
        "updatedAt": "2020-07-07T11:53:22.346Z"
        }
      ]
     * 
     * @apiHeaderExample {json} Request-Header:
     *    {
     *        "Content-Type":"application/json",
     *        "Authorization": "JWT 1234.xysd.34hiy"
     *    }
     * 
     * @apiParamExample {JSON} Request-Example:
     * {
     *     "labels" : ["parent","Mother"]
     * }
     * 
     * 
     */
    .put(async (req, res) => {
      const cart = await Cart.findAll({
        where: {
          clientId: req.user.uId,
        },
      });

      SmartCart.findAll({
          group: ["productId"],
          attributes: [
            "productId",
            [
              app.db.Sequelize.fn("SUM", app.db.Sequelize.col("quantity")),
              "quantity",
            ],
          ],
          where: {
            [Op.and]: [{
                label: {
                  [Op.in]: req.body.labels
                }
              },
              {
                clientId: req.user.uId
              },
            ],
          }
        })
        .then((smartcart) => {
          var wholeCart = JSON.parse(JSON.stringify(cart)).concat(JSON.parse(JSON.stringify(smartcart))).map(({
            productId,
            quantity
          }) => ({
            productId,
            quantity: +quantity,
            clientId: req.user.uId
          })).reduce((a, b) => {
            a[b.productId] = {
              ...b,
              quantity: (a[b.productId] == undefined ? 0 : a[b.productId].quantity) + b.quantity
            };
            return a;
          }, {});

          return Cart.destroy({
            where: {
              [Op.and]: [{
                clientId: req.user.uId
              }, ]
            }
          }).then((_) =>
            Cart.bulkCreate(Object.values(wholeCart))
          );
        }).then(d => res.json(d))
        .catch((err) =>
          res.status(406).json({
            msg: err.message,
          })
        );
    });
};