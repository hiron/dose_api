const uniqid = require("uniqid");
const axios = require("axios");
const bcrypt = require("bcrypt");

module.exports = (app) => {
  const Login = app.db.AdminLogin;

  app
    .route("/registration")
    .all(app.auth.authenticate("admin-role"))
    .all(app.adminAccess.of("Add User"))
    /**
     *
     * @api {get} /registration get all registared Admin profile
     * @apiName get agmins profile
     * @apiGroup Registration
     * @apiVersion  1.0.0
     * @apiDescription get agmin's profile
     * @apiPrivate
     * @apiPermission admin
     * @apiSuccess  {String{..15}} username Admin's unique username
     * @apiSuccess  {String{..15}} firstname Admin's first name
     * @apiSuccess  {String{..15}} lastname Admin's last name
     * @apiSuccess  {String} email Admin's email
     * @apiSuccess  {String{8..15}} nid Admin's Password
     * @apiSuccess  {String} phone Admin's phone
     * @apiSuccess  {String=admin, account-admin, agent-admin, customer-admin} type Admin's type
     *
     * @apiSuccess {Boolean{true, false}} state the type of the user profile locked or open
     *
     *
     * @apiSuccessExample {JSON} Success-Response:
     * HTTP/1.1 200 OK
[
    {
        "id": 1,
        "firstName": "Hiron",
        "lastName": "Das",
        "nid": null,
        "address": null,
        "email": "hcdas.09@gmail.com",
        "photo": null,
        "createdAt": "2021-07-08T10:56:39.533Z",
        "updatedAt": "2021-07-08T10:56:39.533Z",
        "AdminLoginId": 8,
        "AdminLogin": {
            "username": "hiron",
            "phone": "01739573213",
            "state": true,
            "type": "admin",
            "AdminType": {
                "id": 1,
                "type": "admin"
            }
        }
    },
    {
        "id": 2,
        "firstName": "SuvasH",
        "lastName": "Das",
        "nid": null,
        "address": null,
        "email": "das.suvash09@gmail.com",
        "photo": null,
        "createdAt": "2021-07-08T11:02:57.354Z",
        "updatedAt": "2021-07-08T17:27:25.059Z",
        "AdminLoginId": 9,
        "AdminLogin": {
            "username": "suvash",
            "phone": "01739573213",
            "state": true,
            "type": "admin",
            "AdminType": {
                "id": 1,
                "type": "admin"
            }
        }
    }
]
     *
     *
     *  @apiErrorExample {json} Error-wrong phone or type:
     *  HTTP/1.1 403 Forbidden Error
     *
     * @apiErrorExample {json} Error-account lock:
     *  HTTP/1.1 401 Unauthorized Error
     *
     * @apiErrorExample {json} Error-email send fail:
     *  HTTP/1.1 412 Precondition Failed
     *
     */

    .get((req, res) => {
      app.db.AdminProfile.findAll({
        attributes: { exclude: ["photo"] },
        include: {
          model: Login,
          attributes: ["username", "phone", "state", "type"],
          include: {
            model: app.db.AdminType,
          },
        },
      })
        .then((user) => res.json(user))
        .catch((err) => res.status(401).json({ msg: err.message }));
    })
    /**
     * @apiDefine admin Admin access rights needed
     *    Only a admin can create user for the application
     */
    /**
     *
     * @api {POST} /registration Admin Registration
     * @apiName PostRegistration
     * @apiGroup Registration
     * @apiVersion  1.0.0
     * @apiDescription Admin user registration api
     * @apiPrivate
     * @apiPermission admin
     * @apiParam  {String{..15}} username Admin's unique username
     * @apiParam  {String{8..13}} password Admin's Password
     * @apiParam  {String} phone Admin's phone
     * @apiParam  {String=admin, account-admin, agent-admin, customer-admin} type Admin's type
     *
     * @apiSuccess {String} msg phone acknowledge msg
     *
     * @apiParamExample  {JSON} Request-Example:
     * {
     *     "phone" : "01874595676",
     *      
            "type": "admin",
            "password": "Hiron@12345",
            "username": "Hiron",
            "firstName":"Hiorn",
            "lastName":"Das",
            "email":"testmail@gmail.com",
            "nid":"12453556",
            "address":"Your Address Here"
        }
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     *  {
            "msg": "username & password is send to the phone"
        }
     *
     *
     *  @apiErrorExample {json} Error-wrong phone or type:
     *  HTTP/1.1 403 Forbidden Error
     *
     * @apiErrorExample {json} Error-account lock:
     *  HTTP/1.1 401 Unauthorized Error
     *
     * @apiErrorExample {json} Error-email send fail:
     *  HTTP/1.1 412 Precondition Failed
     *
     */
    .post((req, res) => {
      if (
        req.body.username == " " ||
        req.body.phone == undefined ||
        req.body.type == undefined
      ) {
        res.status(412).json({
          msg: "Precondition Failed!! phone or type wrong",
        });
      } else {
        Login.create({
          ...req.body,
          phone:
            req.body.phone.length < 14
              ? req.body.phone.substr(-11)
              : req.body.phone,
          secret: bcrypt.hashSync(req.body.password, 10),
          uId: uniqid.process(),
        })
          .then(async (user) => {
            await app.db.AdminProfile.create({
              ...req.body,
              AdminLoginId: user.id,
            });

            axios
              .get(
                `${process.env.otpHost}&receiver=${user.phone}&message=Your username is ${user.username} and the password is ${req.body.password} . Don't share it to others.`
              )
              .then((info) =>
                res.json({
                  msg: "username & password is send to the phone",
                })
              )
              .catch((err) => res.status(412).json(err.message));
          })
          .catch((err) =>
            res.status(403).json({
              msg: err.message,
            })
          );
      }
    })
    /**
     *
     * @api {PUT} /registration Admin Registration
     * @apiName update admin profile
     * @apiGroup Registration
     * @apiVersion  1.0.0
     * @apiDescription Update admin profile
     * @apiPrivate
     * @apiPermission admin
     * @apiParam  {String{..15}} username Admin's unique username
     * @apiParam  {String{8..13}} password Admin's Password
     * @apiParam  {String} phone Admin's phone
     * @apiParam  {String=admin, account-admin, agent-admin, customer-admin} type Admin's type
     *
     * @apiSuccess {String} msg phone acknowledge msg
     *
     * @apiParamExample  {JSON} Request-Example:
     * {
     *      "adminId":5,
     *     "phone" : "01874595676",
            "type": "admin",
            "password": "Hiron@12345",
            "firstName":"Hiorn",
            "lastName":"Das",
            "email":"testmail@gmail.com",
            "nid":"12453556",
            "address":"Your Address Here"
        }
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     *  {
            "msg": "username & password is send to the phone"
        }
     *
     *
     *  @apiErrorExample {json} Error-wrong phone or type:
     *  HTTP/1.1 403 Forbidden Error
     *
     * @apiErrorExample {json} Error-account lock:
     *  HTTP/1.1 401 Unauthorized Error
     *
     * @apiErrorExample {json} Error-email send fail:
     *  HTTP/1.1 412 Precondition Failed
     *
     */
    .put(async (req, res) => {
      app.db.AdminLogin.findOne(
        //{ password: req.body.password, phone: req.body.phone },
        {
          include: {
            model: app.db.AdminProfile,
            where: { id: req.body.adminId },
          },
        }
      )
        .then(async (data) => {
          if (!data) throw new Error("No Record Found!!");

          Object.keys(req.body).forEach((d) => {
            data[d] = req.body[d];
            data.AdminProfile[d] = req.body[d];
          });

          if (!!req.body.password)
            data.secret = bcrypt.hashSync(req.body.password, 10);
          //AdminProfile: req.body,
          await data.AdminProfile.save();
          data
            .save()
            .then((user) =>
              !!req.body.password
                ? axios.get(
                    `${process.env.otpHost}&receiver=${user.phone}&message=Your username is ${user.username} and the password is ${req.body.password} . Don't share it to others.`
                  )
                : res.json({
                    msg: "Profile is updated!!",
                  })
            )
            .then((info) =>
              res.json({
                msg: "username & password is send to the phone",
              })
            )
            .catch((err) => res.status(401).json({ msg: err.message }));
        })
        .catch((err) => res.status(403).json({ msg: err.message }));

      // } else {
      //   app.db.AdminProfile.update(req.body, {
      //     where: { id: req.body.adminId },
      //     include: { model: app.db.AdminLogin },
      //   })
      //     .then((data) => res.json(data))
      //     .catch((err) => res.status(403).json({ msg: err.message }));
      // }
    });
};
