const fs = require("fs");

module.exports = (app) => {
  app
    .route("/upload/:id")
    .all(app.auth.authenticate())
    /**
     *
     * @api {delete} /upload/:id delete uploaded image file
     * @apiName delete image
     * @apiGroup Prescription
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {id} id of the deleted prescription image
     *
     * @apiSuccessExample {json} Success-Response:
     * 1
     */
    .delete(async (req, res) => {
      let img = await app.db.Prescription.findOne({
        where: { id: req.params.id, clientId: req.user.uId },
      }).then((d) => d.image);
      fs.unlink("public/prescription/" + img, (err) => {
        if (err) {
          console.log(err);
          res.sendStatus(500);
        } else {
          app.db.Prescription.destroy({
            where: { id: req.params.id },
          })
            .then((d) => res.json(d))
            .catch((e) => res.status(401).json({ msg: e.message }));
        }
      });
    });
};
