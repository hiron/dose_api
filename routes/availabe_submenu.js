module.exports = (app) => {
  app
    .route("/submenu/:id")
    .all(app.auth.authenticate("admin-role"))
    .all(app.adminAccess.of("User Type"))
    /**
       * 
       * @api {get} /submenu/:id get assign submenu for the user
       * @apiDescription user get submenue for this Adminuser
       * @apiName submenus
       * @apiGroup User Management
       * @apiPrivate
       * @apiVersion  1.0.0
       * @apiHeader  {String} Authorization Admin's unique token
       * 
       * @apiSuccess {Number} id id of the admin user
       * @apiSuccess {String} type type of the user
       * @apiSuccess {String} path route path of the menu
       * @apiSuccess {String} subMenuName sumMenu of the menu
       * 
       * @apiHeaderExample  {json} Header:
        * {
        *     "Authorization" : "JWT 1234.xyde.4563"
        * }
       *
       *
       * 
       * 
       * @apiSuccessExample {JSON} Success-Response:
       * HTTP/1.1 200 OK
       * [
        {
            "id": 1,
            "path": "/main/dashboard",
            "subMenuName": "Dashboard",
            "MenuId": 1,
            "C": {
                "createdAt": "2021-07-07T09:29:59.144Z",
                "updatedAt": "2021-07-07T09:29:59.144Z",
                "AdminTypeId": 2,
                "SubMenuId": 1
            }
        },
        {
            "id": 2,
            "path": "/main/agent",
            "subMenuName": "Agent List",
            "MenuId": 2,
            "C": {
                "createdAt": "2021-07-07T09:29:59.144Z",
                "updatedAt": "2021-07-07T09:29:59.144Z",
                "AdminTypeId": 2,
                "SubMenuId": 2
            }
        },
        {
            "id": 3,
            "path": "/main/request",
            "subMenuName": "Agent Pending Request",
            "MenuId": 2,
            "C": {
                "createdAt": "2021-07-07T09:29:59.144Z",
                "updatedAt": "2021-07-07T09:29:59.144Z",
                "AdminTypeId": 2,
                "SubMenuId": 3
            }
        },
        {
            "id": 4,
            "path": "/main/clear",
            "subMenuName": "Clear Payment",
            "MenuId": 3,
            "C": {
                "createdAt": "2021-07-07T09:29:59.144Z",
                "updatedAt": "2021-07-07T09:29:59.144Z",
                "AdminTypeId": 2,
                "SubMenuId": 4
            }
        },
        {
            "id": 5,
            "path": "/main/due",
            "subMenuName": "Due Payment",
            "MenuId": 3,
            "C": {
                "createdAt": "2021-07-07T09:29:59.144Z",
                "updatedAt": "2021-07-07T09:29:59.144Z",
                "AdminTypeId": 2,
                "SubMenuId": 5
            }
        },
        {
            "id": 6,
            "path": "/main/expired",
            "subMenuName": "Expired Due",
            "MenuId": 3,
            "C": {
                "createdAt": "2021-07-07T09:29:59.144Z",
                "updatedAt": "2021-07-07T09:29:59.144Z",
                "AdminTypeId": 2,
                "SubMenuId": 6
            }
        },
        {
            "id": 7,
            "path": "/main/order",
            "subMenuName": "Order List",
            "MenuId": 4,
            "C": {
                "createdAt": "2021-07-07T09:29:59.144Z",
                "updatedAt": "2021-07-07T09:29:59.144Z",
                "AdminTypeId": 2,
                "SubMenuId": 7
            }
        },
        {
            "id": 8,
            "path": "/main/runing-order",
            "subMenuName": "On-going Order",
            "MenuId": 4,
            "C": {
                "createdAt": "2021-07-07T09:29:59.144Z",
                "updatedAt": "2021-07-07T09:29:59.144Z",
                "AdminTypeId": 2,
                "SubMenuId": 8
            }
        },
        {
            "id": 9,
            "path": "/main/history",
            "subMenuName": "Order History",
            "MenuId": 4,
            "C": {
                "createdAt": "2021-07-07T09:29:59.144Z",
                "updatedAt": "2021-07-07T09:29:59.144Z",
                "AdminTypeId": 2,
                "SubMenuId": 9
            }
        },
        {
            "id": 10,
            "path": "/main/client",
            "subMenuName": "Client List",
            "MenuId": 5,
            "C": {
                "createdAt": "2021-07-07T09:29:59.144Z",
                "updatedAt": "2021-07-07T09:29:59.144Z",
                "AdminTypeId": 2,
                "SubMenuId": 10
            }
        },
        {
            "id": 11,
            "path": "/main/medicine",
            "subMenuName": "Medicine List",
            "MenuId": 6,
            "C": {
                "createdAt": "2021-07-07T09:29:59.144Z",
                "updatedAt": "2021-07-07T09:29:59.144Z",
                "AdminTypeId": 2,
                "SubMenuId": 11
            }
        },
        {
            "id": 16,
            "path": "/main/assign",
            "subMenuName": "Menu Management",
            "MenuId": 9,
            "C": {
                "createdAt": "2021-07-07T09:29:59.144Z",
                "updatedAt": "2021-07-07T09:29:59.144Z",
                "AdminTypeId": 2,
                "SubMenuId": 16
            }
        },
        {
            "id": 17,
            "path": "/main/user",
            "subMenuName": "Create User",
            "MenuId": 9,
            "C": {
                "createdAt": "2021-07-07T09:29:59.144Z",
                "updatedAt": "2021-07-07T09:29:59.144Z",
                "AdminTypeId": 2,
                "SubMenuId": 17
            }
        }
    ]
       * 
       */
    .get((req, res) => {
      //console.log(req);
      app.db.AdminType.findOne({
        where: { id: req.params.id },
        include: [{ model: app.db.SubMenu, as: "subMenu" }],
      })
        .then((menu) => res.json(menu.subMenu))
        .catch((err) => {
          res.status(401).json({ msg: err.message });
        });
    });
};
