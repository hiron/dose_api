module.exports = (app) => {
  const UserClient = app.db.UserClient;

  app
    .route("/client")
    .all(app.auth.authenticate())
    /**
     *
     * @api {get} /client Return the authenticated user's data
     * @apiName Get user
     * @apiGroup Client
     * @apiVersion  1.0.0
     *
     *
     * @apiHeader  {String} Authorization Token of authenticated user
     *
     * @apiSuccess  {String} firstName Clients first name
     * @apiSuccess  {String} lastName Clients last name
     * @apiSuccess  {Number} age Client's age
     * @apiSuccess  {Date} birthDate Date of birth
     * @apiSuccess  {String} phone client's phone number
     * @apiSuccess  {String} email Client's email address
     * @apiSuccess  {String} sex Client's gender
     * @apiSuccess  {String} nid Client's national ID
     * @apiSuccess  {String} level=null Client's current level
     * @apiSuccess  {Boolean} emailStatus=false email is varified or no?
     *
     * @apiHeaderExample  {json} Header:
     * {
     *     "Authorization" : "JWT 1234.xyde.4563"
     * }
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 Ok
     * {
     *    "birthDate": "1989-03-17",
     *    "age": 31,
     *    "firstName": "Hiron",
     *    "lastName": "Das",
     *    "phone": "01739573212",
     *    "email": "hcdas.09@gmail.com",
     *    "sex": "male",
     *    "nid": "2693717",
     *    "level": null,
     *    "emailStatus": false
     *  }
     *
     *  @apiErrorExample {json} Error-Response:
     *  HTTP/1.1 404 not found
     *
     */
    .get((req, res) => {
      UserClient.findOne({
          where: {
            clientId: req.user.uId
          },
          include: [app.db.Login],
          attributes: [
            "firstName",
            "lastName",
            "email",
            "age",
            "birthDate",
            "sex",
            "nid",
            "level",
            "emailStatus",
          ],
        })
        .then((usr) => {
          console.log(JSON.parse(JSON.stringify(usr)));
          res.json(
            usr !== null ?
            [JSON.parse(JSON.stringify(usr))].map(({
              Login,
              ...d
            }) => ({
              ...d,
              phone: Login.phone,
            }))[0] :
            usr
          );
        })
        .catch((err) => res.status(404).json(err.message));
    })
    /**
     *
     * @api {post} /client Insert client details
     * @apiName add client
     * @apiGroup Client
     * @apiVersion  1.0.0
     *
     *
     * @apiHeader  {String} Content-type content type of response
     * @apiHeader  {String} Authorication Token of authenticated user
     *
     * @apiParam  {String} firstName Clients first name
     * @apiParam  {String} [lastName] Clients last name
     * @apiParam  {Date} [birthDate] Date of birth
     * @apiParam  {String} [email] Client's email address
     * @apiParam  {String} [sex=male] Client's gender
     * @apiParam  {String} [nid] Client's national ID
     *
     *
     * @apiParamExample  {json} Request-Example:
     * {
     *    "birthDate": "1989-03-17",
     *    "firstName": "Hiron",
     *    "lastName": "Das",
     *    "email": "hcdas.09@gmail.com",
     *    "sex": "male",
     *    "nid": "2693717",
     *  }
     *
     *
     * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 204 No Content
     * @apiHeaderExample {json} Request-Example:
     *    {
     *        "Content-Type":"application/json",
     *        "Authorization": "JWT 1234.xysd.34hiy"
     *    }
     * @apiErrorExample {json} Error-Response:
     *  HTTP/1.1 403 Forbidden
     *
     */

    .post((req, res) => {
      console.log(req.body.birthDate);
      UserClient.create({
          ...req.body,
          birthDate: req.body.birthDate == null ?
            null :
            new Date(req.body.birthDate),
          clientId: req.user.uId,
        })
        .then((user) => res.status(200).json(user))
        .catch((err) => res.status(403).json({
          msg: err.message
        }));
    })
    /**
     *
     * @api {put} /client Update client's info
     * @apiName Update client
     * @apiGroup Client
     * @apiVersion  1.0.0
     *
     *
     * @apiHeader  {String} Content-type Application content type
     * @apiHeader  {String} Authorization Token of authorized user
     * @apiHeaderExample {json} Request-Example:
     *    {
     *        "Content-Type":"application/json",
     *        "Authorization": "JWT 1234.xysd.34hiy"
     *    }
     *
     *
     * @apiParam  {String} firstName Clients first name
     * @apiParam  {String} lastName Clients last name
     * @apiParam  {Date} birthDate Date of birth
     * @apiParam  {String} sex Client's gender
     * @apiParam  {String} nid Client's national ID
     *
     *
     * @apiParamExample  {json} Request-Example:
     * {
     *    "birthDate": "1989-03-17",
     *    "firstName": "Hiron",
     *    "lastName": "Das",
     *    "sex": "male",
     *    "nid": "2693717",
     *  }
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 204 No Content
     *
     *
     */
    .put(async (req, res) => {
      const client = await UserClient.findOne({
        where: {
          clientId: req.user.uId
        },
      });
      client.firstName = req.body.firstName || client.firstName;
      client.lastName = req.body.lastName || client.lastName;
      client.sex = req.body.sex || client.sex;
      client.nid = req.body.nid || client.nid;
      client.email = req.body.email|| client.email;
      client.birthDate = req.body.birthDate ?
        new Date(req.body.birthDate) :
        new Date(client.birthDate);

      client.emailStatus = req.body.email !== null? false : client.emailStatus;

      await client.save();
      res.status(200).json(client);
    });
};