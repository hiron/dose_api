const { Op } = require("sequelize");

module.exports = (app) => {
  const Address = app.db.ShopAddress;
  app
    .route("/shopaddress")
    .all(app.auth.authenticate())
    /**
         * 
         * @api {get} /shopaddress Get address of the shop
         * @apiName Get shop Address
         * @apiGroup Address
         * @apiVersion  1.0.0
         * 
         * 
         * @apiHeader  {String} Content-type content type of the request
         * @apiHeader  {String} Authorization User's authentication token
         * 
         * @apiSuccess {Number} lat Latitude of the address
         * @apiSuccess {Number} long Longitude of the address
         * @apiSuccess {Number} area area id of the address
         * @apiSuccess {String} name Name of the Shop
         * @apiSuccess {Number} id Address id
         * 
         * @apiHeaderExample  {json} Header-Example:
         * {
         *     Content-Type : "aplication/json"
         *     Authorization : "JWT xyz.1234.wxyz"
         * }
         * 
         * 
         * @apiSuccessExample {json} Success-Response:
         * HTTP/1.1 200 Ok
                 {
                      "id": 2,
                      "lat": 34.23,
                      "long": 23.45,
                      "name": "Test shop",
                      "area": 1,
                      "Area": {
                          "id": 1,
                          "district": "Dhaka",
                          "area": "Adabar"
                      }
                  }
         * 
         * @apiErrorExample {json} Authentication error:
         *  HTTP/1.1 401 Unauthorized
         * 
         */
    .get((req, res) => {
      Address.findOne({
        where: {
          agentId: req.user.uId,
        },
        attributes: { exclude: ["agentId", "createdAt", "updatedAt"] },
        include: [app.db.Area],
      })
        .then((address) => res.json(address))
        .catch((err) =>
          res.status(401).json({
            msg: err.message,
          })
        );
    })
    /**
         * 
         * @api {post} /shopaddress Set address of the shop
         * @apiName Set shop Address
         * @apiGroup Address
         * @apiVersion  1.0.0
         * 
         * 
         * @apiHeader  {String} Content-type content type of the request
         * @apiHeader  {String} Authorization User's authentication token
         * 
         * @apiParam {Number} lat Latitude of the address
         * @apiParam {Number} long Longitude of the address
         * @apiParam {String} shopName Name of the Shop
         * @apiParam {String} district district of the area
         * @apiParam {String} area Area of the Shop
         * 
         * @apiSuccess {String} agentId user's Id
         * @apiSuccess {Number} id shopAddress id
         * 
         * @apiHeaderExample  {json} Header-Example:
         * {
         *     Content-Type : "aplication/json"
         *     Authorization : "JWT xyz.1234.wxyz"
         * }
         
         * @apiParamExample {json} Param-Request:
            {
              "lat": 32.86,
              "long": 2358.98,
              "shopName": "Shop Name",
              "district": "Dhaka",
              "area": "Adabar"
            }
         * 
         * @apiSuccessExample {json} Success-Response:
         * HTTP/1.1 200 Ok
           {
    "id": 4,
    "agentId": "474kffcmsao",
    "lat": 32.86,
    "long": 2358.98,
    "area": 1,
    "name": "Shop Name",
    "updatedAt": "2020-09-23T17:22:06.660Z",
    "createdAt": "2020-09-23T17:22:06.660Z"
}
         * 
         * @apiErrorExample {json} Not Acceptable error:
         *  HTTP/1.1 406 Not Acceptable
         * 
         */
    .post((req, res) => {
      app.db.UserAgent.update(
        { addressId: null},
        { where: { agentId: req.user.uId } }
      )
        .then((_) =>
          Address.destroy({
            where: {
              agentId: req.user.uId,
            },
          })
        )
        .then((_) =>
          app.db.Area.findOne({
            where: {
              [Op.and]: {
                district: req.body.district,
                area: req.body.area,
              },
            },
          })
        )
        .then((d) => {
          console.log(d);
          return Address.create({
            agentId: req.user.uId,
            lat: req.body.lat,
            long: req.body.long,
            area: d.id,
            name: req.body.shopName,
          });
        })
        .then((address) =>
          app.db.UserAgent.update(
            { addressId: address.id, pharmacy: address.name },
            { where: { agentId: req.user.uId } }
          ).then((_) => res.send(address))
        )
        .catch((err) =>
          res.json({
            msg: err.message,
          })
        );
    });
};
