const { Op } = require("sequelize");

module.exports = (app) => {
  app
    .route("/history")
    .all(app.auth.authenticate())
    /**
     * 
     * @api {get} /history Get all done/cancel order List
     * @apiName All Delivered Products
     * @apiGroup History
     * @apiVersion  1.0.0
     * 
     * 
     * @apiHeader  {String} Authorization User Authorization Token
     * 
     * @apiSuccess (200) {String} paymentMethod Payment Method of this order
     * @apiSuccess (200) {String} paymentType Payment type of this order
     * @apiSuccess (200) {String} txnId Transection Id of the payment
     * @apiSuccess (200) {Number} price Total cost of the Order
     * @apiSuccess (200) {Number} discountPrice Price after discount
     * @apiSuccess (200) {Number} discount Persentage of the discount
     *  @apiSuccess (200) {Number} agentCharge Agent amount of the delivery charge
     * @apiSuccess (200) {Number} doseCharge Dose amount of the delivery charge
     * @apiSuccess (200) {Number} agentCommission Persentage of the discount
     * @apiSuccess (200) {Date} deliveredAt Date of product delivery
     * @apiSuccess (200) {Date} confirmedAt Date of confirm delivery
     * @apiSuccess (200) {id} addressId Id of the delivery address
     * @apiSuccess (200) {Boolean} done=false Delivery Confirmation Flag
     * @apiSuccess (200) {Boolean} cancel=false Delivery Cancelation Flag
     * @apiSuccess (200) {String} cancelBy id of the user(agent/client) who cancel the order
     * @apiSuccess (200) {String} invoiceId Id of the order
     * @apiSuccess (200) {Number} clientId Id of the Client
     * @apiSuccess (200) {Number} agentId Id of the agent who receive the order
     * @apiSuccess (200) {Number} cancelCauseId Id of the cancel cause
     * 
     * @apiHeaderExample  {json} Header-Example:
     * {
     *     "Authorization" : "1234.vchdtr.xyer"
     * }
     * 
     * 
     * @apiSuccessExample {JSON} Success-Response:
     *[
    {
          "id": 1,
          "paymentMethod": null,
          "paymentType": "postpay",
          "txnId": null,
          "price": 44.96,
          "discountPrice": 44.96,
          "deliveryCharge": 30,
          "prescription": null,
          "discount": 0,
          "doseCharge": null,
          "agentCharge": 20,
          "agentCommission": null,
          "doseCommission": null,
          "deliveredAt": null,
          "confirmedAt": null,
          "addressId": 1,
          "done": null,
          "cancel": null,
          "cancelBy": null,
          "invoiceId": "99CN99NK",
          "clientId": 1,
          "agentId": null,
          "createdAt": "2020-07-23T15:24:53.637Z",
          "updatedAt": "2020-07-23T15:24:53.637Z",
          "CancelCauseId": null
      }
  ]
     * 
     * 
     */
    .get((req, res) =>
      app.db.UserClient.findOne({
        where: { clientId: req.user.uId },
        include: [
          {
            model: app.db.Invoice,
            where: { [Op.or]: [{ cancel: true }, { done: true }] },
            include: [{ model: app.db.Rating }],
          },
        ],
      })
        .then((d) =>
          d.Invoices == undefined ? res.json([]) : res.json(d.Invoices)
        )
        .catch((e) => res.status(401).json({ msg: e.message }))
    ); 
};
