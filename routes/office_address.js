module.exports = (app) => {
  app
    .route("/office")
    /**
   * 
   * @api {get} /office Get Office Address
   * @apiName office Address
   * @apiGroup Office Address
   * @apiVersion  1.0.0
   * 
   * @apiSuccess {Number} lat Latitude of the address
         * @apiSuccess {Number} long Longitude of the address
         * @apiSuccess {String} address Address detail of the office
         * @apiSuccess {String} email email of the office
         * @apiSuccess {String} phone Phone address of the Office
         * @apiSuccess {Number} id Address id
   * 
   * 
   * 
   * @apiSuccessExample {json} Success-Response:
   * {
    "id": 1,
    "lat": 23.3658597,
    "long": 40.9562554,
    "address": "12ka block-b Babor Road, Adabor Dhaka-1207",
    "phone": "+8801521257488, +8801739573213",
    "email": "dosebd@outlook.com",
    "createdAt": "2020-08-15T14:09:06.907Z",
    "updatedAt": "2020-08-15T14:09:06.907Z"
}
   * 
   * 
   */
    .get((req, res) => {
      app.db.OfficeAddress.findOne().then((d) => res.json(d));
    });
};
