const { Op } = require("sequelize");

module.exports = (app) => {
  app
    .route("/cart/:id")
    .all(app.auth.authenticate())
    /**
     *
     * @api {delete} /cart/:id Delete items from cart
     * @apiName delete item
     * @apiGroup Cart
     * @apiVersion  1.0.0
     *
     * @apiHeader  {String} Authorization User's authentication token
     * @apiHeader  {String} Content-type content type header
     *
     *
     * @apiHeaderExample  {JSON} Request-Header:
     * {
     *     "Authorization" : "JWT 1234.xyde.4563"
     *     "Content-Type" : "application/json"
     * }
     *
     *
     * @apiSuccessExample {JSON} Success-Response:
     *  1
     *
     *
     */
    .delete((req, res) => {
      console.log(req.params.id);
      app.db.Cart.destroy({
        where: {
          [Op.and]: [
            {
              clientId: req.user.uId,
            },
            {
              id: req.params.id,
            },
          ],
        },
      })
        .then((d) => res.json(d))
        .catch((err) =>
          res.status(406).json({
            msg: err.message,
          })
        );
    });
};
