module.exports = (app) => {
  app
    .route("/assign-menu")
    .all(app.auth.authenticate("admin-role"))
    /**
       * 
       * @api {post} /assign-menu set submenus for the admain 
       * @apiDescription Admin assign menu to users
       * @apiName Assign Menus
       * @apiGroup User Management
       * @apiPrivate
       * @apiVersion  1.0.0
       * @apiHeader  {String} Authorization Admin's unique token
       * 
       * @apiParam {String} typeId if of the adminType
       * @apiParam {String} subMenuId Array of subMenu ids
       * 
       * @apiHeaderExample  {JSON} Header:
        * {
        *     "Authorization" : "JWT 1234.xyde.4563"
        * }
       *
       *
       * 
       * @apiParamExample {JSON} Header:
       * {
        *        "typeId": 1,
        *        "subMenuId": [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
        * }
       * 
       * @apiSuccessExample {JSON} Success-Response:
       * HTTP/1.1 200 OK
       * []
       * 
       */
    .post(async (req, res) => {
        let adminType = await app.db.AdminType.findOne({where:{id: req.body.typeId}});
        let subMenus = await app.db.SubMenu.findAll({where:{id: req.body.subMenuId}});
        adminType.setSubMenu(subMenus,{through:{}})
        .then((data) => res.json(data))
        .catch((err) => {
          res.status(401).json({ msg: err.message });
        });
    });
};
