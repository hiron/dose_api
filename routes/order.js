var customId = require("custom-id");

module.exports = (app, io) => {
  //console.log(app);
  app
    .route("/order")
    .all(app.auth.authenticate())
    /**
     * 
     * @api {get} /order Get all order List
     * @apiName All available undelivered Products
     * @apiGroup Order
     * @apiVersion  1.0.0
     * 
     * 
     * @apiHeader  {String} Authorization User Authorization Token
     * 
     * @apiSuccess (200) {String} paymentMethod Payment Method of this order
     * @apiSuccess (200) {String} paymentType Payment type of this order
     * @apiSuccess (200) {String} txnId Transection Id of the payment
     * @apiSuccess (200) {Number} price Total cost of the Order
     * @apiSuccess (200) {Number} discountPrice Price after discount
     * @apiSuccess (200) {Number} discount Persentage of the discount
     * @apiSuccess (200) {Number} agentCharge Agent amount of the delivery charge
     * @apiSuccess (200) {Number} doseCharge Dose amount of the delivery charge
     * @apiSuccess (200) {Number} agentCommission Persentage of the discount
     * @apiSuccess (200) {Date} deliveredAt Date of product delivery
     * @apiSuccess (200) {Date} confirmedAt Date of confirm delivery
     * @apiSuccess (200) {id} addressId Id of the delivery address
     * @apiSuccess (200) {Boolean} done=false Delivery Confirmation Flag
     * @apiSuccess (200) {Boolean} cancel=false Delivery Cancelation Flag
     * @apiSuccess (200) {String} cancelBy id of the user(agent/client) who cancel the order
     * @apiSuccess (200) {String} invoiceId Id of the order
     * @apiSuccess (200) {Number} clientId Id of the Client
     * @apiSuccess (200) {Number} agentId Id of the agent who receive the order
     * @apiSuccess (200) {Number} cancelCauseId Id of the cancel cause
     * 
     * @apiHeaderExample  {json} Header-Example:
     * {
     *     "Authorization" : "1234.vchdtr.xyer"
     * }
     * 
     * 
     * @apiSuccessExample {JSON} Success-Response:
     *[
{
        "id": 2,
        "paymentMethod": null,
        "paymentType": "postpay",
        "txnId": null,
        "price": 166.96,
        "discountPrice": 166.96,
        "deliveryCharge": 30,
        "prescription": null,
        "discount": 0,
        "doseCharge": null,
        "agentCharge": 20,
        "agentCommission": 158.612,
        "doseCommission": null,
        "deliveredAt": null,
        "confirmedAt": null,
        "addressId": 1,
        "done": false,
        "cancel": false,
        "cancelBy": null,
        "invoiceId": "85EI88MU",
        "clientId": 1,
        "agentId": null,
        "createdAt": "2020-07-24T05:42:06.606Z",
        "updatedAt": "2020-07-24T05:42:06.606Z",
        "CancelCauseId": null,
        "Address": {
            "id": 1,
            "lat": 34.23,
            "long": 123.45,
            "address": "this is a test address",
            "phone": "01521257488",
            "name": "Hiron",
            "label": "Home",
            "createdAt": "2020-07-23T15:20:55.668Z",
            "updatedAt": "2020-07-23T15:20:55.668Z",
            "userId": "acgkcywo60v"
        },
        "Orders": [
            {
                "id": 3,
                "quantity": 33,
                "price": 10.56,
                "clientId": "acgkcywo60v",
                "productId": 100,
                "invoiceId": 2,
                "createdAt": "2020-07-24T05:42:06.638Z",
                "updatedAt": "2020-07-24T05:42:06.638Z",
                "InvoiceId": 2,
                "ProductId": 100
            },
            {
                "id": 4,
                "quantity": 133,
                "price": 106.4,
                "clientId": "acgkcywo60v",
                "productId": 500,
                "invoiceId": 2,
                "createdAt": "2020-07-24T05:42:06.638Z",
                "updatedAt": "2020-07-24T05:42:06.638Z",
                "InvoiceId": 2,
                "ProductId": 500
            },
            {
                "id": 5,
                "quantity": 5,
                "price": 50,
                "clientId": "acgkcywo60v",
                "productId": 1500,
                "invoiceId": 2,
                "createdAt": "2020-07-24T05:42:06.638Z",
                "updatedAt": "2020-07-24T05:42:06.638Z",
                "InvoiceId": 2,
                "ProductId": 1500
            }
        ]
    },
    {
        "id": 3,
        "paymentMethod": null,
        "paymentType": "postpay",
        "txnId": null,
        "price": 238,
        "discountPrice": 238,
        "deliveryCharge": 30,
        "prescription": null,
        "discount": 0,
        "doseCharge": null,
        "agentCharge": 20,
        "agentCommission": 226.1,
        "doseCommission": null,
        "deliveredAt": null,
        "confirmedAt": null,
        "addressId": 1,
        "done": false,
        "cancel": false,
        "cancelBy": null,
        "invoiceId": "20NE21XG",
        "clientId": 1,
        "agentId": null,
        "createdAt": "2020-07-24T05:51:08.388Z",
        "updatedAt": "2020-07-24T05:51:08.388Z",
        "CancelCauseId": null,
        "Address": {
            "id": 1,
            "lat": 34.23,
            "long": 123.45,
            "address": "this is a test address",
            "phone": "01521257488",
            "name": "Hiron",
            "label": "Home",
            "createdAt": "2020-07-23T15:20:55.668Z",
            "updatedAt": "2020-07-23T15:20:55.668Z",
            "userId": "acgkcywo60v"
        },
        "Orders": [
            {
                "id": 6,
                "quantity": 5,
                "price": 50,
                "clientId": "acgkcywo60v",
                "productId": 1500,
                "invoiceId": 3,
                "createdAt": "2020-07-24T05:51:08.571Z",
                "updatedAt": "2020-07-24T05:51:08.571Z",
                "InvoiceId": 3,
                "ProductId": 1500
            },
            {
                "id": 7,
                "quantity": 235,
                "price": 188,
                "clientId": "acgkcywo60v",
                "productId": 500,
                "invoiceId": 3,
                "createdAt": "2020-07-24T05:51:08.571Z",
                "updatedAt": "2020-07-24T05:51:08.571Z",
                "InvoiceId": 3,
                "ProductId": 500
            }
        ]
    },
  ]
     * 
     * 
     */
    .get((req, res) =>
      app.db.UserClient.findOne({
        where: { clientId: req.user.uId },
        include: [
          {
            model: app.db.Invoice,
            where: { cancel: false, done: false },
            include: [
              { model: app.db.ShippingAddress, as: "Address" },
              {
                model: app.db.Order,
                //group: "productId",
                // attributes: [
                //   "productId",
                //   [
                //     app.db.Sequelize
                //     .fn(
                //       "SUM",
                //       app.db.Sequelize.col("quantity")
                //     ),
                //     "item",
                //   ],
                // ],
              },
            ],
          },
        ],
      })
        .then((d) => res.json(d.Invoices))
        .catch((e) => res.status(404).json({ msg: e.message }))
    )
    /**
     *
     * @api {post} /order Confirm a Order
     * @apiName Confirm Order
     * @apiGroup Order
     * @apiVersion  1.0.0
     *
     *  * @apiHeader  {String} Authorization User Authorization Token
     *  * @apiHeader  {String} Content-Type Content type of the post document
     *
     * @apiParam  {String} paymentType Type of the payment
     * @apiParam  {String} paymentMethod Method of the payment
     * @apiParam  {String} txnId Transection id of the payment
     * @apiParam  {String} addressId Address id of the delivery location
     *
     *
     * @apiParamExample  {JSON} Request-Example:
     * {
     * "paymentType":"postpay",
     * "paymentMethod": null,
     * "addressId": 1,
     * "txnId":null
     * }
     *
     *  @apiHeaderExample  {json} Header-Example:
     * {
     *     "Authorization" : "1234.vchdtr.xyer",
     *     "Content-Type": "application/json"
     * }
     * @apiSuccessExample {JSON} Success-Response:
     * {
     *"invoiceId": "32FB83IH"
     *}
     *
     */

    .post(async (req, res) => {
      var cart = await app.db.Cart.findAll({
        where: { clientId: req.user.uId },
        include: [{ model: app.db.Product }],
      });

      var discountRule = await app.db.UserClient.findOne({
        where: { clientId: req.user.uId },
        include: [{ model: app.db.DiscountRule }],
      })
        .then((d) => d.DiscountRule)
        .catch((e) => console.log(e));

      var totalPrice = JSON.parse(JSON.stringify(cart)).reduce(
        (a, d) => (a += d.Product.price * d.quantity),
        0
      );

      var userId = await app.db.UserClient.findOne({
        where: { clientId: req.user.uId },
      })
        .then((d) => d.id)
        .catch((e) => console.log(e));

      console.log(userId);

      var invoiceId = customId({});

      // let imageName = await app.db.Prescription.findOne({
      //   where: { clientId: req.user.uId },
      // })
      //   .then((d) => d.image)
      //   .catch((_) => null);

      var shippingId = await app.db.Address.findOne({
        where: { id: req.body.addressId },
      })
        .then((d) => {
          var { id, ...data } = JSON.parse(JSON.stringify(d));
          return app.db.ShippingAddress.create(data);
        })
        .then((d) => d.id)
        .catch((e) => console.log(e));

      await app.db.Invoice.create({
        paymentMethod: req.body.paymentMethod,
        paymentType: req.body.paymentType,
        txnId: req.body.txnId,
        price: totalPrice, //req.body.price,
        deliveryCharge: discountRule.delivery,
        discount: discountRule.discount,
        addressId: shippingId,
        discountPrice: totalPrice - (totalPrice * discountRule.discount) / 100,
        // doseCharge: +discountRule.delivery / 3,
        agentCharge: (+discountRule.delivery * 2) / 3,
        agentCommission: totalPrice * 0.95,
        clientId: userId,
        invoiceId: invoiceId,
        //Prescription: imageName,
      })
        .then((invoice) =>
          app.db.Order.bulkCreate(
            JSON.parse(JSON.stringify(cart)).map(
              ({ id, Product, createdAt, updatedAt, ...d }) => {
                console.log(d);
                return {
                  ...d,
                  price: Product.price * d.quantity,
                  productId: Product.id,
                  invoiceId: invoice.id,
                };
              }
            )
          )
        )
        .then((_) =>
          app.db.Cart.destroy({
            where: { clientId: req.user.uId },
          })
        )
        .then((_) =>{
          console.log(invoiceId);
          return app.db.Prescription.update(
            { invoiceId: invoiceId },
            {
              where: { clientId: req.user.uId, invoiceId: null },
            }
          );
          })
        .then(async (_) => {
          let orders = await app.db.Invoice.findAll({
            where: { cancel: false, done: false, agentId: null },
            include: [{ model: app.db.ShippingAddress, as: "Address" }],
          }).then((d) => d);

          io.emit("alertMsg", {
            order: orders.length,
            title: "New Order Request",
            body: "New Available Orders " + orders.length,
          });

          io.emit("orderInfo", {
            order: orders,
            orderCount: orders.length,
          });
          res.json({ invoiceId: invoiceId });
        })
        .catch((e) => res.status(401).json({ msg: e.message }));
    });
};
