module.exports = (app) => {
  app
    .route("/menu")
    .all(app.auth.authenticate("admin-role"))
    /**
       * 
       * @api {get} /menu get available menu for the user
       * @apiDescription user get side menue data
       * @apiName menu
       * @apiGroup Menu
       * @apiPrivate
       * @apiVersion  1.0.0
       * @apiHeader  {String} Authorization Admin's unique token
       * 
       * @apiSuccess {String} icon hex value of menu icon
       * @apiSuccess {String} menuName name of the menu
       * @apiSuccess {String} path route path of the menu
       * @apiSuccess {String} subMenuName sumMenu of the menu
       * 
       * @apiHeaderExample  {json} Header:
        * {
        *     "Authorization" : "JWT 1234.xyde.4563"
        * }
       *
       *
       * 
       * 
       * @apiSuccessExample {json} Success-Response:
       * HTTP/1.1 200 OK
       * [
    {
        "id": 1,
        "icon": "0xf0db",
        "menuName": "Menu",
        "SubMenus": [
            {
                "id": 1,
                "path": "/main/dashboard",
                "subMenuName": "Dashboard",
                "MenuId": 1
            }
        ]
    },
    {
        "id": 2,
        "icon": "0xf54e",
        "menuName": "Agent",
        "SubMenus": [
            {
                "id": 2,
                "path": "/main/agent",
                "subMenuName": "Agent List",
                "MenuId": 2
            },
            {
                "id": 3,
                "path": "/main/request",
                "subMenuName": "Agent Pending Request",
                "MenuId": 2
            }
        ]
    },
    {
        "id": 3,
        "icon": "0xf09d",
        "menuName": "Payment",
        "SubMenus": [
            {
                "id": 4,
                "path": "/main/clear",
                "subMenuName": "Clear Payment",
                "MenuId": 3
            },
            {
                "id": 5,
                "path": "/main/due",
                "subMenuName": "Due Payment",
                "MenuId": 3
            },
            {
                "id": 6,
                "path": "/main/expired",
                "subMenuName": "Expired Due",
                "MenuId": 3
            }
        ]
    },
    {
        "id": 4,
        "icon": "0xf03a",
        "menuName": "Order",
        "SubMenus": [
            {
                "id": 7,
                "path": "/main/order",
                "subMenuName": "Order List",
                "MenuId": 4
            },
            {
                "id": 8,
                "path": "/main/runing-order",
                "subMenuName": "On-going Order",
                "MenuId": 4
            },
            {
                "id": 9,
                "path": "/main/history",
                "subMenuName": "Order History",
                "MenuId": 4
            }
        ]
    },
    {
        "id": 5,
        "icon": "0xf0c0",
        "menuName": "Client",
        "SubMenus": [
            {
                "id": 10,
                "path": "/main/client",
                "subMenuName": "Client List",
                "MenuId": 5
            }
        ]
    },
    {
        "id": 6,
        "icon": "0xf46b",
        "menuName": "Medicine",
        "SubMenus": [
            {
                "id": 11,
                "path": "/main/medicine",
                "subMenuName": "Medicine List",
                "MenuId": 6
            }
        ]
    },
    {
        "id": 7,
        "icon": "0xf086",
        "menuName": "FeedBack",
        "SubMenus": [
            {
                "id": 12,
                "path": "/main/feedback",
                "subMenuName": "Client Feedback",
                "MenuId": 7
            }
        ]
    },
    {
        "id": 8,
        "icon": "0xf541",
        "menuName": "Discount",
        "SubMenus": [
            {
                "id": 13,
                "path": "/main/discount",
                "subMenuName": "Discount List",
                "MenuId": 8
            }
        ]
    },
    {
        "id": 9,
        "icon": "0xf013",
        "menuName": "User Mangement",
        "SubMenus": [
            {
                "id": 14,
                "path": "/main/type",
                "subMenuName": "User Type",
                "MenuId": 9
            },
            {
                "id": 15,
                "path": "/main/assign",
                "subMenuName": "Menu Management",
                "MenuId": 9
            },
            {
                "id": 16,
                "path": "/main/user",
                "subMenuName": "Create User",
                "MenuId": 9
            }
        ]
    }
]
       * 
       */
    .get((req, res) => {
      app.db.Menu.findAll({
        // where: { username: req.body.username },
        include: [
          {
            model: app.db.SubMenu,
            required: true,
            include: [
              {
                model: app.db.AdminType,
                as: "belongsTo",
                attributes:[],
                where: { type: req.user.type },
              },
            ],

          },
        ],
      })
        .then((menu) => res.json(menu))
        .catch((err) => {
          res.status(401).json({ msg: err.message });
        });
    });
};
