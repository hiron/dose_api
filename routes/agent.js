module.exports = (app) => {
  const UserAgent = app.db.UserAgent;

  app
    .route("/agent")
    .all(app.auth.authenticate())
    /**
     *
     * @api {get} /agent Return the authenticated user's data
     * @apiName Get shop detail
     * @apiGroup Shop
     * @apiVersion  1.0.0
     *
     *
     * @apiHeader  {String} Authorization Token of authenticated user
     *
     * @apiSuccess  {String} firstName agents first name
     * @apiSuccess  {String} lastName Agents last name
     * @apiSuccess  {Date} birthDate Date of birth
     * @apiSuccess  {String} phone Agent's phone number
     * @apiSuccess  {String} email agent's email address
     * @apiSuccess  {String} pharmacy Shops name
     * @apiSuccess  {String} openingTime Shops open time
     * @apiSuccess  {String} closingTime Shops close time
     * @apiSuccess  {String} tradeLicense Shops trading lincense
     * @apiSuccess  {String} drugLicense Shops drug license
     * @apiSuccess  {String} sex Agent's gender
     * @apiSuccess  {String} nid agent's national ID
     * @apiSuccess  {String} level=5 Client's current level
     * @apiSuccess  {Boolean} emailStatus=false email is varified or no?
     *
     * @apiHeaderExample  {json} Header:
     * {
     *     "Authorization" : "JWT 1234.xyde.4563"
     * }
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 Ok
     {
    "birthDate": null,
    "pharmacy": null,
    "openingTime": null,
    "closingTime": null,
    "firstName": "Hiron",
    "lastName": "Das",
    "email": "hcdas.09@gmail.com",
    "sex": null,
    "nid": null,
    "level": 5,
    "emailStatus": false,
    "tradeLicense": null,
    "drugLicense": null,
    "phone": "01739573213",
    "address": null
}
     *
     *  @apiErrorExample {json} Error-Response:
     *  HTTP/1.1 404 not found
     *
     */
    .get((req, res) => {
      UserAgent.findOne({
        where: {
          agentId: req.user.uId,
        },
        include: [app.db.Login, app.db.ShopAddress],
        attributes: [
          "pharmacy",
          "openingTime",
          "closingTime",
          "firstName",
          "lastName",
          "email",
          "birthDate",
          "sex",
          "nid",
          "level",
          "emailStatus",
          "tradeLicense",
          "drugLicense",
        ],
      })
        .then((usr) => {
          console.log(JSON.parse(JSON.stringify(usr)));
          res.json(
            usr !== null
              ? [JSON.parse(JSON.stringify(usr))].map(
                  ({ Login, ShopAddress, ...d }) => ({
                    ...d,
                    phone: Login.phone,
                    address: ShopAddress,
                  })
                )[0]
              : usr
          );
        })
        .catch((err) => res.status(404).json(err.message));
    })
    /**
     *
     * @api {post} /agent Insert Agent details
     * @apiName add Agents
     * @apiGroup Shop
     * @apiVersion  1.0.0
     *
     *
     * @apiHeader  {String} Content-type content type of response
     * @apiHeader  {String} Authorication Token of authenticated user
     *
     * @apiSuccess  {String} firstName agents first name
     * @apiSuccess  {String} lastName Agents last name
     * @apiSuccess  {Date} birthDate Date of birth
     * @apiSuccess  {String} phone Agent's phone number
     * @apiSuccess  {String} email agent's email address
     * @apiSuccess  {String} pharmacy Shops name
     * @apiSuccess  {String} openingTime Shops open time
     * @apiSuccess  {String} closingTime Shops close time
     * @apiSuccess  {String} tradeLicense Shops trading lincense
     * @apiSuccess  {String} drugLicense Shops drug license
     * @apiSuccess  {String} sex Agent's gender
     * @apiSuccess  {String} nid agent's national ID
     *
     *
     * @apiParamExample  {json} Request-Example:
     * {
     *    "birthDate": "1989-03-17",
     *    "firstName": "Hiron",
     *    "lastName": "Das",
     *    "email": "hcdas.09@gmail.com",
     *    "sex": "male",
     *    "nid": "2693717",
     *  }
     *
     *
     * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 200 
     * {
    "birthDate": null,
    "pharmacy": null,
    "openingTime": null,
    "closingTime": null,
    "firstName": "Hiron",
    "lastName": "Das",
    "email": "hcdas.09@gmail.com",
    "sex": null,
    "nid": null,
    "level": 5,
    "emailStatus": false,
    "tradeLicense": null,
    "drugLicense": null,
    "phone": "01739573213",
    "address": {
        "id": 4,
        "lat": 32.86,
        "long": 2358.98,
        "name": "Shop Name",
        "createdAt": "2020-09-23T17:22:06.660Z",
        "updatedAt": "2020-09-23T17:22:06.660Z",
        "agentId": "474kffcmsao",
        "area": 1
    }
}
     * @apiHeaderExample {json} Request-Example:
     *    {
     *        "Content-Type":"application/json",
     *        "Authorization": "JWT 1234.xysd.34hiy"
     *    }
     * @apiErrorExample {json} Error-Response:
     *  HTTP/1.1 403 Forbidden
     *
     */

    .post((req, res) => {
      console.log(req.body.birthDate);
      UserAgent.create({
        ...req.body,
        birthDate:
          req.body.birthDate == null ? null : new Date(req.body.birthDate),
        agentId: req.user.uId,
      })
        .then((user) => res.status(200).json(user))
        .catch((err) =>
          res.status(403).json({
            msg: err.message,
          })
        );
    })
    /**
     *
     * @api {put} /agent Update agent's info
     * @apiName Update Agent
     * @apiGroup Shop
     * @apiVersion  1.0.0
     *
     *
     * @apiHeader  {String} Content-type Application content type
     * @apiHeader  {String} Authorization Token of authorized user
     * @apiHeaderExample {json} Request-Example:
     *    {
     *        "Content-Type":"application/json",
     *        "Authorization": "JWT 1234.xysd.34hiy"
     *    }
     *
     * @apiSuccess  {String} firstName agents first name
     * @apiSuccess  {String} lastName Agents last name
     * @apiSuccess  {Date} birthDate Date of birth
     * @apiSuccess  {String} phone Agent's phone number
     * @apiSuccess  {String} email agent's email address
     * @apiSuccess  {String} pharmacy Shops name
     * @apiSuccess  {String} openingTime Shops open time
     * @apiSuccess  {String} closingTime Shops close time
     * @apiSuccess  {String} tradeLicense Shops trading lincense
     * @apiSuccess  {String} drugLicense Shops drug license
     * @apiSuccess  {String} sex Agent's gender
     * @apiSuccess  {String} nid agent's national ID
     *
     * @apiParamExample  {json} Request-Example:
     * {
     *    "birthDate": "1989-03-17",
     *    "firstName": "Hiron",
     *    "lastName": "Das",
     *    "sex": "male",
     *    "nid": "2693717",
     *  }
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 No Content
    {
    "birthDate": null,
    "pharmacy": null,
    "openingTime": null,
    "closingTime": null,
    "firstName": "Hiron",
    "lastName": "Das",
    "email": "hcdas.09@gmail.com",
    "sex": null,
    "nid": null,
    "level": 5,
    "emailStatus": false,
    "tradeLicense": null,
    "drugLicense": null,
    "phone": "01739573213",
    "address": {
        "id": 4,
        "lat": 32.86,
        "long": 2358.98,
        "name": "Shop Name",
        "createdAt": "2020-09-23T17:22:06.660Z",
        "updatedAt": "2020-09-23T17:22:06.660Z",
        "agentId": "474kffcmsao",
        "area": 1
    }
}
     *
     */
    .put(async (req, res) => {
      const agent = await UserAgent.findOne({
        where: {
          agentId: req.user.uId,
        },
      });
      agent.firstName = req.body.firstName || agent.firstName;
      agent.lastName = req.body.lastName || agent.lastName;
      agent.pharmacy = req.body.pharmacy || agent.pharmacy;
      agent.openingTime = req.body.openingTime || agent.openingTime;
      agent.closingTime = req.body.closingTime || agent.closingTime;
      agent.tradeLicense = req.body.tradeLicense || agent.tradeLicense;
      agent.drugLicense = req.body.drugLicense || agent.drugLicense;
      //agent.lastName = req.body.lastName || agent.lastName;
      agent.sex = req.body.sex || agent.sex;
      agent.nid = req.body.nid || agent.nid;
      agent.email = req.body.email || agent.email;
      agent.birthDate = req.body.birthDate
        ? new Date(req.body.birthDate)
        : new Date(agent.birthDate);

      agent.emailStatus = req.body.email !== null ? false : agent.emailStatus;

      await agent.save();
      res.status(200).json(agent);
    });
};
