module.exports=(app)=>{
    return {of:(sumMenuName)=>{
        return  (req, res, next)=> {
            app.db.SubMenu.findOne({
             where: { subMenuName: sumMenuName },
             include: [
               {
                 model: app.db.AdminType,
                 as: "belongsTo",
                 where: { type: req.user.type },
                 required: true,
               },
             ],
           })
             .then((d) =>
               !d
                 ? res
                     .status(401)
                     .json({ msg: "You don't have any access permission" })
                 : next()
             )
             .catch((_) =>
               res.status(401).json({ msg: "You don't have any access permission" })
             );
         };
    }}
    
}