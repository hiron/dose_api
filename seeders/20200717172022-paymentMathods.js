'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
   
      return queryInterface.bulkInsert('payment_methods', [{
        method: 'bkash',
        img: 'images/d_06.png'
      },{
        method: 'rocket',
        img: 'images/d_09.png'
      },{
        method: 'ucash',
        img: 'images/d_11.png'
      },{
        method: 'qcash',
        img: 'images/d_03.png'
      },{
        method: 'okwallet',
        img: 'images/d_15.png'
      },{
        method: 'Cash on delivery',
        img: ''
      }], {
        underscored: true
      });
    
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('payment_methods', null, {});
  
  }
};
