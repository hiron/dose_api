"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "admin_types",
      [
        {
          type: "admin",
        },
        {
          type: "account-admin",
        },
        {
          type: "agent-admin",
        },
        {
          type: "customer-admin",
        },
      ],
      { underscored: true }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("admin_types", null, {});
  },
};
