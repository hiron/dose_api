"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "user_types",
      [
        {
          type: "client",
        },
        {
          type: "agent",
        },
      ],
      { underscored: true }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("user_types", null, {});
  },
};
