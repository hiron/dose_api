'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('discount_rules', [{
      type: 'client',
      level: "regular",
      discount: 0
    },{
      type: 'client',
      level: "Silver",
      discount: 2
    },{
      type: 'client',
      level: "gold",
      discount: 3
    },{
      type: 'client',
      level: "platinum",
      discount: 5
    },{
      type: 'agent',
      level: "regular",
      discount: 0
    },{
      type: 'agent',
      level: "Silver",
      discount: 2
    },{
      type: 'agent',
      level: "gold",
      discount: 3
    },{
      type: 'agent',
      level: "platinum",
      discount: 5
    }], {
      underscored: true
    });

  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('discount_rules', null, {});
    
  }
};