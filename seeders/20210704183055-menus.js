"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "menus",
      [
        { icon: "0xf0db", menu_name: "Menu", mui_icon:"dashboard" },
        { icon: "0xf54e", menu_name: "Agent",mui_icon:"store" },
        { icon: "0xf09d", menu_name: "Payment", mui_icon:"payment" },
        { icon: "0xf03a", menu_name: "Order",mui_icon:"Content_paste" },
        { icon: "0xf0c0", menu_name: "Client",mui_icon:"People" },
        { icon: "0xf46b", menu_name: "Medicin", mui_icon:"Vaccine" },
        { icon: "0xf086", menu_name: "FeedBack", mui_icon:"Question_Answer" },
        { icon: "0xf541", menu_name: "Discount", mui_icon:"Discount" },
        { icon: "0xf013", menu_name: "User Mangement", mui_icon:"Manage_Account" },
        //{icon: '0xf0c0', menu_name: 'Client'},
        //{icon: '0xf0c0', menu_name: 'Client'},
      ],
      {}
    );

    const menuId = await queryInterface.sequelize.query(
      `SELECT id from menus;`
    );

    const menuRows = menuId[0];

    return await queryInterface.bulkInsert(
      "sub_menus",
      [
        {
          path: "/main/dashboard",
          sub_menu_name: "Dashboard",
          menu_id: menuRows[0].id,
        },
        {
          path: "/main/agent",
          sub_menu_name: "Agent List",
          menu_id: menuRows[1].id,
        },
        {
          path: "/main/request",
          sub_menu_name: "Agent Pending Request",
          menu_id: menuRows[1].id,
        },
        {
          path: "/main/clear",
          sub_menu_name: "Clear Payment",
          menu_id: menuRows[2].id,
        },
        {
          path: "/main/due",
          sub_menu_name: "Due Payment",
          menu_id: menuRows[2].id,
        },
        {
          path: "/main/expired",
          sub_menu_name: "Expired Due",
          menu_id: menuRows[2].id,
        },
        {
          path: "/main/order",
          sub_menu_name: "Order List",
          menu_id: menuRows[3].id,
        },
        {
          path: "/main/runing-order",
          sub_menu_name: "On-going Order",
          menu_id: menuRows[3].id,
        },
        {
          path: "/main/history",
          sub_menu_name: "Order History",
          menu_id: menuRows[3].id,
        },
        {
          path: "/main/client",
          sub_menu_name: "Client List",
          menu_id: menuRows[4].id,
        },
        {
          path: "/main/medicine",
          sub_menu_name: "Medicin Management",
          menu_id: menuRows[5].id,
        },
        {
          path: "/main/feedback",
          sub_menu_name: "Client Feedback",
          menu_id: menuRows[6].id,
        },
        {
          path: "/main/faq",
          sub_menu_name: "FAQ",
          menu_id: menuRows[6].id,
        },
        {
          path: "/main/discount",
          sub_menu_name: "Discount List",
          menu_id: menuRows[7].id,
        },
        {
          path: "/main/type",
          sub_menu_name: "User Type",
          menu_id: menuRows[8].id,
        },
        {
          path: "/main/user",
          sub_menu_name: "Add User",
          menu_id: menuRows[8].id,
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("sub_menus", null, {});
    return await queryInterface.bulkDelete("menus", null, {});
  },
};
