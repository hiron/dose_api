"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "areas",
      [
        { district: "Dhaka", area: "Adabar" },
        { district: "Dhaka", area: "Azampur" },
        { district: "Dhaka", area: "Badda" },
        { district: "Dhaka", area: "Bangsal" },
        { district: "Dhaka", area: "Bimanbandar" },
        { district: "Dhaka", area: "Cantonment" },
        { district: "Dhaka", area: "Chowkbazar" },
        { district: "Dhaka", area: "Darus Salam" },
        { district: "Dhaka", area: "Demra" },
        { district: "Dhaka", area: "Dhanmondi" },
        { district: "Dhaka", area: "Gendaria" },
        { district: "Dhaka", area: "Gulshan" },
        { district: "Dhaka", area: "Hazaribagh" },
        { district: "Dhaka", area: "Kadamtali" },
        { district: "Dhaka", area: "Kafrul" },
        { district: "Dhaka", area: "Kalabagan" },
        { district: "Dhaka", area: "Kamrangirchar" },
        { district: "Dhaka", area: "Khilgaon" },
        { district: "Dhaka", area: "Khilkhet" },
        { district: "Dhaka", area: "Kotwali" },
        { district: "Dhaka", area: "Lalbagh" },
        { district: "Dhaka", area: "Mirpur Model" },
        { district: "Dhaka", area: "Mohammadpur" },
        { district: "Dhaka", area: "Motijheel" },
        { district: "Dhaka", area: "New Market" },
        { district: "Dhaka", area: "Pallabi" },
        { district: "Dhaka", area: "Paltan" },
        { district: "Dhaka", area: "Panthapath" },
        { district: "Dhaka", area: "Ramna" },
        { district: "Dhaka", area: "Rampura" },
        { district: "Dhaka", area: "Sabujbagh" },
        { district: "Dhaka", area: "Shah Ali" },
        { district: "Dhaka", area: "Shahbag" },
        { district: "Dhaka", area: "Sher-e-Bangla Nagar" },
        { district: "Dhaka", area: "Shyampur" },
        { district: "Dhaka", area: "Sutrapur" },
        { district: "Dhaka", area: "Tejgaon Industrial Area" },
        { district: "Dhaka", area: "Tejgaon" },
        { district: "Dhaka", area: "Turag" },
        { district: "Dhaka", area: "Uttar Khan" },
        { district: "Dhaka", area: "Uttara" },
        { district: "Dhaka", area: "Vatara" },
        { district: "Dhaka", area: "Wari" },
      ],
      {
        underscored: true
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("areas", null, {
      underscored: true
    });
  },
};
