"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "cancel_causes",
      [
        {
          cause: "Change/combine order",
          type: "client",
        },
        {
          cause: "Duplicate order",
          type: "client",
        },
        {
          cause: "Change of delivery address",
          type: "client",
        },
        {
          cause: "Change of mind",
          type: "client",
        },
        {
          cause: "Deside for alternative product",
          type: "client",
        },
        {
          cause: "Change payment method",
          type: "client",
        },
        {
          cause: "Found cheaper elsewhere",
          type: "client",
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("cancel_causes", null, {});
  },
};
