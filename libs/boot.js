// const https = require("https");
// const http = require('http');
// const fs = require("fs");

module.exports = (app) => {
  const PORT = process.env.PORT || 3000;

  // const credentials = {
  //   key: fs.readFileSync("./ssl/dose.key", "utf8"),
  //   cert: fs.readFileSync("./ssl/dose.cert", "utf8"),
  // };
  // console.log(app.db.sequelize);
  if (process.env.NODE_ENV !== "test")
    app.db.sequelize
      .sync()
      .then(() =>{}
        //https.createServer(credentials,app)
        //http.createServer(app).listen(PORT, () => console.log(`DOSE API @port-${PORT}`))
      )
      .catch((err) => console.log(err));
};
