const nodeMailer = require("nodemailer");
const http = require("http");

module.exports = (app) => {
  return {
    sendMail: ({ from, to, subject, html, text }) => {
      console.log(to);
      // let testAccount = await nodeMailer.createTestAccount();
      let transporter = nodeMailer.createTransport({
        // host: "imap-mail.outlook.com",
        host: "smtp-mail.outlook.com",

        // port: 993,
        port: 587,
        secure: false,
        // secureConnection: false,
        tls: {
          ciphers: "SSLv3",
        },
        auth: {
          user: process.env.email,
          pass: process.env.pass,
        },
      });

      return transporter.sendMail({
        from: from || "'Dose BD'<" + process.env.email + ">",
        to,
        subject,
        html: html || "",
        text: text || "",
      });
    },
  };
};
