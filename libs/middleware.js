//const bodyParser = require("body-parser");
const express = require("express");
const cors = require("cors");

module.exports = (app) => {
  // app.use(bodyParser.json());
  app.use(express.json({ limit: "1mb" }));
  app.use(
    cors({
      origin: "*",
      methods: ["GET", "POST", "PUT", "DELETE"],
      allowedHeaders: ["Content-Type", "Authorization"],
    })
  );
  app.use((req, res, next) => {
    delete req.body.id;
    next();
  });
  app.use(express.static("public"));
  //app.use(express.static("public/public-flutter"));
  app.use(app.auth.initialize());
};
